//
//  UIView+ColorOfPoint.h
//  Europa Plus
//
//  Created by Piezo on 2/6/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ColorOfPoint)

- (UIColor *) colorOfPoint:(CGPoint)point;

@end
