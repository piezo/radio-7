//
//  JASidePanelController+Button.m
//  Europa Plus
//
//  Created by Piezo on 2/10/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "JASidePanelController+Button.h"

@implementation JASidePanelController (Button)


- (UIBarButtonItem *)leftButtonForCenterPanel {
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [btn setImage:[UIImage imageNamed:@"menu-button-passive"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"menu-button-active"] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(toggleLeftPanel:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * btnItm = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    return btnItm;
}

@end
