//
//  NSString+EncodeURIComponent.h
//  Europa Plus
//
//  Created by Piezo on 2/1/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EncodeURIComponent)

+ (NSString*)stringEncodeURIComponent:(NSString *)string;
+ (NSString*)stringEncodeURIComponent2:(NSString *)string;

@end
