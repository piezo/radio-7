//
//  UIView+UserInteractionFeatures.m
//  Europa Plus
//
//  Created by Piezo on 2/9/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "UIView+UserInteractionFeatures.h"

@implementation UIView (UserInteractionFeatures)


-(void)setRecursiveUserInteraction:(BOOL)value{
    self.userInteractionEnabled =   value;
    for (UIView *view in [self subviews]) {
        [view setRecursiveUserInteraction:value];
    }
}

@end
