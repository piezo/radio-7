//
//  NSString+Empty.m
//  Europa Plus
//
//  Created by Piezo on 2/10/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "NSString+Empty.h"

@implementation NSString (Empty)

+(NSString *) emptyIfNull : (NSString *) string{
    
    if (string == nil) {
        return @"";
    }
    
    return string;
    
}

@end
