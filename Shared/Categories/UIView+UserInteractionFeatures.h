//
//  UIView+UserInteractionFeatures.h
//  Europa Plus
//
//  Created by Piezo on 2/9/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UserInteractionFeatures)

-(void)setRecursiveUserInteraction:(BOOL)value;

@end
