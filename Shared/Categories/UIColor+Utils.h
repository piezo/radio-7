//
//  UIColor+Grayscale.h
//  Radio7
//
//  Created by Piezo on 1/2/14.
//  Copyright (c) 2014 Oleksandr Izvekov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

+(CGFloat)findDistanceBetweenTwoColor:(UIColor *)first secondColor:(UIColor *)second;

-(UIColor *)grayscale;



@end
