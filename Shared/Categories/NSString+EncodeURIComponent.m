//
//  NSString+EncodeURIComponent.m
//  Europa Plus
//
//  Created by Piezo on 2/1/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "NSString+EncodeURIComponent.h"

@implementation NSString (EncodeURIComponent)



+ (NSString*)stringEncodeURIComponent:(NSString *)string {

    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    
    string = [string stringByReplacingOccurrencesOfString:@" Feat. " withString:@"|"];
    string = [string stringByReplacingOccurrencesOfString:@" feat. " withString:@"|"];
    string = [string stringByReplacingOccurrencesOfString:@" & " withString:@"|"];
    string = [string stringByReplacingOccurrencesOfString:@";" withString:@"|"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    string = [string stringByReplacingOccurrencesOfString:@"_" withString:@"+"];
    
    string = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                       (__bridge CFStringRef)string,
                                                                       NULL,//CFSTR("|"),
                                                                       CFSTR("!*'();:@&=$,/?%#[]"),
                                                                       kCFStringEncodingUTF8));
    
    return string;
}


+ (NSString*)stringEncodeURIComponent2:(NSString *)string {
    
    @try {
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSMutableString *s = [NSMutableString stringWithString:string];
        NSRange start = [s rangeOfString:@"("];
        NSRange end = [s rangeOfString:@")"];
        
        if (end.location!=NSNotFound) {
            [s deleteCharactersInRange:(NSRange){ start.location, end.location - start.location + 1}];
        }
        
        
        string = s;
        
        string = [string stringByReplacingOccurrencesOfString:@"ELLIOTT" withString:@"ELLIOT"];
        string = [string stringByReplacingOccurrencesOfString:@"ЦЕЛОВАТ" withString:@"ЦЕЛОВАТЬ"];
        string = [string stringByReplacingOccurrencesOfString:@"ЗАКРИТИй" withString:@"ЗАКРыТый"];
        string = [string stringByReplacingOccurrencesOfString:@" ТИ" withString:@" Ты"];
        
        
        
        string = [string stringByReplacingOccurrencesOfString:@" & " withString:@" "];
        string = [string stringByReplacingOccurrencesOfString:@";" withString:@" "];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@" "];
        string = [string stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    }
    @catch (NSException * e) {
        CLS_LOG(@"Exception: %@", e);
    }
    @finally {
    }
    
    
    
    return string;
}
@end
