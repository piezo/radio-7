//
//  UIColor+Grayscale.m
//  Radio7
//
//  Created by Piezo on 1/2/14.
//  Copyright (c) 2014 Oleksandr Izvekov. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)


+(CGFloat)findDistanceBetweenTwoColor:(UIColor *)first secondColor:(UIColor *)second //
{
    
    if (first == nil || second == nil) {
        return 1.0;
    }
    
    CGFloat distance;
    CGFloat r1,g1,b1,a1,r2,g2,b2,a2;
    
    
    const CGFloat *components = CGColorGetComponents(first.CGColor);
    r1 = components[0];
    g1 = components[1];
    b1 = components[2];
    a1 = components[3];
    
    const CGFloat *components2 = CGColorGetComponents(second.CGColor);
    r2 = components2[0];
    g2 = components2[1];
    b2 = components2[2];
    a2 = components2[3];
    
//    [first getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    
//    [second getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
    
    distance = sqrtf( powf(r1 - r2, 2) + powf(g1 - g2, 2) + powf(b1 - b2, 2) );
    
    return distance;
}

- (UIColor *)grayscale {
    CGFloat red = 0;
    CGFloat blue = 0;
    CGFloat green = 0;
    CGFloat alpha = 0;
    if ([self getRed:&red green:&green blue:&blue alpha:&alpha]) {
        return [UIColor colorWithWhite:(0.299*red + 0.587*green + 0.114*blue) alpha:alpha];
    } else {
        return self;
    }
}

@end
