//
//  UIColor+Grayscale.h
//  Radio7
//
//  Created by Piezo on 1/2/14.
//  Copyright (c) 2014 Oleksandr Izvekov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Grayscale)

- (UIColor *)grayscale;


@end
