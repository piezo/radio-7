//
//  UIView+Screenshot.m
//  Europa Plus
//
//  Created by Piezoon 2/7/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIView+Screenshot.h"

@implementation UIView (Screenshot)

- (UIImage*)screenshotAfterScreenUpdates:(BOOL) afterScreenUpdates
{
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, self.window.screen.scale);
    
    /* iOS 7 */
    if ([self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)] && self.window)
        [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:afterScreenUpdates];    
    else /* iOS 6 */
        [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage* ret = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return ret;

}

@end