//
//  UIImageView+AFNetworkingFadein.m
//  Europa Plus
//
//  Created by Piezo on 5/28/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "UIImageView+AFNetworkingFadein.h"


@implementation UIImageView (AFNetworkingFadein)


- (void)setImageWithURL:(NSURL *)url fadein : (BOOL) fadein{

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    __block UIImageView *weakSelf = self;
    
    [self setImageWithURLRequest:request placeholderImage:[[UIImage alloc] init]
                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                weakSelf.alpha = 0;
                                weakSelf.image = image;
                             
                                [UIView animateWithDuration:.3 animations:^{
                                    weakSelf.alpha=1;
                                }];
                         }
                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                         
                         }];

}

- (void)setImageWithURL:(NSURL *)url fadein : (BOOL) fadein completeBlock : (void (^)())completeBlock{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    __block UIImageView *weakSelf = self;
    
    [self setImageWithURLRequest:request placeholderImage:[[UIImage alloc] init]
                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                             weakSelf.alpha = 0;
                             weakSelf.image = image;
                             
                             [UIView animateWithDuration:.3 animations:^{
                                 weakSelf.alpha=1;
                             }];
                             
                             completeBlock();
                         }
                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                             
                         }];
}

@end
