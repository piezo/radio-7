//
//  UIView+Screenshot.h
//  Europa Plus
//
//  Created by Piezo on 2/7/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Screenshot)

- (UIImage*)screenshotAfterScreenUpdates:(BOOL) afterScreenUpdates;


@end
