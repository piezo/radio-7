//
//  NSString+Empty.h
//  Europa Plus
//
//  Created by Piezo on 2/10/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Empty)

+(NSString *) emptyIfNull : (NSString *) string;

@end
