//
//  UIImageView+AFNetworkingFadein.h
//  Europa Plus
//
//  Created by Piezo on 5/28/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+AFNetworking.h>

@interface UIImageView (AFNetworkingFadein)

- (void)setImageWithURL:(NSURL *)url fadein : (BOOL) fadein;
- (void)setImageWithURL:(NSURL *)url fadein : (BOOL) fadein completeBlock : (void (^)())completeBlock;
@end
