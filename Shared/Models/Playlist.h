//
//  Playlist.h
//  Europa Plus
//
//  Created by Piezo on 3/14/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PlaylistItem;

@interface Playlist : NSManagedObject

@property (nonatomic, retain) NSString * logo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * preferredStream;
@property (nonatomic, retain) NSNumber * priotiry;
@property (nonatomic, retain) NSData * screenshot;
@property (nonatomic, retain) NSString * stream32;
@property (nonatomic, retain) NSString * stream64;
@property (nonatomic, retain) NSString * stream128;
@property (nonatomic, retain) NSString * streamHD;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *items;
@property (nonatomic, retain) PlaylistItem *lastItem;
@end

@interface Playlist (CoreDataGeneratedAccessors)

- (void)addItemsObject:(PlaylistItem *)value;
- (void)removeItemsObject:(PlaylistItem *)value;
- (void)addItems:(NSSet *)values;
- (void)removeItems:(NSSet *)values;

@end
