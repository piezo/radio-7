//
//  Cover.m
//  Europa Plus
//
//  Created by Piezo on 2/2/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "Cover.h"
#import "Track.h"


@implementation Cover

@dynamic url;
@dynamic img;
@dynamic searchUrl;
@dynamic tracks;

@end
