//
//  Track.m
//  Europa Plus
//
//  Created by Piezo on 5/2/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "Track.h"
#import "Cover.h"
#import "PlaylistItem.h"


@implementation Track

@dynamic artist;
@dynamic artist_id;
@dynamic duration;
@dynamic favorite;
@dynamic hash_id;
@dynamic img1;
@dynamic img2;
@dynamic itn_artworkUrl100;
@dynamic itn_collectionId;
@dynamic itn_collectionViewUrl;
@dynamic itn_complete;
@dynamic itn_complete_success;
@dynamic itn_need_image;
@dynamic itn_previewUrl;
@dynamic rating;
@dynamic song;
@dynamic song_id;
@dynamic itn_trackId;
@dynamic itn_trackPrice;
@dynamic itn_currency;
@dynamic cover;
@dynamic paylistItems;

@end
