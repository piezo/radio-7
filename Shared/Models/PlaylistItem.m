//
//  PlaylistItem.m
//  Europa Plus
//
//  Created by Piezoon 6/15/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "PlaylistItem.h"
#import "Playlist.h"
#import "Track.h"


@implementation PlaylistItem

@dynamic complete;
@dynamic tid;
@dynamic time_end;
@dynamic time_start;
@dynamic start_ts;
@dynamic playlist;
@dynamic track;

@end
