//
//  Playlist.m
//  Europa Plus
//
//  Created by Piezo on 3/14/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "Playlist.h"
#import "PlaylistItem.h"


@implementation Playlist

@dynamic logo;
@dynamic name;
@dynamic preferredStream;
@dynamic priotiry;
@dynamic screenshot;
@dynamic stream32;
@dynamic stream64;
@dynamic stream128;
@dynamic streamHD;
@dynamic title;
@dynamic items;
@dynamic lastItem;

@end
