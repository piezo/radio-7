//
//  PlaylistItem.h
//  Europa Plus
//
//  Created by Piezo on 6/15/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Playlist, Track;

@interface PlaylistItem : NSManagedObject

@property (nonatomic, retain) NSNumber * complete;
@property (nonatomic, retain) NSNumber * tid;
@property (nonatomic, retain) NSDate * time_end;
@property (nonatomic, retain) NSDate * time_start;
@property (nonatomic, retain) NSNumber * start_ts;
@property (nonatomic, retain) Playlist *playlist;
@property (nonatomic, retain) Track *track;

@end
