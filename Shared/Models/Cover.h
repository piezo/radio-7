//
//  Cover.h
//  Europa Plus
//
//  Created by Piezo on 2/2/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Track;

@interface Cover : NSManagedObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSData * img;
@property (nonatomic, retain) NSString * searchUrl;
@property (nonatomic, retain) NSSet *tracks;
@end

@interface Cover (CoreDataGeneratedAccessors)

- (void)addTracksObject:(Track *)value;
- (void)removeTracksObject:(Track *)value;
- (void)addTracks:(NSSet *)values;
- (void)removeTracks:(NSSet *)values;

@end
