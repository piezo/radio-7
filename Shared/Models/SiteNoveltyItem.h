//
//  SiteNoveltyItem.h
//  Europa Plus
//
//  Created by Piezo on 2/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SiteNoveltyItem : NSManagedObject

@property (nonatomic, retain) NSString * artist;
@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSString * imgURL;
@property (nonatomic, retain) NSString * itmId;
@property (nonatomic, retain) NSString * mp3;
@property (nonatomic, retain) NSString * recording;
@property (nonatomic, retain) NSNumber * sort;
@property (nonatomic, retain) NSString * time;

@end
