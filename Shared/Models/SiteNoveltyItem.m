//
//  SiteNoveltyItem.m
//  Europa Plus
//
//  Created by Piezo on 2/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "SiteNoveltyItem.h"


@implementation SiteNoveltyItem

@dynamic artist;
@dynamic favorite;
@dynamic imgURL;
@dynamic itmId;
@dynamic mp3;
@dynamic recording;
@dynamic sort;
@dynamic time;

@end
