//
//  Track.h
//  Europa Plus
//
//  Created by Piezo on 5/2/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cover, PlaylistItem;

@interface Track : NSManagedObject

@property (nonatomic, retain) NSString * artist;
@property (nonatomic, retain) NSNumber * artist_id;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSString * hash_id;
@property (nonatomic, retain) NSString * img1;
@property (nonatomic, retain) NSString * img2;
@property (nonatomic, retain) NSString * itn_artworkUrl100;
@property (nonatomic, retain) NSNumber * itn_collectionId;
@property (nonatomic, retain) NSString * itn_collectionViewUrl;
@property (nonatomic, retain) NSNumber * itn_complete;
@property (nonatomic, retain) NSNumber * itn_complete_success;
@property (nonatomic, retain) NSNumber * itn_need_image;
@property (nonatomic, retain) NSString * itn_previewUrl;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSString * song;
@property (nonatomic, retain) NSNumber * song_id;
@property (nonatomic, retain) NSNumber * itn_trackId;
@property (nonatomic, retain) NSNumber * itn_trackPrice;
@property (nonatomic, retain) NSString * itn_currency;
@property (nonatomic, retain) Cover *cover;
@property (nonatomic, retain) NSSet *paylistItems;
@end

@interface Track (CoreDataGeneratedAccessors)

- (void)addPaylistItemsObject:(PlaylistItem *)value;
- (void)removePaylistItemsObject:(PlaylistItem *)value;
- (void)addPaylistItems:(NSSet *)values;
- (void)removePaylistItems:(NSSet *)values;

@end
