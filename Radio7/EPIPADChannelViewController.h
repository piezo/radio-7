//
//  EPIPADMainViewController.h
//  Europa Plus
//
//  Created by Piezo on 3/23/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPIPADStyledViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "EPIPADPlaylistViewController.h"
#import <StreamingKit/STKAudioPlayer.h>

@interface EPIPADChannelViewController : EPIPADStyledViewController<EPIPADPlaylistViewControllerDelegate, STKAudioPlayerDelegate>

@property (nonatomic, strong) Playlist *playlist;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttomSpacingCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *plTopSpacingCnstr;

- (NSString *) streamUrl;

@end
