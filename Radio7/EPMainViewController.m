//
//  EPMainViewController.m
//  Europa Plus
//
//  Created by Piezo on 2/9/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPMainViewController.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>


@interface EPMainViewController ()

@property (nonatomic, strong) IBOutlet UIAlertView *alertView;

@end

@implementation EPMainViewController{
    UIView *_preloaderImageView;
    UIActivityIndicatorView *_activityIndicator;
    
}

-(void)awakeFromNib{
    
    self.carousrlController = [self.storyboard instantiateViewControllerWithIdentifier:@"CarouselViewController"];
    
    [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"]];
    [self setCenterPanel:_carousrlController];
    
    self.wantsFullScreenLayout = YES;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.leftFixedWidth = 280;
    self.bounceOnSidePanelClose = NO;
    [super viewDidLoad];
	
    
    _preloaderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:IS_IPHONE5?@"LaunchImage-568h.png":@"LaunchImage"]];
    [self.view setUserInteractionEnabled:NO];
    [self.view addSubview:_preloaderImageView];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds)-5,
                                                                                   CGRectGetMaxY(self.view.bounds)-100,
                                                                                   10,
                                                                                   10)];
    [_activityIndicator startAnimating];
    [self.view addSubview:_activityIndicator];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itunesTrackUpdateNotification:)
                                                 name:epItunesTrackUpdateNotification
                                               object:nil];
    
    
    __block EPWebsiteDataHelper *weakDataHelper = [EPWebsiteDataHelper shared];
    __block BOOL reachability = NO;
    __block EPMainViewController *weakSelf = self;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        CLS_LOG(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                CLS_LOG(@"No Internet Connection");
                reachability = NO;
                [weakDataHelper stopDataUpdates];
                
                weakSelf.alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                                 message:@"You are not connected to the Internet. Make sure your internet connection is active and try again"
                                                                                delegate:self
                                                                       cancelButtonTitle:@"Ok"
                                                                       otherButtonTitles: nil];
                
                [weakSelf.alertView show];
                
                break;
            }

                

            case AFNetworkReachabilityStatusReachableViaWiFi:
                
                if (!reachability) {
                    [weakDataHelper startDataUpdates];
                    [weakSelf.alertView dismissWithClickedButtonIndex:0 animated:YES];
                }
                
                reachability = YES;
                
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                
                if (!reachability) {
                    [weakDataHelper startDataUpdates];
                    [weakSelf.alertView dismissWithClickedButtonIndex:0 animated:YES];
                }
                
                reachability = YES;
                
                break;
            default:
                CLS_LOG(@"Unkown network status");
                break;
                
                [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
    }];

    
}

-(void) itunesTrackUpdateNotification : (NSNotification *) notification
{
    
    Playlist *playlist = (Playlist *) notification.userInfo[@"playlist"];
    
    if ([playlist.name isEqualToString:@"r7"]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:.5
                                  delay:1
                                options:0
                             animations:^{
                                 _preloaderImageView.alpha=.0;
                                 _activityIndicator.alpha =.0;
                             } completion:^(BOOL finished) {
                                 [_activityIndicator stopAnimating];
                                 [_activityIndicator  removeFromSuperview];
                                 [_preloaderImageView removeFromSuperview];
                                 [self.view setUserInteractionEnabled:YES];
                             }
             ];
        });
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
 
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        self.alertView = [[UIAlertView alloc] initWithTitle:@""
                                                        message: NSLocalizedString(@"NETWORK ERROR",nil)
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        
        [self.alertView show];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)styleContainer:(UIView *)container animate:(BOOL)animate duration:(NSTimeInterval)duration {
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:container.bounds cornerRadius:0.0f];
    if (animate) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"shadowPath"];
        animation.fromValue = (id)container.layer.shadowPath;
        animation.toValue = (id)shadowPath.CGPath;
        animation.duration = duration;
        [container.layer addAnimation:animation forKey:@"shadowPath"];
    }
    container.layer.shadowPath = shadowPath.CGPath;
    container.layer.shadowColor = [UIColor blackColor].CGColor;
    container.layer.shadowRadius = 10.0f;
    container.layer.shadowOpacity = 0.35f;
    container.clipsToBounds = NO;
}

- (void)stylePanel:(UIView *)panel {
    panel.layer.cornerRadius = 0.0f;
    panel.clipsToBounds = YES;
}

@end
