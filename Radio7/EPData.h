//
//  EPData.h
//  Europa Plus
//
//  Created by Piezo on 2/6/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STKAudioPlayer.h"
#import "EPIPADChannelViewController.h"

@class EPChannelScrollViewController;

@interface EPData : NSObject{
    STKAudioPlayer *_player;
    NSDate *_updTime;
}

+(EPData *) sharedEPData;

@property (nonatomic, strong) STKAudioPlayer *player;
@property (nonatomic, strong) Playlist *currentPlaylist;
@property (nonatomic, strong) Playlist *alarmPlaylist;
@property (nonatomic, strong) EPChannelScrollViewController *currentChannelController;
//@property (nonatomic, strong) EPChannelViewController *epChannelController;
@property (nonatomic, strong) EPIPADChannelViewController *ipadChannelController;

@property (nonatomic, strong) NSMutableArray *alarmTimers;
@property (nonatomic, strong) NSMutableArray *alarmDays;
@property (nonatomic, assign) NSInteger alarmHours;
@property (nonatomic, assign) NSInteger alarmMinutes;
@property (nonatomic, strong) NSString *alarmChannel;

@property (nonatomic, strong) NSArray *bannersArray;
@property (nonatomic, strong) UIImage *imageOfCurrentChannelController;
@property (nonatomic, strong) NSDate *updTime;

-(void) setupTimerAlarmsForHour : (NSInteger) hours
                   minutes : (NSInteger) minutes
                      days : (NSArray *) days
                channelIdx : (NSString *) aChannelIdx
                  playlist : (Playlist *) anAlarmPlaylist;

- (dispatch_queue_t)serialQueue;

@end
