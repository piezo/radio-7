//
//  EPPlaylistCollectionCell.h
//  Europa Plus
//
//  Created by Piezo on 5/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StreamingKit/STKAudioPlayer.h>
#import "EPPlayIButton.h"

@interface EPPlaylistCollectionCell : UICollectionViewCell<STKAudioPlayerDelegate>{
//    NSString *streamUrl;
//    id item;
}


@property (weak, nonatomic) IBOutlet UIImageView *cover;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *recordingLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet EPPlayIButton *playerButton;
@property (nonatomic, strong) NSString *streamUrl;
@property (nonatomic, strong) id item;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverLeftCnstr;


@end
