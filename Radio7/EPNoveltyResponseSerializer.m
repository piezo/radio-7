//
//  EPNoveltyResponseSerializer.m
//  Europa Plus
//
//  Created by Piezo on 2/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPNoveltyResponseSerializer.h"
#import "HTMLParser.h"
#import "NSString+Empty.h"


@implementation EPNoveltyResponseSerializer

-(id)responseObjectForResponse:(NSURLResponse *)response
                          data:(NSData *)data
                         error:(NSError *__autoreleasing *)error
{
    
    NSString * document = [super responseObjectForResponse:response
                                                      data:data
                                                     error:error];
    
    
    
    HTMLParser *parser = [[HTMLParser alloc] initWithString:document error:(NSError *__autoreleasing *)error];
    
    NSArray *jp_container_children = [parser.doc findChildrenOfClass:@"player-in-playlist-holder "];
    
    [SiteNoveltyItem MR_truncateAll];
    
    [jp_container_children enumerateObjectsUsingBlock:^(HTMLNode *node, NSUInteger idx, BOOL *stop){
        
        
        
        NSString *itmId = [node getAttributeNamed:@"id"];
        NSString *imgURL = [[[[node findChildOfClass:@"img-holder"] findChildTag:@"img"] getAttributeNamed:@"src"] stringByReplacingOccurrencesOfString:@"song_50x50" withString:@"song_110x110"];
//      NSString *time = [[node findChildOfClass:@"time"] contents];
        NSString *artist = [[[node findChildOfClass:@"jp-title"] findChildTag:@"a"] contents];
        NSString *recording = [[[node findChildOfClass:@"jp-title"] findChildTag:@"span"] contents];
        NSString *mp3 = [[[node findChildOfClass:@"jp-controls"] findChildTag:@"a"] getAttributeNamed:@"data-url"];
        
        SiteNoveltyItem *sitePlaylistItem = [SiteNoveltyItem MR_createEntity];

        
        sitePlaylistItem.itmId = [NSString emptyIfNull:itmId];
        sitePlaylistItem.imgURL = [NSString emptyIfNull:imgURL];
//      sitePlaylistItem.time = [NSString emptyIfNull:time];
        sitePlaylistItem.artist = [NSString emptyIfNull:artist];
        sitePlaylistItem.recording = [NSString emptyIfNull:recording];
        sitePlaylistItem.mp3 = [NSString emptyIfNull:mp3];
        sitePlaylistItem.sort = @([SitePlaylistItem MR_countOfEntities]);

 
        
        
    }];
    
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
    
    return document;
    
}

@end
