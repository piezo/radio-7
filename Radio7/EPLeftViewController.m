//
//  EPLeftViewController.m
//  Europa Plus
//
//  Created by Piezo on 1/31/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPLeftViewController.h"
#import "FXBlurView.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "EPChannelScrollViewController.h"
#import "EPData.h"
#import "UIView+ColorOfPoint.h"
#import "UIButton+tintImage.h"
#import "UIColor+Grayscale.h"
#import "UIView+Screenshot.h"
#import "UIImage+Tint.h"
#import "EPMainViewController.h"
#import "CBAutoScrollLabel.h"

@interface EPLeftViewController ()


@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *menuDelimitersImageView;

@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *noveltyBtn;
@property (weak, nonatomic) IBOutlet UIButton *playListBtn;
@property (weak, nonatomic) IBOutlet UIButton *alarmBtn;

@property (weak, nonatomic) IBOutlet UILabel *playLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *bannersScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannersTopCnstr;

@end

@implementation EPLeftViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)showChannel:(id)sender {
    [(EPMainViewController *) self.sidePanelController setCenterPanel:[(EPMainViewController *) self.sidePanelController carousrlController]];
}

-(void)updateViewConstraints{
    [super updateViewConstraints];
    if (!IS_OS_7_OR_LATER) {
//        _topCnstr.constant+=20;
    }
    
    if (!IS_IPHONE5) {
        _bannersTopCnstr.constant=0;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showCenter:(id)sender {
    [self.sidePanelController showCenterPanelAnimated:YES];
}
- (IBAction)playlistBtnDidPress:(id)sender {
// s   [[EPWebsiteDataHelper shared] requestPlaylist];
}


-(void) updateUITheme{
    
    
//    [super updateUITheme];
    
//    UIColor *tintColor = [[self currentController] tintColor];
//    UIColor *aTintColor = [[self currentController] aTintColor];
    UIColor *tintColor = [UIColor whiteColor];
    
    PlaylistItem *__plItem =  [[[[EPData sharedEPData] currentChannelController] playlist] lastItem];
    
    NSString *playLAbelText = [NSString stringWithFormat:@"%@ - %@",__plItem.track.artist,__plItem.track.song];
    
//    _playLabel.font = _noveltyBtn.titleLabel.font;
//    _playLabel.labelSpacing = 35; // distance between start and end labels
//    _playLabel.pauseInterval = 0; // seconds of pause before scrolling starts again
//    _playLabel.scrollSpeed = 30; // pixels per second
//    _playLabel.textAlignment = NSTextAlignmentLeft; // centers text when no auto-scrolling is applied
//    _playLabel.fadeLength = 12.f; // length of the left and right edge fade, 0 to disable

    [_playBtn setTitle:playLAbelText forState:UIControlStateNormal];
//    [_playLabel setText:playLAbelText refreshLabels:YES];
    
    _menuDelimitersImageView.image = [[UIImage imageNamed:@"menu_delimiters"] imageTintedWithColor:tintColor];


}

- (IBAction)vkBtnDidPress:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://vk.com/radio7"]];
    
    NSURL *a_url = [NSURL URLWithString:@"vk://vk.com/radio7"];
    NSURL *w_url = [NSURL URLWithString:@"http://vk.com/radio7"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }
}

- (IBAction)fbBtnDidPress:(id)sender {
    
    NSURL *a_url = [NSURL URLWithString:@"fb://profile/185681461468828"];
    NSURL *w_url = [NSURL URLWithString:@"https://www.facebook.com/Radio7Moscow"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }
    
}

- (IBAction)twBtnDidPress:(id)sender {

    
    NSURL *a_url = [NSURL URLWithString:@"twitter://user?127845956"];
    NSURL *w_url = [NSURL URLWithString:@"https://twitter.com/radio7msk"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }

}

- (IBAction)ytBtnDidPress:(id)sender {
    NSURL *a_url = [NSURL URLWithString:@"youtube://user/EuropaPlusMsk"];
    NSURL *w_url = [NSURL URLWithString:@"http://www.youtube.com/user/EuropaPlusMsk"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }

}

- (IBAction)webBtnDidPress:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://radio7.ru"]];
}



@end
