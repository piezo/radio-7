//
//  EPChannelTableViewController.h
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPChannelControllerDelegate.h"

@interface EPChannelTableViewController : UITableViewController

@property (nonatomic, weak) id<EPChannelControllerDelegate> delegate;

@property (nonatomic, strong) Playlist *playlist;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, strong) UIColor *aTintColor;
@property (nonatomic, assign) CGFloat white;
@property (nonatomic, strong) NSArray *sortedPLItems;


-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white;

@end
