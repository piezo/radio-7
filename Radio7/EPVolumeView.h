//
//  EPVolumeView.h
//  Europa Plus
//
//  Created by Piezo on 6/1/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface EPVolumeView : MPVolumeView{
//    UIColor *tintColor;
}

@property (nonatomic, strong) UIColor *tintColor;

@end
