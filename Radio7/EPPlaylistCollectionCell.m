//
//  EPPlaylistCollectionCell.m
//  Europa Plus
//
//  Created by Piezo on 5/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPPlaylistCollectionCell.h"
#import "EPData.h"

@implementation EPPlaylistCollectionCell{
    
}

-(void)awakeFromNib{
    
    __block STKAudioPlayer *weakPlayer = [[EPData sharedEPData] player];
    __block EPPlaylistCollectionCell *weakSelf = self;
    
    
    
    RACSignal *myPlayerState_Signal = [RACObserve([[EPData sharedEPData] player], state) filter:^BOOL(id x) {
        
        STKAudioPlayerState state = [x intValue];
        NSString *urlStr = [[NSString alloc] init];
        
        if (state == STKAudioPlayerStateBuffering && weakPlayer.pendingQueueCount>0) {
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.pendingQueue[0]];
        }else{
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId];
        }
        
        
        return [urlStr isEqualToString:[weakSelf streamUrl]];
        
    }];
    
    RAC(self.playerButton, playerState) = [RACSignal combineLatest:@[
                                                                     [myPlayerState_Signal distinctUntilChanged],
                                                                     [RACObserve(weakPlayer, delegate) distinctUntilChanged]
                                                                     ]
                                                            reduce:^id{
                                                                STKAudioPlayerState state;
                                                                
                                                                if (![weakPlayer.delegate isEqual:weakSelf]) {
                                                                    state = STKAudioPlayerStateStopped;
                                                                }else{
                                                                    state = weakPlayer.state;
                                                                }
                                                                
                                                                return @(state);
                                                            }];
    
    

    
    [[self.playerButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        if (   self.playerButton.playerState == STKAudioPlayerStatePlaying
            || self.playerButton.playerState == STKAudioPlayerStateBuffering)
        {
            
            [weakPlayer stop ];
            [weakPlayer setDelegate:nil];
            
        }else{
            NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];

            [weakPlayer setDelegate:weakSelf];
            [weakPlayer playURL:url ];

            
        }
        
    }];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initDefaults];
    }
    return self;
}

-(void) initDefaults{
    
    
}

//- (NSString *) streamUrl{
//    return plItem.track.itn_previewUrl;
//}




/// Raised when an item has started playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId{}
/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId{}
/// Raised when the state of the player has changed
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState{}
/// Raised when an item has finished playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration{}
/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode{}


@end
