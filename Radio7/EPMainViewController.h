//
//  EPMainViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/9/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "EPCarouselViewController.h"

@interface EPMainViewController : JASidePanelController<UIAlertViewDelegate>{
}

@property (nonatomic, strong) EPCarouselViewController *carousrlController;

@end
