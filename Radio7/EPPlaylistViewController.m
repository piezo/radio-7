//
//  EPPlaylistViewController.m
//  Europa Plus
//
//  Created by Piezo on 2/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPPlaylistViewController.h"
#import "EPChannelScrollViewController.h"
#import "EPData.h"
#import "UIButton+tintImage.h"
#import "UIImageView+AFNetworkingFadein.h"
#import "UIColor+Utils.h"

@interface EPPlaylistViewController (){

}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *epPlaylist;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topTblConstraint;

@end

@implementation EPPlaylistViewController


-(void)dealloc{
    if ([[[[EPData sharedEPData] player] delegate] isEqual:self]) {
        [[[EPData sharedEPData] player] setDelegate:nil];
    }
    
}

- (void)viewDidLoad
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
    {
        _topTblConstraint.constant = 44;
    }
    
    [self updateEPPlaylist];
    
    self.tableView.backgroundColor=[UIColor clearColor];
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated{
    [self updateEPPlaylist];
    [super viewWillAppear:animated];
}


- (void)setupNotificationObserver
{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(currentControllerUpdated:)
//                                                 name:@"EPChannelControllerUpdated"
//                                               object:nil];
}

-(void)currentControllerUpdated : (NSNotification *) aNotification{
    
    if ([(EPChannelScrollViewController *) aNotification.object isEqual:self.currentController]) {
        [self updateUIAndAnimate:YES];
    }
    
}



-(void) updateUITheme{
    [super updateUITheme];
    
    UIColor *tintColor = [[self currentController] tintColor];
    [_tableView setSeparatorColor:[tintColor colorWithAlphaComponent:.5]];
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIButton *leftNavBtn = (UIButton *) self.navigationItem.leftBarButtonItem.customView;
    
    [leftNavBtn setImageTintColor:tintColor];
    [leftNavBtn setTitleColor:tintColor forState:UIControlStateNormal];
    [leftNavBtn setTitleColor:tintColor forState:UIControlStateHighlighted];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: tintColor};
    
    [self.tableView reloadData];
}


-(EPChannelScrollViewController *) currentController{
    return [[EPData sharedEPData] currentChannelController];
}



-(void) updateEPPlaylist{
    self.epPlaylist = [SitePlaylistItem MR_findAllSortedBy:@"sort" ascending:YES];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _epPlaylist.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PlaylistCell";
    EPPlaylistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    
    SitePlaylistItem * plItem = _epPlaylist[indexPath.row];
    
    UIColor *tintColor = [[self currentController] tintColor];

    cell.delegate=self;
    cell.sitePLItem = plItem;
    cell.artistLabel.text = plItem.artist;
    cell.artistLabel.textColor = tintColor;

    cell.recordingLabel.text = plItem.recording;
    cell.recordingLabel.textColor = tintColor;

    cell.timeLabel.text = plItem.time;
    cell.timeLabel.textColor = [tintColor colorWithAlphaComponent:.5];
    cell.thumbImageView.clipsToBounds=YES;
    
    BOOL isBlack = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:tintColor]<.02;
    [cell.playerButton setTintIsBlack:isBlack];

    cell.item = plItem.mp3;

    if (plItem.mp3 == nil) {
        cell.hidden = YES;
    }

    [cell.thumbImageView setImageWithURL:[NSURL URLWithString:plItem.imgURL] fadein:YES];
    
        
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 79;
}

#pragma mark
#pragma mark EPPlaylistCellDelegate

-(void) didStartPlayInCell : (EPPlaylistCell *) cell{
    
    
    NSString *strUrl = cell.sitePLItem.mp3;
    NSURL *url = [NSURL URLWithString:strUrl];

    [[[EPData sharedEPData] player] setDelegate:self];
    [[[EPData sharedEPData] player] playURL:url];
    
    [self.tableView reloadData];
    
    
}

-(void) didStopPlayInCell : (EPPlaylistCell *) cell{
     [[[EPData sharedEPData] player] stop];
    
    [self.tableView reloadData];
}




#pragma mark
#pragma mark STKAudioPlayerDelegate


/// Raised when an item has started playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId{

}

/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId{

}

/// Raised when the state of the player has changed
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState{
    switch (state) {
        case STKAudioPlayerStateBuffering:
            
            [self.tableView reloadData];
            break;
            
        case STKAudioPlayerStatePlaying:
            
            [self.tableView reloadData];
            break;
            
        case STKAudioPlayerStateStopped:
            
            [self.tableView reloadData];
            
            break;
            
        default:
            break;
    }
}

/// Raised when an item has finished playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration{
    
}

/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode{
    
    CLS_LOG(@"unexpectedError");
}


@end
