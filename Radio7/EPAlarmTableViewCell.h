//
//  EPAlarmTableViewCell.h
//  Europa Plus
//
//  Created by Piezo on 5/27/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPAlarmTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mTextLabel;
@end
