//
//  EPIPADLeftMenuViewController.m
//  Europa Plus
//
//  Created by Piezo on 4/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADLeftMenuViewController.h"
#import "CBAutoScrollLabel.h"
#import <UIViewController+JASidePanel.h>
#import "EPIPADMainViewController.h"
#import "EPIPADChannelViewController.h"


@interface EPIPADLeftMenuViewController ()

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *currentPlayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *currentPlaylistLogo;
@property (strong, nonatomic)  Playlist *playlist;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCnstr;


@end

@implementation EPIPADLeftMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _currentPlayLabel.font =  [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    __block CBAutoScrollLabel *weakCurrentPlayLabel = _currentPlayLabel;
    __block UIImageView *weakCurrentPlaylistLogo = _currentPlaylistLogo;
    
    
    
    [RACObserve(self, playlist) subscribeNext:^(Playlist *x) {
        weakCurrentPlaylistLogo.image=[UIImage imageNamed:x.logo];
    }];
    
    
    [[RACObserve(self, playlist.lastItem.track) map:^id(Track *value) {
        return [NSString stringWithFormat:@"%@ - %@", value.artist, value.song];
    }] subscribeNext:^(NSString *x) {
        if (![weakCurrentPlayLabel.text isEqualToString:x]) {
            [weakCurrentPlayLabel setText:x refreshLabels:YES];
        }
        
    }];
    
}



-(void)updateViewConstraints{
    [super updateViewConstraints];
    self.topCnstr.constant = 140.0 - self.topLayoutGuide.length;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white
{
    
    self.playlist = playlist;
    
    [super updateWithPlaylist:playlist tintColor:tintColor aTintColor:aTintColor white:white];

    
    
}

- (IBAction)vkBtnDidPress:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://vk.com/radio7"]];
    
    NSURL *a_url = [NSURL URLWithString:@"vk://vk.com/radio7"];
    NSURL *w_url = [NSURL URLWithString:@"http://vk.com/radio7"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }
}

- (IBAction)fbBtnDidPress:(id)sender {
    
    NSURL *a_url = [NSURL URLWithString:@"fb://profile/185681461468828"];
    NSURL *w_url = [NSURL URLWithString:@"https://www.facebook.com/Radio7Moscow"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }
    
}

- (IBAction)twBtnDidPress:(id)sender {
    
    
    NSURL *a_url = [NSURL URLWithString:@"twitter://user?127845956"];
    NSURL *w_url = [NSURL URLWithString:@"https://twitter.com/radio7msk"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }
    
}

- (IBAction)ytBtnDidPress:(id)sender {
    NSURL *a_url = [NSURL URLWithString:@"youtube://user/EuropaPlusMsk"];
    NSURL *w_url = [NSURL URLWithString:@"http://www.youtube.com/user/EuropaPlusMsk"];
    
    if ([[UIApplication sharedApplication] canOpenURL:a_url]) {
        [[UIApplication sharedApplication] openURL:a_url];
    }else{
        [[UIApplication sharedApplication] openURL:w_url];
    }
    
}

- (IBAction)webBtnDidPress:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://radio7.ru"]];
}

- (IBAction)switchChannel:(id)sender {
    
    
    BOOL currentlyCenterISChannel = [[(EPIPADMainViewController *) [self sidePanelController] centerPanel] isKindOfClass:[EPIPADChannelViewController class]];
    
    if (currentlyCenterISChannel) {
       [(EPIPADMainViewController *) [self sidePanelController] switchChannel:self];
    }else{
       [(EPIPADMainViewController *) [self sidePanelController] switchToChannel];
    }
    
}
- (IBAction)switchToChannel:(id)sender {
    [(EPIPADMainViewController *) [self sidePanelController] switchToChannel];
}


- (IBAction)switchToNovetly:(id)sender {
    [(EPIPADMainViewController *) [self sidePanelController] switchToNovetly];
}

- (IBAction)switchToAlarm:(id)sender {
    [(EPIPADMainViewController *) [self sidePanelController] switchToAlarm];
}

@end
