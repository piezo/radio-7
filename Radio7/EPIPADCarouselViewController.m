//
//  EPIPADCarouselViewController.m
//  Europa Plus
//
//  Created by Piezo on 5/22/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADCarouselViewController.h"
#import "EPData.h"
#import "EPIPADCoverViewController.h"

@interface EPIPADCarouselViewController ()

@property (weak, nonatomic) IBOutlet iCarousel *carouselView;
@end

@implementation EPIPADCarouselViewController{
    NSArray *_playlists;
    NSMutableArray *_coversCntrls;
    int c;
}


- (void)dealloc
{

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _carouselView.bounceDistance = .5;
    _carouselView.decelerationRate = .9;
    self.view.backgroundColor = [UIColor clearColor];
    
    Playlist *c_playlist = [[EPData sharedEPData] currentPlaylist];
    
    if (c_playlist) {
        NSInteger idx = [_playlists indexOfObject:c_playlist];
        _carouselView.currentItemIndex = idx;

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark
#pragma mark iCarouselDataSource

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    if (_playlists == nil) {
        _playlists = [Playlist MR_findAllSortedBy:@"priotiry" ascending:NO];
        
        _coversCntrls = [NSMutableArray arrayWithCapacity:42];
        
        [_playlists enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
           
            EPIPADCoverViewController *coverCntr = [[EPIPADCoverViewController alloc]
                                                    initWithNibName:@"EPIPADCoverViewController"
                                                    bundle:[NSBundle mainBundle]];
            
            coverCntr.playlist = obj;
            coverCntr.view.backgroundColor=[UIColor clearColor];
            
            [_coversCntrls addObject:coverCntr];
            
        }];
        
    }
    return _playlists.count;
}
- (UIView *)carousel:(iCarousel *)carousel
  viewForItemAtIndex:(NSUInteger)index
         reusingView:(UIView *)view
{
    return [_coversCntrls[index] view];
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option) {
        case iCarouselOptionSpacing:
            value = 1.13;
            break;
            
        case iCarouselOptionVisibleItems:
            value = (CGFloat) _playlists.count;
            break;
            
        default:
            break;
    }
    
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    Playlist *playlist = _playlists[index];
    
    if (_delegate ) {
        [_delegate didSelectCannel:playlist];
    }
}


@end
