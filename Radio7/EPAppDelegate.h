//
//  EPAppDelegate.h
//  Europa Plus
//
//  Created by Piezo on 2/4/14.
//


#import <UIKit/UIKit.h>

@class BackgroundTask;
@interface EPAppDelegate : UIResponder <UIApplicationDelegate>{
//    BackgroundTask * bgTask;
}

@property (nonatomic, strong) BackgroundTask *bgTask;
@property (strong, nonatomic) UIWindow *window;

-(void) backgroundCallback:(id)info;
-(void) applicationDidReceiveAlarm:(NSTimer *)sender;

@end