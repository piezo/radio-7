//
//  EPPlayIButton.m
//  Europa Plus
//
//  Created by Piezo on 5/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPPlayIButton.h"

@implementation EPPlayIButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initDefaults];
    }
    return self;
}

-(void) initDefaults{
    _playerState = 0;
    _progress = 0;
    _bufferRotFraction = 0;
    _continuousPlaying = YES;

    
    self.backgroundColor = [UIColor clearColor];
    
    __block EPPlayIButton *weakSelf = self;

    
    POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    anim.fromValue = @(1.0);
    anim.toValue = @(0.0);
    anim.duration = 1;
    anim.property = [POPAnimatableProperty propertyWithName:@"buferRotFraction"
                                                initializer:^(POPMutableAnimatableProperty *prop) {
                                                    // read value
                                                    prop.readBlock = ^(id obj, CGFloat values[]) {
                                                        values[0] = [obj bufferRotFraction];
                                                    };
                                                    // write value
                                                    prop.writeBlock = ^(id obj, const CGFloat values[]) {
                                                        [obj setBufferRotFraction:values[0]];
                                                    };
                                                    // dynamics threshold
                                                    prop.threshold = 0.01;
                                                }];
    
    [anim setCompletionBlock:^(POPAnimation *anim, BOOL complete) {

        if (complete) {
            [weakSelf pop_addAnimation:anim forKey:@"buferRotFraction"];
        }
        
        
    }];
    
    [[RACObserve(self, highlighted) distinctUntilChanged] subscribeNext:^(id x) {
        dispatch_async(dispatch_get_main_queue(), ^{
               [self setNeedsDisplay];
        });
    }];
    

    [[RACObserve(self, playerState) distinctUntilChanged] subscribeNext:^(id x) {
        
        STKAudioPlayerState state =  [x intValue];

//        dispatch_async(dispatch_get_main_queue(), ^{
//            weakSelf.enabled = state != STKAudioPlayerStateBuffering;
//        });
        
        
        if ( state == STKAudioPlayerStateBuffering ) {
            [weakSelf pop_addAnimation:anim forKey:@"buferRotFraction"];
        }else{
            if ([weakSelf pop_animationKeys] && [[weakSelf pop_animationKeys] count]>0) {
                [weakSelf pop_removeAllAnimations];
            }

        }
    }];
    

   
    
    
}

- (void)drawRect:(CGRect)rect
{
    
    [EPStyleKit drawPlayBtnWithState:_playerState
                         tintIsBlack:_tintIsBlack
                          highligted:self.highlighted
                   continuousPlaying:_continuousPlaying
                            progress:_progress
                   bufferRotFraction:_bufferRotFraction];
    
}


-(CABasicAnimation *)makeAnimationForKey:(NSString *)key {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:key];
	anim.fromValue = [[self.layer presentationLayer] valueForKey:key];
	anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
	anim.duration = [CATransaction animationDuration];
    
	return anim;
}

//----------------------------------------------------------
// - setPlayerState:
//----------------------------------------------------------
- (void)setPlayerState:(STKAudioPlayerState)aPlayerState
{
    if (_playerState != aPlayerState) {
        _playerState = aPlayerState;

        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });
        
    }
}


//----------------------------------------------------------
// - setContinuousPlaying:
//----------------------------------------------------------
- (void)setContinuousPlaying:(BOOL)flag
{
    if (_continuousPlaying != flag) {
        _continuousPlaying = flag;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });
    }
}

//----------------------------------------------------------
// - setProgress:
//----------------------------------------------------------
- (void)setProgress:(CGFloat)aProgress
{
    if (_progress != aProgress) {
        _progress = aProgress;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });
    }
}

//----------------------------------------------------------
// - setBufferRotFraction:
//----------------------------------------------------------
- (void)setBufferRotFraction:(CGFloat)aBufferRotFraction
{
    if (_bufferRotFraction != aBufferRotFraction) {
        _bufferRotFraction = aBufferRotFraction;

        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });
    }
}

//----------------------------------------------------------
// - setTintIsBlack:
//----------------------------------------------------------
- (void)setTintIsBlack:(BOOL)flag
{
    if (_tintIsBlack != flag) {
        _tintIsBlack = flag;

        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });
    }
}




@end
