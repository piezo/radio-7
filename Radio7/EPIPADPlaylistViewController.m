//
//  EPIPADPlaylistViewController.m
//  Europa Plus
//
//  Created by Piezo on 5/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADPlaylistViewController.h"

@interface EPIPADPlaylistViewController ()

@end

@implementation EPIPADPlaylistViewController{
    NSArray *_sortedPLItems;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    
    
    
    [super viewDidLoad];
}

-(NSArray *) sortedPLItems{
    if (_sortedPLItems==nil || _sortedPLItems.count==0) {
        
        if (_delegate) {
            _sortedPLItems = [_delegate playlistCntr:self sortedPLItemsWithRemFirst:YES];
        }
        
    }
        
    return _sortedPLItems;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [[self sortedPLItems] count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EPPlaylistCollectionCell *cell = (EPPlaylistCollectionCell *) [cv dequeueReusableCellWithReuseIdentifier:@"PlaylistCell"
                                                                                                forIndexPath:indexPath];
    
    if (_delegate) {
        [_delegate playlistCntr:self fillupCell:cell atIndex:indexPath];
    }
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize retval = CGSizeMake(660/2, 128);
    retval.width = collectionView.frame.size.width/2;
    return retval;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

//----------------------------------------------------------
// - setPlaylist:
//----------------------------------------------------------
- (void)setPlaylist:(Playlist *)aPlaylist
{
    _sortedPLItems = nil;
    if (_playlist != aPlaylist) {
        _playlist = aPlaylist;
    }
    
}



@end
