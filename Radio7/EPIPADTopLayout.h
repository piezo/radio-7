//
//  EPIPADTopLayout.h
//  Europa Plus
//
//  Created by Piezo on 5/31/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPIPADTopLayout : NSObject<UILayoutSupport>{
    
}

@property(nonatomic,readonly) CGFloat length;

@end
