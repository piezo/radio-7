//
//  EPIPADStyledViewController.m
//  Europa Plus
//
//  Created by Piezo on 5/1/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADStyledViewController.h"
#import "UIButton+tintImage.h"
#import "UIImage+Tint.h"

@interface EPIPADStyledViewController ()

@end

@implementation EPIPADStyledViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white
{
    
    //    CGFloat _radius = 4;
    //    CGFloat _opacity = .2;
    
    [_themeLabels enumerateObjectsUsingBlock:^(id label, NSUInteger idx, BOOL *stop) {
        if ([label respondsToSelector:@selector(setTextColor:)]) {
            [label performSelector:@selector(setTextColor:) withObject:tintColor];
        }
        
        //        label.layer.shadowColor = [aTintColor CGColor];
        //        label.layer.shadowOffset= CGSizeMake(0, 0);
        //        label.layer.shadowRadius=_radius;
        //        label.layer.shadowOpacity=_opacity;
    }];
    
    
    [_themeBtns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
        [btn setImageTintColor:tintColor];
        
        [btn setTitleColor:tintColor forState:UIControlStateNormal];
        [btn setTitleColor:tintColor forState:UIControlStateHighlighted];
        [btn setTitleColor:tintColor forState:UIControlStateSelected];

        //        btn.layer.shadowColor = [aTintColor CGColor];
        //        btn.layer.shadowOffset= CGSizeMake(0, 0);
        //        btn.layer.shadowRadius=_radius;
        //        btn.layer.shadowOpacity=_opacity;
        
    }];
    
    [_themeImages enumerateObjectsUsingBlock:^(UIImageView *imgView, NSUInteger idx, BOOL *stop) {
        
        if (imgView.tag==101) {
            imgView.image = [[UIImage imageNamed:@"menu_delimiters"] imageTintedWithColor:tintColor];
        }


    }];
    
}

@end
