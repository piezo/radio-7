//
//  EPChannelTableViewController.m
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPChannelTableViewController.h"
#import "UIColor+Utils.h"
#import "EPPlaylistCell.h"
#import "EPPlayIButton.h"
#import "UIImageView+AFNetworking.h"

@interface EPChannelTableViewController ()


@end

@implementation EPChannelTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sortedPLItems = [NSMutableArray arrayWithCapacity:42];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white
{

    _playlist = playlist;
    _tintColor = tintColor;
    _aTintColor = aTintColor;
    _white = white;

    _sortedPLItems = nil;
    if (_sortedPLItems==nil) {
        NSSortDescriptor *time_start_Descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time_start" ascending:NO];
        NSArray *sorted = [[_playlist items] sortedArrayUsingDescriptors:[NSArray arrayWithObject:time_start_Descriptor]];
        
        NSMutableArray *msorted = [NSMutableArray arrayWithArray:sorted];
        
        if (msorted.count>0) {
            [msorted removeObjectAtIndex:0];
        }
        
        self.sortedPLItems = msorted;
        
    }
    
    
    [self.tableView setSeparatorColor:[_tintColor colorWithAlphaComponent:.2]];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    [self.tableView reloadData];
    
}


#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self sortedPLItems] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PlaylistCell";
    __block EPPlaylistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    PlaylistItem * plItem = [self sortedPLItems][indexPath.row];
    
    
    UIColor *tintColor = [self tintColor];
    
    cell.artistLabel.text = plItem.track.artist;
    cell.artistLabel.textColor = tintColor;
    
    
    cell.recordingLabel.text = plItem.track.song;
    cell.recordingLabel.textColor = tintColor;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    cell.timeLabel.text = [formatter stringFromDate:plItem.time_start];
    cell.timeLabel.textColor = tintColor;
    
    cell.thumbImageView.clipsToBounds=YES;
    
    
    
    BOOL isBlack = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:tintColor]<.02;
    [cell.playerButton setTintIsBlack:isBlack];
    cell.playerButton.hidden = YES;
    
    
    
    cell.thumbImageView.image = [[UIImage alloc] init];
    
    if (![plItem.track.itn_complete boolValue] ) {
        
        [[EPWebsiteDataHelper shared] searchTrackInItunes:plItem.track complete:^(BOOL success, Track *aTrack) {
            if (success) {
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:aTrack.itn_artworkUrl100]];
                if (plItem.track.itn_previewUrl) {
                    cell.item = plItem.track.itn_previewUrl;
                    cell.playerButton.hidden = NO;
                }
                
            }
        }];
        
    }else{
        
        if (plItem.track.itn_previewUrl) {
            cell.item = plItem.track.itn_previewUrl;
            cell.playerButton.hidden = NO;
        }
        
        if (plItem.track.cover.img) {
            [cell.thumbImageView setImage:[UIImage imageWithData:plItem.track.cover.img]];
        }else if(plItem.track.itn_artworkUrl100){
            [cell.thumbImageView setImageWithURL:[NSURL URLWithString:plItem.track.itn_artworkUrl100]];
        }
        
    }
    
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 79;
}

@end
