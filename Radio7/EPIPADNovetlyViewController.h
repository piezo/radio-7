//
//  EPIPADNovetlyViewController.h
//  Europa Plus
//
//  Created by Piezo on 5/26/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADPlaylistViewController.h"
#import "EPIPADStyledViewController.h"

@interface EPIPADNovetlyViewController : EPIPADStyledViewController<EPIPADPlaylistViewControllerDelegate>

@end
