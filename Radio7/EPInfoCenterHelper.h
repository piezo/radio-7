//
//  EPInfoCenterHelper.h
//  Europa Plus
//
//  Created by Piezoon 2/9/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@class EPChannelScrollViewController;
@interface EPInfoCenterHelper : NSObject

+(EPInfoCenterHelper *) sharedEPInfoCenterHelper;
-(void) updtateInfoWithController : (EPChannelScrollViewController *) aController;
-(void) updtateInfoWithPlaylist: (Playlist *) _playlist cover:(UIImage *) cover white : (CGFloat) white;
@end
