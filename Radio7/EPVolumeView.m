//
//  EPVolumeView.m
//  Europa Plus
//
//  Created by Piezo on 6/1/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPVolumeView.h"
#import "UIColor+Utils.h"

@implementation EPVolumeView



-(void)awakeFromNib{
    [self setBackgroundColor:[UIColor clearColor]];
    [self setShowsRouteButton:NO];

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImage *volumeThumbImage = [EPStyleKit imageOfVolumeThumb];
        [self setVolumeThumbImage:volumeThumbImage forState:UIControlStateNormal];
                
        BOOL tintIsBlack = NO;
        [self tintImages:tintIsBlack];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        UIImage *volumeThumbImage = [EPStyleKit imageOfVolumeThumb];
        [self setVolumeThumbImage:volumeThumbImage forState:UIControlStateNormal];
    
        
        BOOL tintIsBlack = NO;
       [self tintImages:tintIsBlack];
    }
    return self;
}

- (void)tintImages:(BOOL)tintIsBlack {
    UIImage *minimumVolumeSliderImage = [self imageOfMinimumVolumeSliderWithTintIsBlack:tintIsBlack];
    UIImage *maximumVolumeSliderImage = [self imageOfMaximumVolumeSliderWithTintIsBlack:tintIsBlack];
    UIImage *routeButtonImage_normal = [EPStyleKit imageOfRouteButtonWithTintIsBlack:tintIsBlack highligted:NO];
    UIImage *routeButtonImage_Highlighted = [EPStyleKit imageOfRouteButtonWithTintIsBlack:tintIsBlack highligted:YES];
    
    [self setMinimumVolumeSliderImage:minimumVolumeSliderImage forState:UIControlStateNormal];
    [self setMaximumVolumeSliderImage:maximumVolumeSliderImage forState:UIControlStateNormal];
    [self setRouteButtonImage:routeButtonImage_normal forState:UIControlStateNormal];
    [self setRouteButtonImage:routeButtonImage_Highlighted forState:UIControlStateHighlighted];
}

//----------------------------------------------------------
// - setTintColor:
//----------------------------------------------------------
- (void)setTintColor:(UIColor *)aTintColor
{
    if (_tintColor != aTintColor) {
        _tintColor = aTintColor;
        
        BOOL tintIsBlack = [UIColor findDistanceBetweenTwoColor:_tintColor secondColor:[UIColor blackColor]]<.02;
        
        [self tintImages:tintIsBlack];
    }
}


- (UIImage*)imageOfMinimumVolumeSliderWithTintIsBlack: (BOOL)tintIsBlack;
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(224, 8), NO, 0.0f);
    [EPStyleKit drawMinimumVolumeSliderWithTintIsBlack: tintIsBlack];
    UIImage* imageOfMinimumVolumeSlider = UIGraphicsGetImageFromCurrentImageContext();
    
    if (IS_OS_7_OR_LATER) {
        imageOfMinimumVolumeSlider = [[imageOfMinimumVolumeSlider resizableImageWithCapInsets: UIEdgeInsetsMake(0, 2, 0, 2) resizingMode: UIImageResizingModeStretch] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
        
    }else{
        imageOfMinimumVolumeSlider = [imageOfMinimumVolumeSlider stretchableImageWithLeftCapWidth:2 topCapHeight:0];
    }
    
    UIGraphicsEndImageContext();
    
    return imageOfMinimumVolumeSlider;
}

- (UIImage*)imageOfMaximumVolumeSliderWithTintIsBlack: (BOOL)tintIsBlack;
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(224, 8), NO, 0.0f);
    [EPStyleKit drawMaximumVolumeSliderWithTintIsBlack: tintIsBlack];
    
    UIImage* imageOfMaximumVolumeSlider = UIGraphicsGetImageFromCurrentImageContext();;
    
    if (IS_OS_7_OR_LATER) {
        imageOfMaximumVolumeSlider = [[imageOfMaximumVolumeSlider resizableImageWithCapInsets: UIEdgeInsetsMake(0, 2, 0, 2) resizingMode: UIImageResizingModeTile] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }else{
        imageOfMaximumVolumeSlider = [imageOfMaximumVolumeSlider stretchableImageWithLeftCapWidth:2 topCapHeight:0];
    }
    UIGraphicsEndImageContext();
    
    return imageOfMaximumVolumeSlider;
}

@end
