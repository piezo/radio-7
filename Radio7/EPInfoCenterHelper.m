//
//  EPInfoCenterHelper.m
//  Europa Plus
//
//  Created by Piezo on 2/9/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPInfoCenterHelper.h"
#import "EPChannelScrollViewController.h"

@implementation EPInfoCenterHelper

static EPInfoCenterHelper *sharedEPInfoCenterHelperInstance = nil;

+(EPInfoCenterHelper *) sharedEPInfoCenterHelper
{
    @synchronized(self) {
        if (sharedEPInfoCenterHelperInstance == nil) {
            sharedEPInfoCenterHelperInstance = [[self alloc] init];
        }
        return sharedEPInfoCenterHelperInstance;
    }
}

-(void) updtateInfoWithController : (EPChannelScrollViewController *) aController{
    
    Playlist *_playlist  = aController.playlist;
    
    if (_playlist.lastItem) {
        MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
        infoCenter.nowPlayingInfo = @{
                                      MPMediaItemPropertyTitle : _playlist.title,
                                      MPMediaItemPropertyArtist :[NSString stringWithFormat:@"%@ - %@",_playlist.lastItem.track.song, _playlist.lastItem.track.artist],
                                      MPMediaItemPropertyArtwork : [[MPMediaItemArtwork alloc] initWithImage:[aController coverScreenshot]],
                                      MPMediaItemPropertyPlaybackDuration : @(0)
                                      };
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EPCurrentControllerUpdated"
                                                            object:nil
                                                          userInfo:@{@"controller" : aController}];
        
    }    
    
}

-(void) updtateInfoWithPlaylist: (Playlist *) _playlist cover:(UIImage *) cover white : (CGFloat) white{

    if (_playlist.lastItem && cover!=nil) {
        MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
        infoCenter.nowPlayingInfo = @{
                                      MPMediaItemPropertyTitle : _playlist.title,
                                      MPMediaItemPropertyArtist :[NSString stringWithFormat:@"%@ - %@",_playlist.lastItem.track.song, _playlist.lastItem.track.artist],
                                      MPMediaItemPropertyArtwork : [[MPMediaItemArtwork alloc] initWithImage:cover],
                                      MPMediaItemPropertyPlaybackDuration : @(0)
                                      };
        
    }
}


@end
