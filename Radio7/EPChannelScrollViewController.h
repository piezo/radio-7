//
//  EPChannelScrollViewController.h
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPChannelControllerDelegate.h"
#import "EPCarouselViewController.h"


@class EPChannelInfoViewController;

@interface EPChannelScrollViewController : UIViewController<UIScrollViewDelegate, EPChannelControllerDelegate>{
//    id<EPChannelControllerDelegate> delegate;
//    UIImage *bgImg;
}

@property (nonatomic, weak) id<EPChannelControllerDelegate> delegate;

@property (nonatomic, strong) Playlist *playlist;
@property (nonatomic, strong) EPChannelInfoViewController *infoCntr;
@property (nonatomic, strong) UIImage *bgImg;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, strong) UIColor *aTintColor;
@property (nonatomic, assign) CGFloat white;


-(UIImage *) coverScreenshot;

-(void) hideUI;
-(void) showUI;



@end
