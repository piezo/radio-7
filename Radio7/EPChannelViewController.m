//
//  EPChannelViewController.m
//  Europa Plus
//
//  Created by Piezo on 2/4/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPChannelViewController.h"
#import "EPCarouselViewController.h"
#import "FXBlurView.h"
#import "PRTween.h"
#import "EPData.h"
#import "UIView+ColorOfPoint.h"
#import "UIButton+tintImage.h"
#import "UIColor+Grayscale.h"
#import "UIView+Screenshot.h"
#import "EPInfoCenterHelper.h"
#import "EPRecorder.h"
#import "UIImageView+AFNetworking.h"
#import "EPChannelScrollView.h"
#import "UIColor+Utils.h"
#import "EPVolumeView.h"
#import "EPChannelScrollViewController.h"

const CGFloat coverWidthMin = 110;


@interface EPChannelViewController (){
    BOOL _canUpdate;
    BOOL _needsUpdate;
    BOOL _needsScreenshotUpdate;
    
    BOOL _UIHidden;
    BOOL _UINeedHidden;
    
    UIButton *_currentPlayBtn;
    UIButton *_currentStopBtn;
    CGFloat topYOffset;
}


@property (weak, nonatomic) IBOutlet EPChannelScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UILabel *channelHeaderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@property (weak, nonatomic) UIView *bgContainerView;
@property (weak, nonatomic) UIImageView *bgCoverImageView;
@property (weak, nonatomic) UIImageView *bgNoCoverImageView;
@property (weak, nonatomic) FXBlurView *bgBlurView;
@property (weak, nonatomic) IBOutlet EPVolumeView *volumeView;
@property (weak, nonatomic) IBOutlet UIView *volumeViewPlaceholder;

@property (weak, nonatomic) IBOutlet UIButton *itunesBtn;
@property (weak, nonatomic) IBOutlet UIButton *starBtn;


@property (weak, nonatomic) IBOutlet UIButton *btrBtn32;
@property (weak, nonatomic) IBOutlet UIButton *btrBtn64;
@property (weak, nonatomic) IBOutlet UIButton *btrBtn128;
@property (weak, nonatomic) IBOutlet UIButton *btrBtnHD;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIView *titlesView;
@property (weak, nonatomic) IBOutlet UIView *controlsView;

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *channalBtns;

@property (weak, nonatomic) IBOutlet UIImageView *logo1;
@property (weak, nonatomic) IBOutlet UIImageView *logo2;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UIView *vview;
@property (weak, nonatomic) UIView *previewImageView;

@property (nonatomic, strong) NSArray *sortedPLItems;

@property (weak, nonatomic) IBOutlet EPPlayIButton *playerButton;


#pragma mark
#pragma mark Constraints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerBottomConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverLeftConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titlesLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titlesToCoverVSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsToCoverVSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titlesWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itnsBtnRightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *plTableViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *plTableViewHeightConstraint;


#pragma mark
#pragma mark PlaylistTableView

@property (weak, nonatomic) IBOutlet UITableView *playlistTableView;

@end

@implementation EPChannelViewController

- (void)dealloc
{
    [_playlist removeObserver:self forKeyPath:@"lastItem.track.cover.img"];
    [_playlist removeObserver:self forKeyPath:@"llastItem.track.itn_complete"];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _canUpdate = YES;
    }
    return self;
}


- (void)viewDidLoad
{

    
    self.view.clipsToBounds=YES;
    
    CGFloat sideSize = (self.view.height -_coverView.center.y)*2;

    UIImageView *bgNoCoverImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tmp_bg"]];
    bgNoCoverImageView.hidden=NO;

    UIImageView *bgCoverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, sideSize, sideSize)];
    bgCoverImageView.center = _coverView.center;

    [FXBlurView setBlurEnabled:YES];
    [FXBlurView setUpdatesEnabled];
    
    FXBlurView *bgBlurView = [[FXBlurView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
//    [bgBlurView setTintColor:[UIColor clearColor]];
    [bgBlurView setBlurRadius:120.0];
    [bgBlurView setDynamic:NO];
    
    UIView *bgContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bgContainerView.hidden = YES;
    
    [bgContainerView addSubview:bgCoverImageView];
    [bgContainerView addSubview:bgBlurView];
    
    [[self view] insertSubview:bgNoCoverImageView atIndex:0];
    [[self view] insertSubview:bgContainerView atIndex:0];
    
    _coverView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _coverView.layer.shadowRadius=15;
    _coverView.layer.shadowOpacity=.1;
    _coverView.layer.shadowOffset=CGSizeMake(0, 0);
    
    _bgContainerView = bgContainerView;
    _bgCoverImageView = bgCoverImageView;
    _bgBlurView = bgBlurView;
    _bgNoCoverImageView = bgNoCoverImageView;
    
    _coverImageView.image = [UIImage imageNamed:@"no_cover"];
    
    _bgNoCoverImageView.image = [UIImage imageNamed:@"tmp_bg"];
    _bgNoCoverImageView.contentMode = UIViewContentModeBottom;
    _bgNoCoverImageView.frame = self.view.frame;
    
    _artistLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:19];
    _artistLabel.labelSpacing = 35; // distance between start and end labels
    _artistLabel.pauseInterval = 0; // seconds of pause before scrolling starts again
    _artistLabel.scrollSpeed = 30; // pixels per second
    _artistLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    _artistLabel.fadeLength = 12.f; // length of the left and right edge fade, 0 to disable
    _artistLabel.text = @" ";

    _recordingLabel.text = @" ";
    _recordingLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    _recordingLabel.labelSpacing = 35; // distance between start and end labels
    _recordingLabel.pauseInterval = 0; // seconds of pause before scrolling starts again
    _recordingLabel.scrollSpeed = 30; // pixels per second
    _recordingLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    _recordingLabel.fadeLength = 12.f; // length of the left and right edge fade, 0 to disable
    _recordingLabel.text = @" ";
    
    _channelHeaderLabel.text = @" ";
    
    
    _logo2.hidden=YES;
    _canUpdate = YES;
    
    
    _titlesView.backgroundColor=[UIColor clearColor];
    _controlsView.backgroundColor=[UIColor clearColor];

    _volumeViewPlaceholder.backgroundColor = _volumeView.backgroundColor=[UIColor clearColor];
    _volumeView.translatesAutoresizingMaskIntoConstraints = YES;
    _volumeView.showsRouteButton=YES;
    
    [_itunesBtn setTitle:@"" forState:UIControlStateNormal];
    
    if (IS_OS_7_OR_LATER) {
        topYOffset=44;
    }else{
        topYOffset=29;
    }
    
    
    __block EPChannelViewController *weakSelf = self;
    __block STKAudioPlayer *weakPlayer = [[EPData sharedEPData] player];

    
//    RACSignal *playlistChanged_Signal  = [RACObserve(self, playlist) distinctUntilChanged] ;
    RACSignal *plitem_complete_signal = [RACObserve(self, playlist.lastItem.complete) distinctUntilChanged] ;
    RACSignal *preferredStreamChanged_Signal  = [RACObserve(self, playlist.preferredStream) distinctUntilChanged] ;
//    RACSignal *player_delegate_signal = [RACObserve([[EPData sharedEPData] player], delegate) distinctUntilChanged];

    [plitem_complete_signal subscribeNext:^(id x) {
        
        if ([x boolValue]) {
            weakSelf.plItem=weakSelf.playlist.lastItem;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf updateUIAndAnimate:[[[EPData sharedEPData] currentChannelController] isEqual:weakSelf]];
            });
            
            weakSelf.sortedPLItems=nil;
            
        }

    }];
    
    
    RACSignal *myPlayerState_Signal = [RACObserve([[EPData sharedEPData] player], state) filter:^BOOL(id x) {
        
        STKAudioPlayerState state = [x intValue];
        NSString *urlStr = [[NSString alloc] init];
        
        if (state == STKAudioPlayerStateBuffering && weakPlayer.pendingQueueCount>0) {
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.pendingQueue[0]];
        }else{
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId];
        }
        
        
        return [urlStr isEqualToString:[weakSelf streamUrl]];
        
    }];
    

    
    [[RACSignal combineLatest:@[
                            
                                preferredStreamChanged_Signal,
                                [RACObserve([EPData sharedEPData ], currentChannelController) distinctUntilChanged],
                               
                                ]] subscribeNext:^(RACTuple *x) {
        
        
        if([x.second isEqual:weakSelf]){
            
        
            
            BOOL stateIsPlaPlaying = [[[EPData sharedEPData] player] state] == STKAudioPlayerStatePlaying;
            BOOL playerIsMy = ( weakPlayer.delegate && [weakPlayer.delegate isKindOfClass:[weakSelf class]] );
            BOOL currentlyPlayingIsNotMy = ![[NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId] isEqualToString:[weakSelf streamUrl]];
            
            
            if ( stateIsPlaPlaying
                && playerIsMy
                && currentlyPlayingIsNotMy)
            {
                NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];
                [weakPlayer setDelegate:weakSelf];
                [weakPlayer playURL:url];
            }
            
        }
        
        
    }];
    
    

    
    RAC(self.playerButton, playerState) = [RACSignal combineLatest:@[
                                                                     [myPlayerState_Signal distinctUntilChanged],
                                                                     [RACObserve(weakPlayer, delegate) distinctUntilChanged]
                                                                     ]
                                                            reduce:^id{
                                                                STKAudioPlayerState state;
                                                                
                                                                if (![weakPlayer.delegate isEqual:weakSelf]) {
                                                                    state = STKAudioPlayerStateStopped;
                                                                }else{
                                                                    state = weakPlayer.state;
                                                                }
                                                                
                                                                return @(state);
                                                            }];
    
    
    [[self.playerButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        NSDictionary *dict;
        
        if (   self.playerButton.playerState == STKAudioPlayerStatePlaying
            || self.playerButton.playerState == STKAudioPlayerStateBuffering)
        {
            
            [weakPlayer stop ];
            [weakPlayer setDelegate:nil];
            
            dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                           action:@"button_press"
                                                            label:@"pause"
                                                            value:nil] build];

            
        }else{
            NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];
            [weakPlayer setDelegate:weakSelf];
            [weakPlayer playURL:url ];
            
            
            
            dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                     action:@"button_press"
                                                                     label:@"play"
                                                                      value:nil] build];
            
            
        }
        
        [[[GAI sharedInstance] defaultTracker] send:dict];
        
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self scrollViewDidScroll:_mScrollView];
    });

  
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (NSString *) streamUrl{
    return [_playlist valueForKey:[_playlist preferredStream]];
}


#pragma mark - Screenshot


-(UIImage *) coverScreenshot{
    return [_coverView screenshotAfterScreenUpdates:YES];
}

-(UIImage *) viewScreenshot{
    
    if (self.view.needsUpdateConstraints) {
        _needsScreenshotUpdate = YES;
        return nil;
    }else{
        
        
        CGFloat y = _mScrollView.contentOffset.y;
        _mScrollView.contentOffset = CGPointMake(0, 0);
        UIImage* img = [self.view screenshotAfterScreenUpdates:YES];
        
        _mScrollView.contentOffset = CGPointMake(0, y);
        
        return img;
    }

}

#pragma mark
#pragma mark Interface Utils


- (void)updateUIAndAnimate: (BOOL) animate
{
    
    if (!_canUpdate) {
        _needsUpdate = YES;
        return;
    }
    
    
    if (animate) {
        animate = [[[EPData sharedEPData] currentChannelController] isEqual:self];
    }
    
    if (animate && _screenshot == nil) {
        self.screenshot = [self viewScreenshot];
    }
    

    [_artistLabel setText:_plItem.track.artist refreshLabels:YES];
    [_recordingLabel setText:_plItem.track.song refreshLabels:YES];

    _channelHeaderLabel.text = _playlist.title;
    
    _logo1.image = _logo2.image=[UIImage imageNamed:_playlist.logo];
    


    if (!_plItem.track.cover.img) {
        
        _coverImageView.image = [UIImage imageNamed:@"no_cover"];
        _bgNoCoverImageView.image = [UIImage imageNamed:@"tmp_bg"];

        _bgNoCoverImageView.hidden=NO;
        _bgContainerView.hidden = YES;
        
        _logo1.hidden=NO;
        _logo2.hidden=YES;
        
    }else{
        _bgCoverImageView.image = _coverImageView.image = [UIImage imageWithData:_plItem.track.cover.img];

        _bgNoCoverImageView.hidden=YES;
        _bgContainerView.hidden = NO;
        
        _logo2.hidden=NO;
        _logo1.hidden=YES;

    }
    
    
    
    _itunesBtn.hidden = (_plItem.track.itn_collectionViewUrl == nil);
    
    _starBtn.selected =[_plItem.track.favorite boolValue];

    [self updateUITheme];
    [self updateUIControls];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EPChannelControllerUpdated" object:self userInfo:@{@"controller":self}];
    
//    if ([[[EPData sharedEPData] currentChannelController] isEqual:self]) {
//        [[EPInfoCenterHelper sharedEPInfoCenterHelper] updtateInfoWithController:self];
//    }

    
    
    if (animate) {
        UIImageView *imgView= [[UIImageView alloc] initWithImage:_screenshot];
        imgView.userInteractionEnabled=NO;
        [self.view addSubview:imgView];
        
        [UIView animateWithDuration:.25
                              delay:0
                            options:0
                         animations:^{
                             imgView.alpha=0;
                         }completion:^(BOOL finished){
                             [imgView removeFromSuperview];
                             
                             self.screenshot = [self viewScreenshot];
                             
                         }];
        
    }else{
        self.screenshot = [self viewScreenshot];
    }

}

-(void)updateViewConstraints{
    
    [super updateViewConstraints];
    
    _footerBottomConstraint.constant = (_btrBtn32.hidden)?-_footerView.frame.size.height:0;
    
    if (IS_OS_7_OR_LATER) {
        _coverTopConstraint.constant= _mScrollView.contentOffset.y+topYOffset;
    }else{
        _coverTopConstraint.constant= ceilf((_mScrollView.contentOffset.y+topYOffset)*1.5);
    }
    
    
    CGFloat _coverWidth = 300;
    
    if (!IS_IPHONE5) {
        _coverWidth = 220 - _footerBottomConstraint.constant;        
    }
    _coverWidthConstraint.constant = _coverWidth - _mScrollView.contentOffset.y>coverWidthMin?_coverWidth - _mScrollView.contentOffset.y:coverWidthMin;
    
    if (!IS_IPHONE5) {
        _coverLeftConstraint.constant = 10 + (300 - _coverWidthConstraint.constant)/2*(1-fminf(1.0, _mScrollView.contentOffset.y/130));
     
    }
    
    _titlesLeftConstraint.constant=_coverLeftConstraint.constant-10;
    
    if (_needsScreenshotUpdate) {
        self.screenshot = [self viewScreenshot];
        _needsScreenshotUpdate = NO;
    }
    
    
    
    
    _volumeView.center =
    [_volumeView.superview convertPoint: _volumeViewPlaceholder.center
                               fromView: _volumeViewPlaceholder.superview];
    
    
    
}

- (void) updateUIControls{
    
    _btrBtn32.hidden =  [_playlist.stream32 isEqualToString:@""];
    _btrBtn64.hidden =  [_playlist.stream64 isEqualToString:@""];
    _btrBtn128.hidden = [_playlist.stream128 isEqualToString:@""];
    _btrBtnHD.hidden =  [_playlist.streamHD isEqualToString:@""];
    
    
    
    _btrBtn32.selected = [_playlist.preferredStream isEqualToString:@"stream32"];
    _btrBtn64.selected = [_playlist.preferredStream isEqualToString:@"stream64"];
    _btrBtn128.selected = [_playlist.preferredStream isEqualToString:@"stream128"];
    _btrBtnHD.selected = [_playlist.preferredStream isEqualToString:@"streamHD"];
    
    int c = [@(_btrBtn32.hidden) intValue] + [@(_btrBtn64.hidden) intValue] + [@(_btrBtn128.hidden) intValue] + [@(_btrBtnHD.hidden) intValue];
    
    if (c>0) {
        _btrBtn32.hidden = _btrBtn64.hidden = _btrBtn128.hidden = _btrBtnHD.hidden = YES;
    }
    
}

-(void) updateUITheme{

    _titlesView.hidden=YES;
    UIColor *c = [self.view colorOfPoint:CGPointMake(_titlesView.center.x, _titlesView.center.y)];
    _white = 0;
    [c.grayscale getWhite:&_white alpha:nil];
    
    self.tintColor = (_white<=0.65f)?[UIColor whiteColor]:[UIColor blackColor];
    UIColor *aTintColor = (_white<=0.65f)?[UIColor blackColor]:[UIColor whiteColor];
    
    _titlesView.hidden=NO;
    
    [_bgBlurView setNeedsDisplay];

    [_starBtn setImageTintColor:_tintColor];

    [_btrBtnHD setImageTintColor:_tintColor];
    [_btrBtn128 setImageTintColor:_tintColor];
    [_btrBtn64 setImageTintColor:_tintColor];
    [_btrBtn32 setImageTintColor:_tintColor];
    
    _artistLabel.textColor = _recordingLabel.textColor = _channelHeaderLabel.textColor = _tintColor;
    
    [_menuBtn setImageTintColor:_tintColor];
    [_channalBtns setImageTintColor:_tintColor];
    
    [_footerView setBackgroundColor:[aTintColor colorWithAlphaComponent:.1]];
    
    BOOL isBlack = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:_tintColor]<.02;
    [_playerButton setTintIsBlack:isBlack];
    
    UIImage *itunesImage = [EPStyleKit imageOfITunesSmallBadgeWithTintIsBlack:isBlack];
    
    [_itunesBtn setImage:itunesImage forState:UIControlStateNormal];

    [_volumeView setTintColor:_tintColor];

    [_playlistTableView setSeparatorColor:[_tintColor colorWithAlphaComponent:.5]];
    
    if ([_playlistTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_playlistTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }


    [_playlistTableView reloadData];
}

#pragma mark
#pragma mark Actions


- (IBAction)toggleStream:(UIButton *)sender {
    
    if (sender.selected) {
        return;
    }
    
    switch (sender.tag) {
        case 32:
            _playlist.preferredStream = @"stream32";
            break;

        case 64:
            _playlist.preferredStream = @"stream64";
            break;

        case 128:
            _playlist.preferredStream = @"stream128";
            break;
            
        case 256:
            _playlist.preferredStream = @"streamHD";
            break;


        default:
            break;
    }
    
    NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                 action:@"button_press"
                                                                  label:[NSString stringWithFormat:@"toggle_stream_to_%@", _playlist.preferredStream]
                                                                  value:nil] build];
    [[[GAI sharedInstance] defaultTracker] send:dict];
    
    [self updateUIControls];
    
}



- (IBAction)toggleFav:(id)sender {
    
    _playlist.lastItem.track.favorite = @(![_playlist.lastItem.track.favorite boolValue]);
    _starBtn.selected = [_playlist.lastItem.track.favorite boolValue];
    
}



- (IBAction)switchChannel:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(epChannelController:sendAction:)]) {
        [_delegate epChannelController:self sendAction:EPChannelViewControllerActionSwitch];
        
        NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                     action:@"button_press"
                                                                      label:@"Switch Channel"
                                                                      value:nil] build];
        [[[GAI sharedInstance] defaultTracker] send:dict];
        
        
        [MagicalRecord cleanUp];
    }
}
- (IBAction)menuBtnDidPress:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(epChannelController:sendAction:)]) {
        [_delegate epChannelController:self sendAction:EPChannelViewControllerActionMenu];
        
        NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                     action:@"button_press"
                                                                      label:@"Lfft Menu"
                                                                      value:nil] build];
        [[[GAI sharedInstance] defaultTracker] send:dict];
    }
    
}



- (void) hideUI{
    
    if (_UIHidden) {
        return;
    }
    
    
    if ([[[EPData sharedEPData] currentChannelController] isEqual:self]) {
        
        if (_mScrollView.contentOffset.y!=0) {
            
            [_mScrollView setContentOffset:CGPointZero animated:YES];
            
            _UINeedHidden = YES;
            return;
        }
        
    }
    
    _UIHidden = YES;

//    _vview.hidden=YES;
//    _bgContainerView.hidden=YES;
//    _canUpdate = NO;
    
    if (self.previewImageView ==nil) {
        UIView * __previewImageView = [[UIView alloc] initWithFrame:self.view.bounds];
        
        [self.view addSubview:__previewImageView];
        
        self.previewImageView = __previewImageView;
        
        _mScrollView.contentOffset=CGPointMake(0, 0);
    }
    
    
    
    [_recordingLabel refreshLabels];
    [_artistLabel refreshLabels];

}

- (void) showUI{
    
    if (!_UIHidden) {
        return;
    }
    
    _UIHidden = NO;
    
    _mScrollView.contentOffset=CGPointMake(0, 0);
    
    _volumeView.center =
    [_volumeView.superview convertPoint: _volumeViewPlaceholder.center
                               fromView: _volumeViewPlaceholder.superview];


    [_recordingLabel refreshLabels];
    [_artistLabel refreshLabels];
    
    _vview.hidden=NO;
    _bgContainerView.hidden=NO;
    _canUpdate = YES;

    [self.previewImageView removeFromSuperview];
    self.previewImageView = nil;

    
    if (_needsUpdate) {
        _needsUpdate = NO;
        [self updateUIAndAnimate:YES];
    }
    
        self.view.userInteractionEnabled=YES;
}



- (IBAction)showInItunes:(id)sender {
    
    Track *track = _playlist.lastItem.track;
    NSURL *url = [NSURL URLWithString:[track.itn_collectionViewUrl stringByAppendingString:@"&at=10ldCb"]];
    
    [[UIApplication sharedApplication] openURL:url];
    
    NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"iTunes"
                                                                 action:@"Show_In_iTunes"
                                                                  label:[NSString stringWithFormat:@"%@ %@", track.artist, track.song]
                                                                  value:nil] build];
    
    
    [[[GAI sharedInstance] defaultTracker] send:dict];
    

    
    dict = [[GAIDictionaryBuilder createTransactionWithId:[NSString stringWithFormat:@"%@", track.itn_trackId]
                                             affiliation:@"iTunes"
                                                 revenue:track.itn_trackPrice
                                                     tax:@0.0F
                                                shipping:@1
                                            currencyCode:track.itn_currency] build];
    
    [[[GAI sharedInstance] defaultTracker] send:dict];
}





-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"carousel"]) {
        [(EPCarouselViewController *) segue.destinationViewController setIndex:_index];
    }
}


#pragma mark
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([scrollView isEqual:_mScrollView]) {
       
    
        if (scrollView.contentOffset.y < 0) {
            [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
        }else{
            
            if (scrollView.isDecelerating) {
                CGFloat heightLimit = 200+fmaxf(_playlistTableView.contentSize.height,_playlistTableView.frame.size.height)-_playlistTableView.frame.size.height;
                
                if (scrollView.contentOffset.y > heightLimit && scrollView.isDecelerating) {
                    [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, heightLimit)];
                }
            }
            

            [self updateViewConstraints];
            
            CGFloat titlesPerc;
            CGFloat controlsPerc;
            
            if (scrollView.contentOffset.y<130) {
                
                titlesPerc = scrollView.contentOffset.y/130;
                titlesPerc = fminf(1.0, titlesPerc);
                
                controlsPerc = scrollView.contentOffset.y/130;
                controlsPerc = fminf(1.0, controlsPerc);
                
                
                _titlesToCoverVSpace.constant=10-50*titlesPerc;
                _titlesView.alpha=1-titlesPerc;
                
                
                
                _controlsToCoverVSpace.constant=(89)-(124)*controlsPerc;
                _controlsLeftConstraint.constant=0;
                _controlsView.alpha=1-titlesPerc;
                
                _titlesWidthConstraint.constant = 300;
                
                if ( _recordingLabel.textAlignment != NSTextAlignmentCenter) {
                    _recordingLabel.textAlignment=NSTextAlignmentCenter;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [_recordingLabel refreshLabels];
                    });
                }
                
                if ( _artistLabel.textAlignment != NSTextAlignmentCenter) {
                    _artistLabel.textAlignment=NSTextAlignmentCenter;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [_artistLabel refreshLabels];
                    });
                }
                
                _itnsBtnRightConstraint.constant=0;
                
                _volumeView.center = [self.view convertPoint: _volumeViewPlaceholder.center
                                                    fromView: _volumeViewPlaceholder.superview];
                
                
                _volumeView.alpha = _controlsView.alpha;
                
                
                
            } else {
                
                titlesPerc = (scrollView.contentOffset.y-130)/(200-130);
                titlesPerc = fminf(1.0, titlesPerc);
                
                controlsPerc = (scrollView.contentOffset.y-130)/(200-130);
                controlsPerc = fminf(1.0, controlsPerc);
                                
                _titlesLeftConstraint.constant=-coverWidthMin-10-(200-200*titlesPerc);
                _titlesToCoverVSpace.constant=-_coverWidthConstraint.constant;
                _titlesView.alpha=titlesPerc;
                
                _controlsLeftConstraint.constant=50*(1-controlsPerc);
                _controlsToCoverVSpace.constant=-_coverWidthConstraint.constant+58;
                _controlsView.alpha=titlesPerc;
                
                _titlesWidthConstraint.constant = 300 -coverWidthMin -10;
                
                
                if ( _recordingLabel.textAlignment != NSTextAlignmentLeft) {
                    _recordingLabel.textAlignment=NSTextAlignmentLeft;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [_recordingLabel refreshLabels];
                    });
                    
                }
                
                if ( _artistLabel.textAlignment != NSTextAlignmentLeft) {
                    _artistLabel.textAlignment=NSTextAlignmentLeft;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [_artistLabel refreshLabels];
                    });
                }
                
                _itnsBtnRightConstraint.constant=90*controlsPerc;
                
                _volumeView.center = CGPointMake(-500, -500);
                _volumeView.alpha = 0;
                _itunesBtn.alpha = 0;
                
            }
            
            
            CGFloat plTbPerc = 1-fminf(1.0, scrollView.contentOffset.y/150);
            _plTableViewTopConstraint.constant = 10+200*plTbPerc;
            [_playlistTableView setContentOffset:CGPointMake(0, fmaxf(0, scrollView.contentOffset.y-200))];
            
            _plTableViewHeightConstraint.constant=scrollView.height-coverWidthMin-topYOffset-10;
            
        }
    }
    
    
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    CGFloat heightLimit = 200+fmaxf(_playlistTableView.contentSize.height,_playlistTableView.frame.size.height)-_playlistTableView.frame.size.height;
    
    if (scrollView.contentOffset.y > heightLimit) {
        
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y) animated:NO];
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, heightLimit) animated:YES];
        
    }
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset
{

    if ([scrollView isEqual:_mScrollView]) {
        
        if ((*targetContentOffset).y<100) {
            (*targetContentOffset).y=0;
            
            return;
        }else if ((*targetContentOffset).y>100 && (*targetContentOffset).y<200){
            (*targetContentOffset).y=200;
            return;
        }
        
        if (scrollView.contentOffset.y<200 && (*targetContentOffset).y>200) {
            (*targetContentOffset).y=200;
            return;
        }
    }
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (_UINeedHidden) {        
        _UINeedHidden = NO;
        [self hideUI];
    }
    
    _canUpdate = YES;
    
    if (_needsUpdate) {
        _needsUpdate = NO;
        [self updateUIAndAnimate:YES];
    }
    
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _canUpdate = NO;
}



#pragma mark
#pragma mark Playlist TableView Data Source Delegate

-(NSArray *) sortedPLItems{
    if (_sortedPLItems==nil) {
        NSSortDescriptor *time_start_Descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time_start" ascending:NO];
         NSArray *sorted = [[_playlist items] sortedArrayUsingDescriptors:[NSArray arrayWithObject:time_start_Descriptor]];
        
        NSMutableArray *msorted = [NSMutableArray arrayWithArray:sorted];
        
        if (msorted.count>0) {
            [msorted removeObjectAtIndex:0];
        }

        _sortedPLItems = msorted;

    }
    
    
    return _sortedPLItems;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self sortedPLItems] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PlaylistCell";
    __block EPPlaylistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    

    PlaylistItem * plItem = [self sortedPLItems][indexPath.row];
    
    
    UIColor *tintColor = [self tintColor];
    
    cell.delegate=self;
    cell.artistLabel.text = plItem.track.artist;
    cell.artistLabel.textColor = tintColor;
    
    
    cell.recordingLabel.text = plItem.track.song;
    cell.recordingLabel.textColor = tintColor;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    cell.timeLabel.text = [formatter stringFromDate:plItem.time_start];
    cell.timeLabel.textColor = tintColor;

    cell.thumbImageView.clipsToBounds=YES;
    

    
    BOOL isBlack = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:tintColor]<.02;
    [cell.playerButton setTintIsBlack:isBlack];
    cell.playerButton.hidden = YES;

    
    
    cell.thumbImageView.image = [[UIImage alloc] init];
    
    if (![plItem.track.itn_complete boolValue] ) {
        
        [[EPWebsiteDataHelper shared] searchTrackInItunes:plItem.track complete:^(BOOL success, Track *aTrack) {
            if (success) {
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:aTrack.itn_artworkUrl100]];
                if (plItem.track.itn_previewUrl) {
                    cell.item = plItem.track.itn_previewUrl;
                    cell.playerButton.hidden = NO;
                }
                
            }
        }];
        
    }else{
        
        if (plItem.track.itn_previewUrl) {
            cell.item = plItem.track.itn_previewUrl;
            cell.playerButton.hidden = NO;
        }
        
        if (plItem.track.cover.img) {
            [cell.thumbImageView setImage:[UIImage imageWithData:plItem.track.cover.img]];
        }else if(plItem.track.itn_artworkUrl100){
            [cell.thumbImageView setImageWithURL:[NSURL URLWithString:plItem.track.itn_artworkUrl100]];
        }
        
    }
    


    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 79;
}

#pragma mark
#pragma mark EPPlaylistCellDelegate



-(void) didStartPlayInCell : (EPPlaylistCell *) cell{
    
    NSIndexPath *indexPath = [_playlistTableView indexPathForCell:cell];
    
    PlaylistItem * __plItem = [self sortedPLItems][indexPath.row];
    
    
    NSString *strUrl = __plItem.track.itn_previewUrl;
    NSURL *url = [NSURL URLWithString:strUrl];
    
    [[[EPData sharedEPData] player] playURL:url];
    

}

-(void) didStopPlayInCell : (EPPlaylistCell *) cell{
    [[[EPData sharedEPData] player] stop];
}



#pragma mark
#pragma mark STKAudioPlayerDelegate


/// Raised when an item has started playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId{
}

/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId{
}

/// Raised when the state of the player has changed
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer
       stateChanged:(STKAudioPlayerState)state
      previousState:(STKAudioPlayerState)previousState
{
    }

/// Raised when an item has finished playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration{
    
}

/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode{
    
}



@end
