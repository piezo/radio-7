//
//  EPHTMLResponseSerializer.m
//  Europa Plus
//
//  Created by Piezo on 2/10/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPHTMLResponseSerializer.h"

@implementation EPHTMLResponseSerializer

-(id)responseObjectForResponse:(NSURLResponse *)response
                          data:(NSData *)data
                         error:(NSError *__autoreleasing *)error
{
    return [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
}

@end
