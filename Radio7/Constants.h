//
//  Constants.h
//  Europa Plus
//
//  Created by Piezo on 3/30/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#ifndef Europa_Plus_Constants_h
#define Europa_Plus_Constants_h

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define iPad    UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))

#endif
