//
//  EPAlarmViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/10/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPCurrentBgViewController.h"
#import <iCarousel/iCarousel.h>


@interface EPAlarmViewController : EPCurrentBgViewController<iCarouselDataSource, iCarouselDelegate,UITableViewDataSource, UITableViewDelegate>

@end
