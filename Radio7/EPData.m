//
//  EPData.m
//  Europa Plus
//
//  Created by Piezo on 2/6/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPData.h"
#import "EPChannelScrollViewController.h"
#import "EPInfoCenterHelper.h"
#import "EPAppDelegate.h"
#import "EPMainViewController.h"
#import "EPChannelInfoViewController.h"
#import "UIView+Screenshot.h"

@implementation EPData


static EPData *sharedEPDataInstance = nil;

+(EPData *) sharedEPData
{
    @synchronized(self) {
        if (sharedEPDataInstance == nil) {
            sharedEPDataInstance = [[self alloc] init];
        }
        return sharedEPDataInstance;
    }
}

-(void)dealloc{
    self.alarmDays = nil;
    self.alarmTimers = nil;
}



- (id)init
{
    self = [super init];
    if (self) {
        
        
        self.player = [[STKAudioPlayer alloc] init];
        self.imageOfCurrentChannelController=[[UIImage alloc] init];
        
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        
        if (defs) {
            NSInteger alarmHours = -1;
            NSInteger alarmMinutes = -1;
            NSString *alarmChannel;
            NSArray *days;
            
            if ([defs objectForKey:@"alarmHours"]) {
                alarmHours = [[defs objectForKey:@"alarmHours"] integerValue];
            }
            
            
            if ([defs objectForKey:@"alarmMinutes"]) {
                alarmMinutes = [[defs objectForKey:@"alarmMinutes"] integerValue];
            }
            
            if ([defs objectForKey:@"alarmDays"]) {
                days = [defs objectForKey:@"alarmDays"];
            }
            
            if ([defs objectForKey:@"alarmChannel"]) {
                alarmChannel = [defs objectForKey:@"alarmChannel"];
            }
            
            Playlist * alarmPlaylist = [Playlist MR_findFirstByAttribute:@"name" withValue:alarmChannel];
            
            if (alarmHours>-1 && alarmMinutes>0 && days && alarmChannel) {
                [self setupTimerAlarmsForHour:alarmHours
                                 minutes:alarmMinutes
                                    days:days
                              channelIdx:alarmChannel
                                playlist:alarmPlaylist];

            }

            
            
        }

        [[[[[NSNotificationCenter defaultCenter] rac_addObserverForName:epPlaylistUpdateNotification
                                                                 object:nil]
           
           takeUntil:[self rac_willDeallocSignal]]
          
          filter:^BOOL(NSNotification *notif) {
              BOOL ret = self.currentPlaylist==nil && [[(Playlist *) notif.userInfo[@"playlist"] name] isEqualToString:@"r7"];
              return ret;
          }]
         
         subscribeNext:^(NSNotification *notif) {
             self.currentPlaylist = (Playlist *) notif.userInfo[@"playlist"];
         }];
        
        if (!iPad) {
            __block EPData *weakSelf = self;
            
            [[[RACObserve(self, currentChannelController.tintColor) distinctUntilChanged] skip:1] subscribeNext:^(id x) {
                if (weakSelf.currentChannelController.white<=.45) {
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                }else{
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
                }
            }];
            
            
            RACSignal *ccCoverImageChangedSignal = [[RACObserve(self, currentChannelController.infoCntr.coverImageView.image) distinctUntilChanged] filter:^BOOL(id value) {
                return ![value isEqual:nil];
            }];
            
            RACSignal *playertateChangedSignal = [[RACObserve(self, player.state) distinctUntilChanged]
                                                  filter:^BOOL(id value)
                                                  {
                                                      
                                                      STKAudioPlayerState state = [value intValue];
                                                      return state == STKAudioPlayerStatePlaying;
                                                  }];
            
            RACSignal *combineImageAndPlayerSignals = [RACSignal combineLatest:@[
                                                                                 ccCoverImageChangedSignal,
                                                                                 playertateChangedSignal,
                                                                                 ]];
            
            
            [combineImageAndPlayerSignals subscribeNext:^(RACTuple *x) {
                Playlist *playlist = weakSelf.currentChannelController.playlist;
                
                UIImage *cover = x.first;
                if(cover && playlist){
                    MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
                    infoCenter.nowPlayingInfo = @{
                                                  MPMediaItemPropertyTitle : playlist.title,
                                                  MPMediaItemPropertyArtist :[NSString stringWithFormat:@"%@ - %@",playlist.lastItem.track.song, playlist.lastItem.track.artist],
                                                  MPMediaItemPropertyArtwork : [[MPMediaItemArtwork alloc] initWithImage:cover],
                                                  MPMediaItemPropertyPlaybackDuration : @(0)
                                                  };
                    
                }
                    
                
            }];
        }
        
        
        
        
        
        

    }
    
    
    
    return self;
}


- (void)setCurrentChannelController:(EPChannelScrollViewController *)aCurrentChannelController
{
    if (_currentChannelController != aCurrentChannelController) {
        _currentChannelController = aCurrentChannelController;
    }
}

-(void) setupLNAlarmsForHour:(NSInteger)hours
                     minutes:(NSInteger)minutes
                        days:(NSArray *)days
                  channelIdx:(NSString *)aChannelIdx
                    playlist:(Playlist *)anAlarmPlaylist
{
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
}

-(void) setupTimerAlarmsForHour : (NSInteger) hours
                   minutes : (NSInteger) minutes
                      days : (NSArray *) days
                channelIdx : (NSString *) aChannelIdx
                  playlist : (Playlist *) anAlarmPlaylist
{
    
    
    self.alarmDays = [NSMutableArray arrayWithArray:days];
    self.alarmTimers = [NSMutableArray array];
    self.alarmHours = hours;
    self.alarmMinutes = minutes;
    self.alarmChannel = aChannelIdx;
    self.alarmPlaylist = anAlarmPlaylist;
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    if (defs) {
        [defs setObject:@(_alarmHours) forKey:@"alarmHours"];
        [defs setObject:@(_alarmMinutes) forKey:@"alarmMinutes"];
        [defs setObject:_alarmDays forKey:@"alarmDays"];
        [defs setObject:_alarmChannel forKey:@"alarmChannel"];
        
        [defs synchronize];
    }
    

    if (_alarmTimers) {

        [_alarmTimers enumerateObjectsUsingBlock:^(NSTimer *timer, NSUInteger idx, BOOL *stop) {
            [timer invalidate];
        }];
    }
    
    
    NSDate *now = [NSDate new];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    
    self.alarmTimers = [NSMutableArray arrayWithCapacity:42];

    __block EPAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

    
    [NSObject cancelPreviousPerformRequestsWithTarget:appDelegate];
    
    
    [days enumerateObjectsUsingBlock:^(NSNumber *dayIdx, NSUInteger idx, BOOL *stop) {

        NSDateComponents *dateComps = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitTimeZone) fromDate:now];


        [dateComps setHour:hours];
        [dateComps setMinute:minutes];
        [dateComps setSecond:0];
        
        NSInteger daysToAdd = (dayIdx.integerValue-200) - [dateComps weekday];
        
        NSDate *fireDate = [calendar dateFromComponents:dateComps];
        fireDate = [fireDate dateByAddingTimeInterval:60*60*24*daysToAdd];
        
        if ([fireDate timeIntervalSinceDate:now]<=0) {
            fireDate = [fireDate dateByAddingTimeInterval:604800];
        }
        
        
        [appDelegate performSelector:@selector(applicationDidReceiveAlarm:)
                          withObject:@{@"channel" : aChannelIdx}
                          afterDelay:[fireDate timeIntervalSinceNow]];
        

    }];
    
}



@end
