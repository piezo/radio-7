//
//  EPPlayIButton.h
//  Europa Plus
//
//  Created by Piezo on 5/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StreamingKit/STKAudioPlayer.h>

@interface EPPlayIButton : UIButton{
//    STKAudioPlayerState playerState;
//    BOOL continuousPlaying;
//    CGFloat progress;
//    CGFloat bufferRotFraction;
//    BOOL tintIsBlack;
}

@property (nonatomic, assign) STKAudioPlayerState playerState;
@property (nonatomic, assign, getter=isContinuousPlaying) BOOL continuousPlaying;
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, assign) CGFloat bufferRotFraction;
@property (nonatomic, assign, getter=isTintIsBlack) BOOL tintIsBlack;

@end
