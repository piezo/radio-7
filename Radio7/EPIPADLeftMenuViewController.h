//
//  EPIPADLeftMenuViewController.h
//  Europa Plus
//
//  Created by Piezo on 4/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPIPADStyledViewController.h"

@interface EPIPADLeftMenuViewController : EPIPADStyledViewController

@end
