//
//  EPPlaylistCell.h
//  Europa Plus
//
//  Created by Piezo on 2/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StreamingKit/STKAudioPlayer.h>

@class EPPlaylistCell;
@class EPPlayIButton;

@protocol EPPlaylistCellDelegate <NSObject, STKAudioPlayerDelegate>
@required
-(void) didStartPlayInCell : (EPPlaylistCell *) cell;
-(void) didStopPlayInCell : (EPPlaylistCell *) cell;

@end

@interface EPPlaylistCell : UITableViewCell{

}

@property (nonatomic, weak) id<EPPlaylistCellDelegate> delegate;

@property (nonatomic, strong) SitePlaylistItem *sitePLItem;
@property (nonatomic, strong) SiteNoveltyItem *siteNVItem;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *recordingLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet EPPlayIButton *playerButton;
@property (strong, nonatomic) id item;


//- (void) updateUIControls;



@end
