//
//  EPChannelViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/4/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "STKAudioPlayer.h"
#import <CBAutoScrollLabel.h>
#import "EPPlaylistCell.h"

typedef enum
{
    EPChannelViewControllerActionSwitch,
    EPChannelViewControllerActionMenu,
    EPChannelViewControllerActionPlay,
}
EPChannelViewControllerAction;

@class EPChannelViewController;

@protocol EPChannelViewControllerDelegate <NSObject>

-(void) epChannelController: (EPChannelViewController *)aController sendAction:(EPChannelViewControllerAction) action;

@end

@interface EPChannelViewController : UIViewController<STKAudioPlayerDelegate, UIScrollViewDelegate, UITableViewDelegate, EPPlaylistCellDelegate>

@property (nonatomic, assign) id<EPChannelViewControllerDelegate> delegate;
@property (nonatomic, assign)  NSUInteger index;
@property (nonatomic, weak) PlaylistItem *plItem;
@property (nonatomic, strong) Playlist *playlist;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, assign) CGFloat white;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *artistLabel;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *recordingLabel;

@property (nonatomic, strong) UIImage *screenshot;

-(UIImage *) coverScreenshot;

- (void) updateUIAndAnimate: (BOOL) animate;
- (void) updateUIControls;
//- (void) play;
//- (void) stop;

- (void) hideUI;
- (void) showUI;


@end
