//
//  EPCarouselViewController.m
//  Europa Plus
//
//  Created by Piezo on 2/4/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPCarouselViewController.h"
#import "PRTween.h"
#import "EPData.h"
#import "UIView+UserInteractionFeatures.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "EPChannelScrollViewController.h"

#define Z_INDEX_MAX -450
#define ZOOM_OUT_DURATION .25f
#define ZOOM_IN_DURATION .25f

@interface EPCarouselViewController ()

@property (weak, nonatomic) IBOutlet UISlider *scaleSlider;
@property (nonatomic, assign) CGFloat percentage;
@property (nonatomic, strong) NSArray *playlists;
@property (nonatomic, strong) NSMutableArray *imgs;
@property (nonatomic, assign) CGFloat zIndex;
@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer *gestureRecognizer;

@end

@implementation EPCarouselViewController{
    id<GAITracker> tracker;
}

-(id)awakeAfterUsingCoder:(NSCoder *)aDecoder{
    
    _zIndex=0;
    
    return [super awakeAfterUsingCoder:aDecoder];
}

- (void)dealloc
{
    
    self.carousel.delegate = nil;
	self.carousel.dataSource = nil;
    
    self.playlists = nil;
    self.imgs = nil;
    self.controllers = nil;
    self.gestureRecognizer=nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)setIndex:(NSUInteger)anIndex
{
    if (_index != anIndex) {
        _index = anIndex;
    }
}



- (void)setPercentage:(CGFloat)aPercentage
{
    if (_percentage != aPercentage) {
        _percentage = aPercentage;
    }
}


- (void)setZIndex:(CGFloat)aZIndex
{
    if (_zIndex != aZIndex) {
        _zIndex = aZIndex;
        [_carousel setNeedsLayout];
    }
}

-(UIView *) panView{
    if (_panView == nil) {
        //add pan view
        
        UIView *pv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
        pv.backgroundColor = [UIColor redColor];        
        _panView = pv;
    }
    return _panView;
}

- (NSMutableArray *)controllers
{
    if (_playlists==nil) {
        _playlists = [Playlist MR_findAllSortedBy:@"priotiry" ascending:NO];
    }
    if (_controllers==nil) {
        _controllers = [NSMutableArray arrayWithCapacity:42];
        
        [_playlists enumerateObjectsUsingBlock:^(Playlist *pl, NSUInteger idx, BOOL *stop ){
            EPChannelScrollViewController *channelController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChannelScrollViewController"];
            //            channelController.index = idx;
            channelController.playlist = pl;
            channelController.delegate=self;
            
            
            [_controllers addObject:channelController];
        }];
    }
    return _controllers;
}




- (NSArray *)playlists
{
    
    if (_playlists==nil) {
        _playlists = [Playlist MR_findAllSortedBy:@"priotiry" ascending:NO];
    }
    
    return _playlists;
}





- (void)viewDidLoad
{
    
    _zIndex=0;
    self.percentage = 1.0f;
    
    _carousel.type = iCarouselTypeCustom;
    _carousel.vertical = YES;
    _carousel.bounceDistance=.3;
    _carousel.scrollEnabled = NO;
    
    
    
    if (IS_OS_7_OR_LATER)
    {
        
        self.gestureRecognizer  =[[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePan:)];
        _gestureRecognizer.delegate = self;
        _gestureRecognizer.edges=UIRectEdgeRight;
        
//        [self.view addGestureRecognizer:_gestureRecognizer];
    }
    
    [[EPData sharedEPData] setCurrentChannelController:_controllers[0]];
    
    [self.view addSubview:_panView];
    
    [super viewDidLoad];
}



-(void)viewWillAppear:(BOOL)animated{
    [_carousel setNeedsLayout];
    
    [super viewWillAppear:animated];
    
    if ([self isCarouselMode]) {
        [self GAITrackCarousel];
    }else{
        [self GAITrackChannel];
    }
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}




#pragma mark


-(void) edgePan : (UIScreenEdgePanGestureRecognizer *) gestureRecognizer
{
    
    CGPoint  translation = [gestureRecognizer translationInView:self.view];
    
    _zIndex=-Z_INDEX_MAX*translation.x/100.f;
    if (_zIndex<Z_INDEX_MAX) {
        _zIndex=Z_INDEX_MAX;
    }
    if (_zIndex>0) {
        _zIndex=0;
    }
    
    [_carousel setNeedsLayout];
    
    
    if (  gestureRecognizer.state==UIGestureRecognizerStateEnded ) {
        
        if (_zIndex==0) {
            
        }else if (_zIndex==Z_INDEX_MAX) {
            gestureRecognizer.enabled=NO;
            _carousel.scrollEnabled = YES;
            [self GAITrackCarousel];
            
        }else if (_zIndex<-200){
            [self zoomOut];
        }else{
            [self zoomIn:[[EPData sharedEPData] currentChannelController]];
        }
    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [_controllers enumerateObjectsUsingBlock:^(EPChannelScrollViewController *controller, NSUInteger idx, BOOL *stop){
            [controller hideUI];
        }];
    }
    
}

- (void)zoomIn:(EPChannelScrollViewController *)controller {
    _carousel.scrollEnabled = NO;
    
    [PRTween tween:self property:@"zIndex" from:_zIndex to:0 duration: ZOOM_OUT_DURATION*(_zIndex/Z_INDEX_MAX)
    timingFunction:PRTweenTimingFunctionSineOut
       updateBlock:nil
     completeBlock:^{
         
         
         
         [_controllers enumerateObjectsUsingBlock:^(EPChannelScrollViewController *controller, NSUInteger idx, BOOL *stop){
             [controller showUI];
         }];
         
         
         _gestureRecognizer.enabled=YES;
         
     }];
}

- (void)zoomOut {
    [_controllers enumerateObjectsUsingBlock:^(EPChannelScrollViewController *controller, NSUInteger idx, BOOL *stop){
        [controller hideUI];
        
    }];
    
    
    [self GAITrackCarousel];
    
    [PRTween tween:self
          property:@"zIndex"
              from:_zIndex
                to:Z_INDEX_MAX
          duration:ZOOM_OUT_DURATION * ((Z_INDEX_MAX -_zIndex)/Z_INDEX_MAX)
    timingFunction:PRTweenTimingFunctionSineIn
       updateBlock:^(PRTweenPeriod *period){
           
       }completeBlock:^{
           
           _carousel.scrollEnabled = YES;
       }];
}


#pragma mark iCarouselDataSource delegate

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    NSUInteger count = self.controllers.count;
    return count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    
    if (view == nil) {
        EPChannelScrollViewController *controller = [_controllers objectAtIndex:index];
        view = controller.view;
        controller.view.frame = self.view.frame;
//        view.userInteractionEnabled = NO;
        
    }
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionVisibleItems:
        {
            return 6;
        }
        case iCarouselOptionSpacing:
        {
            return value*2;
            break;
        }
            
        default:
        {
            return value;
        }
    }
}




- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    
    if (!_carousel.scrollEnabled) {
        return;
    }
    
    EPChannelScrollViewController * controller = _controllers[index];
    
    [[EPData sharedEPData] setCurrentChannelController:controller];
    
    [self zoomIn:controller];
    [self GAITrackChannel];
    
}

//- (void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel{
////    _canUpdate = NO;
//}
//
//- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
////    _canUpdate = YES;
//}

- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    const CGFloat offsetFactor = [self carousel:carousel valueForOption:iCarouselOptionSpacing withDefault:1.0f]*carousel.itemWidth;
    
    //The larger these values, as the items move away from the center ...
    
    //... the faster they move to the back
    //    const CGFloat zFactor = 150.0f;
    
    //... the faster they move to the bottom of the screen
    //    const CGFloat normalFactor = 0.0f;
    
    //... the faster they shrink
    //    const CGFloat shrinkFactor = 5.0f;
    
    //hyperbola
    //    CGFloat f = sqrtf(offset*offset+1)-1;
    
    transform = CATransform3DTranslate(transform, 0,offset*offsetFactor, _zIndex);
    //    transform = CATransform3DTranslate(transform,offset*offsetFactor,  f*normalFactor, _zIndex);
    //    transform = CATransform3DScale(transform, 1/(f/shrinkFactor+1.0f), 1/(f/shrinkFactor+1.0f), 1.0);
    return transform;
}



- (IBAction)s1Changed:(id)sender {
    _zIndex = [(UISlider *) sender value];
    //    [_carousel setNeedsLayout];
    //    self.percentage = self.scaleSlider.value;
}

#pragma mark EPChannelControllerDelegate

- (void) menuBtnDidPress{
    [self.sidePanelController showLeftPanelAnimated:YES];
}

- (void) channelsBtnDidPress{
    if (_carousel.scrollEnabled) {
        return;
    }
    
    [self zoomOut];
}



-(BOOL) isCarouselMode{
    return _zIndex==Z_INDEX_MAX;
}




-(void) GAITrackCarousel{
    NSDictionary *dict =  [[[GAIDictionaryBuilder createAppView] set:@"Carousel" forKey:kGAIScreenName] build];
    
    [[[GAI sharedInstance] defaultTracker] send:dict];
    
}

-(void) GAITrackChannel{
    
    EPChannelViewController * controller = _controllers[self.carousel.currentItemIndex];
    NSString *screenName = [NSString stringWithFormat:@"Channel - %@", controller.playlist.name];
    
    NSDictionary *dict =  [[[GAIDictionaryBuilder createAppView] set:screenName forKey:kGAIScreenName] build];
    
    [[[GAI sharedInstance] defaultTracker] send:dict];
    
    
}

@end
