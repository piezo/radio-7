//
//  EPCarouselViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/4/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "EPChannelViewController.h"
#import "EPChannelControllerDelegate.h"

@class EPChannelViewController;


@interface EPCarouselViewController : UIViewController<iCarouselDataSource, iCarouselDelegate, EPChannelControllerDelegate, UIGestureRecognizerDelegate>


@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, weak) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) NSMutableArray *controllers;
//@property (nonatomic, strong) EPChannelViewController *channelViewController;
@property (nonatomic, strong) IBOutlet UIView *panView;


@end
