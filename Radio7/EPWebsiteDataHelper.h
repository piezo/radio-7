//
//  EPWebsiteDataHelper.h
//  Europa Plus
//
//  Created by Piezo on 1/31/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>





#define epPlaylistUpdateNotification @"epPlaylistUpdateNotification"
#define epItunesTrackUpdateNotification @"epItunesTrackUpdateNotification"
#define epItunesTrackCoverUpdateNotification @"epItunesTrackCoverUpdateNotification"
#define epITEMUpdateNotification @"epITEMUpdateNotification"

#define epPlaylist2UpdateNotification @"epPlaylist2UpdateNotification"
#define epNoveltyUpdateNotification @"epNoveltyUpdateNotification"


typedef void(^imageDownloadComplete)(BOOL success, UIImage *image, Track *aTrack);
typedef void(^itunesTrackSearchComplete)(BOOL success, Track *aTrack);

typedef enum : NSUInteger {
    EPWebsiteDataUpdTypeAll,
    EPWebsiteDataUpdTypeCurrent,
    EPWebsiteDataUpdTypeNone,
} EPWebsiteDataUpdType;

@interface EPWebsiteDataHelper : NSObject<NSXMLParserDelegate>{

}

+(EPWebsiteDataHelper *) shared;

@property (nonatomic, assign) EPWebsiteDataUpdType updateType;
@property (nonatomic, strong) NSMutableArray *imgs;

-(void)setupPlaylistsWithCompleteBlock : ( void (^)() ) completeBlock;

-(void)searchTrackInItunes : (Track *) aTrack
                  complete : (itunesTrackSearchComplete) completeBlock;

-(void)searchTrackInItunes : (Track *) aTrack
                 transform : (BOOL) aTransform
                  complete : (itunesTrackSearchComplete) completeBlock;

-(void)downloadImageForTrack : (Track *) aTrack complete : (imageDownloadComplete) completeBlock;

-(void) startDataUpdates;
-(void) stopDataUpdates;
-(void) requestPlayerData;
-(void) requestPlaylist;
-(void) requestNovelty;
-(void) requestBanners;

@end
