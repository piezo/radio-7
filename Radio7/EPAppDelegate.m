//
//  EPAppDelegate.m
//  Europa Plus
//
//  Created by Piezo on 1/31/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPAppDelegate.h"
#import "EPData.h"
#import "EPChannelScrollViewController.h"
//#import <TestFlightSDK/TestFlight.h>
#import "EPMainViewController.h"
#import "EPCarouselViewController.h"
//#import <Parse/Parse.h>
#import <Crashlytics/Crashlytics.h>
#import <GAI.h>
#import <iRate.h>
#import "EPIPADChannelViewController.h"
#import "EPChannelInfoViewController.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "BackgroundTask.h"
#import "Flurry.h"

@implementation EPAppDelegate{


}


- (void)setupAppearance
{
    [[UILabel appearance] setBackgroundColor:[UIColor clearColor]];
    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
    [[UIToolbar appearance] setShadowImage:[[UIImage alloc] init] forToolbarPosition:UIBarPositionAny];
    
    if (iPad) {
        [[UINavigationBar appearance]  setBackgroundImage:[UIImage imageNamed:@"toolbarBg"]
                                            forBarMetrics:UIBarMetricsDefault];
        
        [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"toolbarBg"]
                                forToolbarPosition:UIBarPositionAny
                                        barMetrics:UIBarMetricsDefault];
    }
    
    
    
    
}

- (void)setupAudioSession
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    RACSignal *playerStateChangedSignal = [RACObserve([EPData sharedEPData], player.state) distinctUntilChanged];
    RACSignal *alarmDaysChangedSignal = [RACObserve([EPData sharedEPData], alarmDays) distinctUntilChanged];
    
    __block EPAppDelegate *weakSelf = self;

    //BG Task
    self.bgTask = [[BackgroundTask alloc] init];

    
    [playerStateChangedSignal subscribeNext:^(id x) {
        if ([x intValue] == STKAudioPlayerStatePlaying) {
            [[AVAudioSession sharedInstance] setActive: YES error: nil];
        }else{
            [[AVAudioSession sharedInstance] setActive: NO error: nil];
        }
        
    }];
    

    
    if (IS_OS_6_OR_LATER && !IS_OS_7_OR_LATER) {
        [[RACSignal combineLatest:@[
                                    playerStateChangedSignal,
                                    alarmDaysChangedSignal
                                    ]]
         subscribeNext:^(RACTuple *x) {
             
             STKAudioPlayerState playerState = [x.first intValue];
             NSUInteger daysCount = [x.second count];
             
             if ((playerState == STKAudioPlayerStateStopped || playerState == STKAudioPlayerStateReady) && daysCount>0 ) {
                 [weakSelf.bgTask startBackgroundTasks:8 target:self selector:@selector(backgroundCallback:)];
             }
             
             if (daysCount==0 || playerState == STKAudioPlayerStatePlaying) {
                 [weakSelf.bgTask stopBackgroundTask];
             }
             
         }];
    }

    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [MagicalRecord cleanUp];
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    
    [Cover MR_truncateAll];
    [Track MR_truncateAll];
    [PlaylistItem MR_truncateAll];
    [Playlist MR_truncateAll];
    [SitePlaylistItem MR_truncateAll];
    

    [[EPWebsiteDataHelper shared] setupPlaylistsWithCompleteBlock:^{
        
//        [[EPWebsiteDataHelper shared] requestNovelty];
//        [[EPWebsiteDataHelper shared] requestBanners];

    }];
    
    [self setupAppearance];
    [self setupAudioSession];
    
        
    [Crashlytics startWithAPIKey:@"1f0ebbe7f8f4b4ab27c87351d010c8bf94642f64"];
    

    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
     UIRemoteNotificationTypeAlert|
     UIRemoteNotificationTypeSound];
    
    //GAI
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-50023670-1"];

    [iRate sharedInstance].daysUntilPrompt = 1;
    [iRate sharedInstance].usesUntilPrompt = 10;
    
    if (IS_OS_7_OR_LATER) {
        [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIMinimumKeepAliveTimeout];
    }

    [Flurry startSession:@"S6V49TCJM3G6Y4CZGBMP"];
    
    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:deviceToken];
//    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [PFPush handlePush:userInfo];
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
}

-(void)applicationDidReceiveAlarm:(NSDictionary *)dict{
  
    __block Playlist *playlist = [[EPData sharedEPData] alarmPlaylist];
    
    if (!iPad) {
        __block EPChannelScrollViewController *ccCntr;
        __block EPCarouselViewController *carousrlController = [(EPMainViewController *) self.window.rootViewController carousrlController];
        
        [[carousrlController controllers] enumerateObjectsUsingBlock:^(EPChannelScrollViewController *cntrl, NSUInteger idx, BOOL *stop) {
            
            if ([playlist isEqual:cntrl.playlist]) {
                ccCntr = cntrl;
                [[carousrlController carousel] setCurrentItemIndex:idx];
                [[EPData sharedEPData] setCurrentChannelController:ccCntr];
            }
        }];
        
        
        NSURL *url = [NSURL URLWithString:[ccCntr.infoCntr streamUrl]];
        [[[EPData sharedEPData] player] setDelegate:ccCntr.infoCntr];
        [[[EPData sharedEPData] player] playURL:url];
    }else{
        [[EPData sharedEPData] setCurrentPlaylist:playlist];

        NSURL *url = [NSURL URLWithString:[playlist valueForKey:[playlist preferredStream]]];
        [[[EPData sharedEPData] player] setDelegate:[[EPData sharedEPData] ipadChannelController]];
        [[[EPData sharedEPData] player] playURL:url ];
    }
    
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    

    
    STKAudioPlayer *player = [[EPData sharedEPData] player];

    if (player.state != STKAudioPlayerStatePlaying && player.state != STKAudioPlayerStateBuffering) {
        [[EPWebsiteDataHelper shared] setUpdateType:EPWebsiteDataUpdTypeNone];
            
    }else{
        [[EPWebsiteDataHelper shared] setUpdateType:EPWebsiteDataUpdTypeCurrent];
    }
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[EPWebsiteDataHelper shared] setUpdateType:EPWebsiteDataUpdTypeAll];
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    application.applicationIconBadgeNumber = 0;

}

- (void)applicationWillTerminate:(UIApplication *)application
{

    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
    
    [self resignFirstResponder];
}


-(void)remoteControlReceivedWithEvent:(UIEvent *)event{
    
    if (iPad) {
        if (event.type == UIEventTypeRemoteControl){
        
            switch (event.subtype) {
                case UIEventSubtypeRemoteControlTogglePlayPause:
                    
                    if ([[[EPData sharedEPData] player] state] == STKAudioPlayerStatePlaying || [[[EPData sharedEPData] player] state] == STKAudioPlayerStateBuffering) {
                        [[[EPData sharedEPData] player] stop];
                    }else{
                        Playlist *_playlist = [[EPData sharedEPData] currentPlaylist];
                        NSURL *url = [NSURL URLWithString:[_playlist valueForKey:[_playlist preferredStream]]];
                        [[[EPData sharedEPData] player] setDelegate:[[EPData sharedEPData] ipadChannelController]];
                        [[[EPData sharedEPData] player] playURL:url ];
                    }
                    
                    
                    break;
                    
                case UIEventSubtypeRemoteControlPlay:{
                    
                    Playlist *_playlist = [[EPData sharedEPData] currentPlaylist];
                    NSURL *url = [NSURL URLWithString:[_playlist valueForKey:[_playlist preferredStream]]];
                    [[[EPData sharedEPData] player] setDelegate:[[EPData sharedEPData] ipadChannelController]];
                    [[[EPData sharedEPData] player] playURL:url ];
                    
                    break;
                }
                    
                    
                case UIEventSubtypeRemoteControlPause:
                    [[[EPData sharedEPData] player] stop];
                    
                    break;
                
                    
                default:
                    break;
            }
        
        }
    }else{
        if (event.type == UIEventTypeRemoteControl){
            switch (event.subtype) {
                case UIEventSubtypeRemoteControlTogglePlayPause:{
                    
                    if ([[[EPData sharedEPData] player] state] == STKAudioPlayerStateBuffering || [[[EPData sharedEPData] player] state] == STKAudioPlayerStatePlaying) {
                        [[[EPData sharedEPData] player] stop];
                    }else{
                        
                        EPChannelInfoViewController *ciCntrl = [[[EPData sharedEPData] currentChannelController] infoCntr];
                        NSURL *url = [NSURL URLWithString:[ciCntrl streamUrl]];
                        [[[EPData sharedEPData] player] setDelegate:ciCntrl];
                        [[[EPData sharedEPData] player] playURL:url];

                        
                    }
                    
                    break;
                }
                    
                    
                case UIEventSubtypeRemoteControlPlay:{
                    
                    EPChannelInfoViewController *ciCntrl = [[[EPData sharedEPData] currentChannelController] infoCntr];
                    NSURL *url = [NSURL URLWithString:[ciCntrl streamUrl]];
                    [[[EPData sharedEPData] player] setDelegate:ciCntrl];
                    [[[EPData sharedEPData] player] playURL:url];

                    
                    break;
                }
                    
                    
                case UIEventSubtypeRemoteControlPause:
                    [[[EPData sharedEPData] player] stop];
                    
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    
}


-(void) backgroundCallback:(id)info
{
    CLS_LOG(@"###### BG TASK RUNNING %f",[UIApplication sharedApplication].backgroundTimeRemaining);
}


@end
