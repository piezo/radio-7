//
//  EPNovetlyBgViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPCurrentBgViewController.h"
#import "EPPlaylistCell.h"
#import <STKAudioPlayer.h>

@interface EPNovetlyViewController : EPCurrentBgViewController<UITableViewDelegate, UITableViewDataSource,STKAudioPlayerDelegate, EPPlaylistCellDelegate>

@end
