//
//  EPIPADMainViewController.m
//  Europa Plus
//
//  Created by Piezo on 3/23/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADChannelViewController.h"
#import <CBAutoScrollLabel.h>
#import "EPInfoCenterHelper.h"
#import "EPIPADCoverViewController.h"
#import "UIView+Screenshot.h"
#import "UIImageView+AFNetworkingFadein.h"
#import "UIButton+tintImage.h"
#import "EPData.h"
#import "EPPlayIButton.h"
#import "UIColor+Utils.h"
#import "EPIPADTopLayout.h"
#import "EPVolumeView.h"


@interface EPIPADChannelViewController ()

@property (weak, nonatomic) IBOutlet UILabel *playlistHeader;
@property (weak, nonatomic) IBOutlet EPPlayIButton *playerButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverTopCnstr;


@end

@implementation EPIPADChannelViewController{
    

    __weak IBOutlet CBAutoScrollLabel *_artistLabel;
    __weak IBOutlet CBAutoScrollLabel *_recordingLabel;
    
    __weak IBOutlet EPVolumeView *_volumeView;
    __weak IBOutlet UIView *coverContainerView;
    
    EPIPADPlaylistViewController *_playlistController;
    EPIPADCoverViewController *_coverController;
    
    __weak IBOutlet UIButton *_itunesBtn;
    __weak IBOutlet UIImageView *_coverImagevView;
    __weak IBOutlet UIImageView *_coverLogoImagevView;
    __weak IBOutlet UIView *_coverCntrView;

}

-(id)awakeAfterUsingCoder:(NSCoder *)aDecoder{
    
    
    return [super awakeAfterUsingCoder:aDecoder];
    
}

-(void)awakeFromNib{
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.view.tag=1001;
    
    _coverCntrView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _coverCntrView.layer.shadowRadius=35;
    _coverCntrView.layer.shadowOpacity=.2;
    _coverCntrView.layer.shadowOffset=CGSizeMake(0, 0);
    
    _artistLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:23];
    _recordingLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
    
    _volumeView.showsRouteButton = YES;
    
    
    self.themeBtns = @[];
    self.themeLabels = @[_artistLabel, _recordingLabel, _playlistHeader];
    
    __block STKAudioPlayer *weakPlayer = [[EPData sharedEPData] player];
    __block EPIPADChannelViewController *weakSelf = self;
    
    RACSignal *myPlayerState_Signal = [RACObserve([[EPData sharedEPData] player], state) filter:^BOOL(id x) {
        
        STKAudioPlayerState state = [x intValue];
        NSString *urlStr = [[NSString alloc] init];
        
        if (state == STKAudioPlayerStateBuffering && weakPlayer.pendingQueueCount>0) {
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.pendingQueue[0]];
        }else{
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId];
        }
        
        
        return [urlStr isEqualToString:[weakSelf streamUrl]];
        
    }];
    
    
    RACSignal *playlistChanged_Signal  = [RACObserve(self, playlist) distinctUntilChanged] ;
    RACSignal *preferredStreamChanged_Signal  = [RACObserve(self, playlist.preferredStream) distinctUntilChanged] ;
    
    
    [playlistChanged_Signal subscribeNext:^(id x) {

        if (x) {
            [_playlistController setPlaylist:x];
            [_coverController setPlaylist:x];
        }
        
    }];
    
    
    [[RACSignal combineLatest:@[
                               playlistChanged_Signal,
                               preferredStreamChanged_Signal
                               ]] subscribeNext:^(RACTuple *x) {
        
        
        if (x.first) {
            
            BOOL stateIsPlaPlaying = [[[EPData sharedEPData] player] state] == STKAudioPlayerStatePlaying;
            BOOL playerIsMy = [weakPlayer.delegate isEqual:weakSelf];
            BOOL currentlyPlayingIsNotMy = ![[NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId] isEqualToString:[weakSelf streamUrl]];
            
            
            if ( stateIsPlaPlaying
                && playerIsMy
                && currentlyPlayingIsNotMy)
            {
                NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];
                [weakPlayer setDelegate:weakSelf];
                [weakPlayer playURL:url];
            }
            
        }
        
        
        
    }];

    
    
    RAC(self.playerButton, playerState) = [RACSignal combineLatest:@[
                                                                     [myPlayerState_Signal distinctUntilChanged],
                                                                     [RACObserve(weakPlayer, delegate) distinctUntilChanged]
                                                                     ]
                                                            reduce:^id{
                                                                STKAudioPlayerState state;
                                                                
                                                                if (![weakPlayer.delegate isEqual:weakSelf]) {
                                                                    state = STKAudioPlayerStateStopped;
                                                                }else{
                                                                    state = weakPlayer.state;
                                                                }
                                                                
                                                                return @(state);
                                                            }];
    
    [[self.playerButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        
        NSDictionary *dict;
        
        if (   self.playerButton.playerState == STKAudioPlayerStatePlaying
            || self.playerButton.playerState == STKAudioPlayerStateBuffering)
        {
            
            [weakPlayer stop ];
            [weakPlayer setDelegate:nil];
            
            dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                           action:@"button_press"
                                                            label:@"pause"
                                                            value:nil] build];
            
            
        }else{
            NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];
            [weakPlayer setDelegate:weakSelf];
            [weakPlayer playURL:url ];
            
            
            
            dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                           action:@"button_press"
                                                            label:@"play"
                                                            value:nil] build];
            
            
        }
        
        [[[GAI sharedInstance] defaultTracker] send:dict];
 
    }];

    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        self.view.clipsToBounds=NO;
}


-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white
{
    
    PlaylistItem *_plItem = playlist.lastItem;

    if (![self.playlist isEqual:playlist]) {
        self.playlist = playlist;
    }
    
    
    _coverController.playlist = _playlist;
    
    _artistLabel.text = _plItem.track.artist;
    _recordingLabel.text = _plItem.track.song;
    
    _volumeView.tintColor = tintColor;
    
    _itunesBtn.hidden = (_plItem.track.itn_collectionViewUrl == nil);
    
    
    CGFloat cDist = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:tintColor];
    [_playerButton setTintIsBlack:cDist<.02];
    
    if (_plItem.track.cover.img) {
        _coverImagevView.image = [UIImage imageWithData:_plItem.track.cover.img];
        _coverLogoImagevView.hidden = YES;
    }else{
        _coverImagevView.image = [UIImage imageNamed:@"no_cover"];
        _coverLogoImagevView.hidden = NO;
        _coverLogoImagevView.image = [UIImage imageNamed:_playlist.logo];
    }

    [_playlistController setTintColor:tintColor];
    [_playlistController setATintColor:aTintColor];
    [_playlistController setPlaylist:_playlist];
    [_playlistController.collectionView reloadData];
    
    [[_playlistController collectionView] scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                                 atScrollPosition:UICollectionViewScrollPositionTop
                                                         animated:NO];

    [[EPInfoCenterHelper sharedEPInfoCenterHelper] updtateInfoWithPlaylist:_playlist
                                                                     cover:_coverImagevView.image
                                                                     white:white];
    
    [super updateWithPlaylist:playlist tintColor:tintColor aTintColor:aTintColor white:white];

}

-(void)updateViewConstraints{
    [super updateViewConstraints];
    self.coverTopCnstr.constant = 83.0 - self.topLayoutGuide.length;
}

- (NSString *) streamUrl{
    return [_playlist valueForKey:[_playlist preferredStream]];
}


- (IBAction)showInItunes:(id)sender {
    
    Track *track = _playlist.lastItem.track;
    NSURL *url = [NSURL URLWithString:[track.itn_collectionViewUrl stringByAppendingString:@"&at=10ldCb&ct=r7_ios_app"]];
    [[UIApplication sharedApplication] openURL:url];
    
//    CLS_LOG(@"url : %@", url);
    
//    NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"iTunes"
//                                                                 action:@"Show_In_iTunes"
//                                                                  label:[NSString stringWithFormat:@"%@ %@ %@", track.artist, track.song, [track.itn_collectionViewUrl stringByAppendingString:@"&at=10lsPX&ct=ep_ios_app"]]
//                                                                  value:nil] build];
//    
//    
//    [[[GAI sharedInstance] defaultTracker] send:dict];
    
    
//    dict = [[GAIDictionaryBuilder createTransactionWithId:[NSString stringWithFormat:@"%@", track.itn_trackId]
//                                              affiliation:@"iTunes"
//                                                  revenue:track.itn_trackPrice
//                                                      tax:@0.0F
//                                                 shipping:@1
//                                             currencyCode:track.itn_currency] build];
//    
//    [[[GAI sharedInstance] defaultTracker] send:dict];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"playlist"]) {
        
        _playlistController = (EPIPADPlaylistViewController *) segue.destinationViewController ;
        _playlistController.delegate = self;
        
    }
    
    if ([segue.identifier isEqualToString:@"cover"]) {
        
        _coverController = (EPIPADCoverViewController *) segue.destinationViewController ;
        
    }
    
    
}




#pragma mark EPIPADPlaylistViewControllerDelegate

-(NSArray *)playlistCntr: (EPIPADPlaylistViewController*) playlistCntr sortedPLItemsWithRemFirst : (BOOL) remFirst{
    
    
    NSSortDescriptor *time_start_Descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time_start" ascending:NO];
    NSArray *sorted = [[_playlist items] sortedArrayUsingDescriptors:[NSArray arrayWithObject:time_start_Descriptor]];
    
    NSMutableArray *msorted = [NSMutableArray arrayWithArray:sorted];
    
    if (msorted.count>0) {
        [msorted removeObjectAtIndex:0];
    }
    
    return [msorted subarrayWithRange:NSMakeRange(0, fminf(100.0,  (CGFloat) msorted.count))];
}


-(void) playlistCntr : (EPIPADPlaylistViewController*) playlistCntr
          fillupCell : (EPPlaylistCollectionCell *) cell
             atIndex : (NSIndexPath *) indexPath
{

    PlaylistItem *plItem = [[playlistCntr sortedPLItems] objectAtIndex:indexPath.row];
    
    
    cell.backgroundColor = [UIColor clearColor];
    
    cell.item = plItem.track;
    cell.cover.image = [[UIImage alloc] init];
    cell.artistLabel.text = plItem.track.artist;
    cell.recordingLabel.text = plItem.track.song;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    cell.timeLabel.text = [formatter stringFromDate:plItem.time_start];
    cell.playerButton.hidden = YES;
    cell.coverLeftCnstr.constant = -100;
    
    __block EPPlaylistCollectionCell *weakCell = cell;
    
    if (![plItem.track.itn_complete boolValue] ) {
        
        
        [[EPWebsiteDataHelper shared] searchTrackInItunes:plItem.track complete:^(BOOL success, Track *aTrack) {
            
            if (![aTrack isEqual:weakCell.item]) {
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    
                    NSString *thumbUrlString = plItem.track.itn_artworkUrl100;
                    thumbUrlString = [thumbUrlString stringByReplacingOccurrencesOfString:@"100x100" withString:@"200x200"];
                    
                    
                    [weakCell.cover setImageWithURL:[NSURL URLWithString:thumbUrlString] fadein:YES];
                    
                    if (plItem.track.itn_previewUrl) {
                        weakCell.streamUrl = plItem.track.itn_previewUrl;
                        weakCell.playerButton.alpha=0;
                        weakCell.playerButton.hidden = NO;
                        
                        [UIView animateWithDuration:.3 animations:^{
                            weakCell.playerButton.alpha=1;
                            weakCell.coverLeftCnstr.constant = 0;
                            [weakCell layoutIfNeeded];
                        }];
                    }
                    
                }else{
//                    [weakCell.cover setImage:[UIImage imageNamed:@"no_cover"]];
                }
            });
            
        }];
        
    }else{
        
        if (plItem.track.itn_previewUrl) {
            cell.streamUrl = plItem.track.itn_previewUrl;
            cell.playerButton.alpha=0;
            cell.playerButton.hidden = NO;
            
            [UIView animateWithDuration:.3 animations:^{
                cell.playerButton.alpha=1;
            }];
        }
        
        if (plItem.track.cover.img) {
            [cell.cover setImage:[UIImage imageWithData:plItem.track.cover.img]];
            
        }else if(plItem.track.itn_artworkUrl100){
            
            NSString *thumbUrlString = plItem.track.itn_artworkUrl100;
            thumbUrlString = [thumbUrlString stringByReplacingOccurrencesOfString:@"100x100" withString:@"200x200"];
            
            [cell.cover setImageWithURL:[NSURL URLWithString:thumbUrlString]];
            cell.coverLeftCnstr.constant = 0;
            
        }else{
//           [cell.cover setImage:[UIImage imageNamed:@"no_cover"]];
        }
        
    }

    
    [@[ cell.timeLabel, cell.artistLabel, cell.recordingLabel] enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger idx, BOOL *stop) {
        label.textColor = playlistCntr.tintColor;
    }];
    
    CGFloat cDist = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:playlistCntr.tintColor];
    [cell.playerButton setTintIsBlack:cDist<.02];
    
    
    
//    [@[cell.playBtn, cell.playBtn] enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
//        [btn setImageTintColor: playlistCntr.tintColor];
//    }];
}


//----------------------------------------------------------
// - setPlaylist:
//----------------------------------------------------------
- (void)setPlaylist:(Playlist *)aPlaylist
{
    if (_playlist != aPlaylist) {
        _playlist = aPlaylist;
    }
}




/// Raised when an item has started playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId{}
/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId{}
/// Raised when the state of the player has changed
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState{}
/// Raised when an item has finished playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration{
}
/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode{}

@end
