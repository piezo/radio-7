//
//  EPIPADMainViewController.h
//  Europa Plus
//
//  Created by Piezo on 4/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "EPIPADCarouselViewController.h"

@interface EPIPADMainViewController : JASidePanelController<EPIPADCarouselViewControllerDelegate>


- (IBAction)switchChannel:(id)sender;

- (void) switchToChannel;
- (void) switchToNovetly;
- (void) switchToAlarm;

@end
