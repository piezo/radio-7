//
//  EPCurrentBgViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GAITrackedViewController.h>

@class EPChannelScrollViewController;

@interface EPCurrentBgViewController : UIViewController


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *themeBtns;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *themeLabels;
@property (nonatomic, weak) PlaylistItem *plItem;
@property (nonatomic, strong) Playlist *playlist;


-(EPChannelScrollViewController *) currentController;
- (void) updateUITheme;
- (void) updateUIAndAnimate: (BOOL) animate;
@end
