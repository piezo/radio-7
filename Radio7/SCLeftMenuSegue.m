//
//  SCLeftMenuSegue.m
//  School of Care
//
//  Created by Piezo on 12/29/13.
//  Copyright (c) 2013 Oleksandr Izvekov. All rights reserved.
//

#import "SCLeftMenuSegue.h"
#import "EPLeftViewController.h"

#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"

@implementation SCLeftMenuSegue

-(void)perform{
    EPLeftViewController *src = (EPLeftViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    
    [[src sidePanelController] setCenterPanel:dst];
}

@end
