//
//  EPTestingViewController.m
//  Europa Plus
//
//  Created by Piezo on 1/31/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPTestingViewController.h"
//#import "Discogs-ObjC.h"

#import "Playlist.h"
#import "Playlist+LastItem.h"
#import "PlaylistItem.h"
#import "Track.h"
#import "Cover.h"


@interface EPTestingViewController (){
//    Track *curTrack;
}

@property (nonatomic, strong) Track *curTrack;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end

@implementation EPTestingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newPlaylistItem:) name:@"EPNewTrack" object:nil];
    
    
}


-(void) newPlaylistItem : (NSNotification *) aNotification{
    
    Playlist *pl = [Playlist MR_findFirstByAttribute:@"name" withValue:@"r7"];
    PlaylistItem  *plItem = pl.lastItem;
    
    _label1.text = plItem.track.artist;
    _label2.text = plItem.track.song;
    
    self.curTrack=  plItem.track;
    
}

-(void) newImage : (NSNotification *) aNotification{

    Playlist *pl = [Playlist MR_findFirstByAttribute:@"name" withValue:@"r7"];
    PlaylistItem  *plItem = pl.lastItem;


    if (!plItem.track.cover.img) {
        _img1.image = [UIImage imageNamed:@"no_cover"];
    }else{
        _img1.image = [UIImage imageWithData:plItem.track.cover.img];
    }
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)testSiteData:(id)sender {
    
    [[EPWebsiteDataHelper shared] requestPlayerData];
}



@end
