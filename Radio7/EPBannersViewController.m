//
//  EPBannersViewController.m
//  Europa Plus
//
//  Created by Piezoon 3/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPBannersViewController.h"
#import "EPData.h"
#import <AFNetworking/UIKit+AFNetworking.h>

@interface EPBannersViewController ()
@property (weak, nonatomic) IBOutlet iCarousel *carousel;

@end

@implementation EPBannersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [_carousel setType:iCarouselTypeLinear];
    
    [RACObserve([EPData sharedEPData], bannersArray) subscribeNext:^(id x) {
       
        [_carousel reloadData];
        
    }];

    self.view.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return [[[EPData sharedEPData] bannersArray] count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    UIImageView *imgView = (UIImageView *) view;
    
    if (imgView == nil) {
        
        imgView = [[UIImageView  alloc] initWithFrame:self.view.bounds];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
    }
    
    NSString *picUrlStr = [[[EPData sharedEPData] bannersArray] objectAtIndex:index][@"pic"];
    NSURL *url = [NSURL URLWithString:picUrlStr];
    
    [imgView setImageWithURL:url];

    
    return imgView;
    
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    NSString *handlerUrlStr = [[[EPData sharedEPData] bannersArray] objectAtIndex:index][@"link"];
    NSURL *url = [NSURL URLWithString:handlerUrlStr];
    
    if (    [[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }

}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    
    switch (option) {
        case iCarouselOptionVisibleItems:
            value = 3.0f;
            break;
            
        default:
            break;
    }
    
    return value;
    
}




@end
