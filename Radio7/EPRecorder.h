//
//  EPRecorder.h
//  Europa Plus
//
//  Created by Oleksandr Izvekov on 3/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface EPRecorder : NSObject<AVAudioRecorderDelegate>

+ (id)sharedEPRecorder;

- (void) startRecording;
- (void) stopRecording;

@end
