//
//  EPChannelControllerDelegate.h
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EPChannelControllerDelegate <NSObject>

- (Playlist *) playlistForCntr : (UIViewController *) controller;

- (void) menuBtnDidPress;
- (void) channelsBtnDidPress;
@end
