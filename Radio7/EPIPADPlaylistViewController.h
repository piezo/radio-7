//
//  EPIPADPlaylistViewController.h
//  Europa Plus
//
//  Created by Piezo on 5/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPPlaylistCollectionCell.h"

@class EPIPADPlaylistViewController;
@protocol EPIPADPlaylistViewControllerDelegate <NSObject>

@required
-(NSArray *)playlistCntr : (EPIPADPlaylistViewController*) playlistCntr sortedPLItemsWithRemFirst : (BOOL) remFirst;

-(void) playlistCntr : (EPIPADPlaylistViewController*) playlistCntr
          fillupCell : (EPPlaylistCollectionCell *) cell
             atIndex : (NSIndexPath *) indexPath;

@end

@interface EPIPADPlaylistViewController : UICollectionViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
//    Playlist *_playlist;
//    UIColor *_tintColor;
//    UIColor *_aTintColor;
//    id<EPIPADPlaylistViewControllerDelegate> delegate;
}

@property (nonatomic, weak) id<EPIPADPlaylistViewControllerDelegate> delegate;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, strong) UIColor *aTintColor;
@property (nonatomic, strong) Playlist *playlist;

-(NSArray *) sortedPLItems;

@end
