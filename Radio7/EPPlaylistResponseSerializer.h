//
//  EPPlaylistResponseSerializer.h
//  Europa Plus
//
//  Created by Piezoon 2/10/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPHTMLResponseSerializer.h"
#import "HTMLParser.h"

@interface EPPlaylistResponseSerializer : EPHTMLResponseSerializer

@end
