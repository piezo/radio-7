//
//  EPBannersViewController.h
//  Europa Plus
//
//  Created by Piezo on 3/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iCarousel/iCarousel.h>

@interface EPBannersViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>

@end
