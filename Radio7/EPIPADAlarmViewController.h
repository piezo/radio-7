//
//  EPIPADAlarmViewController.h
//  Europa Plus
//
//  Created by Piezo on 5/27/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADStyledViewController.h"


@interface EPIPADAlarmViewController : EPIPADStyledViewController<UITableViewDataSource, UITableViewDelegate>

@end
