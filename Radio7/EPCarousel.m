//
//  EPCarousel.m
//  Europa Plus
//
//  Created by Piezo on 2/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPCarousel.h"


@interface iCarousel()
- (void)layOutItemViews;
@end

@implementation EPCarousel

- (void)layOutItemViews
{
    
    [super layOutItemViews];
    
    for (int i = 0; i<self.numberOfItems; i++)
    {
        UIView *view =  [self itemViewAtIndex:i];
        CGRect rect = view.frame;
        rect.origin.y=0;
        rect.size.height = self.frame.size.height;
        view.frame=rect;
        
        view.superview.frame = rect;
        
    }
    
}

@end
