//
//  Playlist+LastItem.h
//  Europa Plus
//
//  Created by Piezo on 2/2/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "Playlist.h"

@interface Playlist (LastItem)

-(PlaylistItem *) updateLastItem;

@end
