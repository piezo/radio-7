//
//  EPIPADCarouselViewController.h
//  Europa Plus
//
//  Created by Piezoon 5/22/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iCarousel.h>

@protocol EPIPADCarouselViewControllerDelegate <NSObject>

@required
-(void) didSelectCannel : (Playlist *) playlist;

@end

@interface EPIPADCarouselViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>{

}

@property (nonatomic, weak) id<EPIPADCarouselViewControllerDelegate> delegate;



@end
