//
//  EPIPADMainViewController.m
//  Europa Plus
//
//  Created by Piezoon 4/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADMainViewController.h"
#import "EPData.h"
#import "FXBlurView.h"
#import "EPIPADChannelViewController.h"
#import "EPIPADLeftMenuViewController.h"
#import "EPIPADPlaylistViewController.h"
#import <UIView+CBFrameHelpers.h>
#import <UIImage+AverageColor.h>
#import <UIColor-MJGAdditions.h>
#import "UIView+Screenshot.h"
#import "UIColor+Grayscale.h"
#import "UIButton+tintImage.h"
#import "EPInfoCenterHelper.h"
#import "EPIPADNovetlyViewController.h"
#import "EPIPADAlarmViewController.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>


@interface EPIPADMainViewController (){
    PlaylistItem *_plItem;
    UIColor *_tintColor;
    UIColor *_aTintColor;
}

@property (nonatomic, strong) Playlist *playlist;
@property (nonatomic, strong) EPIPADChannelViewController *channelViewController;
@property (nonatomic, strong) EPIPADCarouselViewController *carouselViewController;
@property (nonatomic, strong) EPIPADLeftMenuViewController  *leftmenuViewController;
@property (nonatomic, strong) EPIPADNovetlyViewController  *novetlyViewController;
@property (nonatomic, strong) EPIPADAlarmViewController  *alarmViewController;


@property (weak, nonatomic) IBOutlet UIView *mTopbar;
@property (weak, nonatomic) IBOutlet UIView *mToolbar;
@property (weak, nonatomic) IBOutlet UIView *carouselContainer;
@property (weak, nonatomic) IBOutlet FXBlurView *blurView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tImg;

@property (weak, nonatomic) IBOutlet UIView *test1;
@property (weak, nonatomic) IBOutlet UIView *test2;
@property (weak, nonatomic) IBOutlet UIView *test3;
@property (weak, nonatomic) IBOutlet UIView *test4;
@property (weak, nonatomic) IBOutlet UIView *test5;
@property (weak, nonatomic) IBOutlet UIButton *switchChennelBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;

@property (weak, nonatomic) IBOutlet UIButton *btrBtn32;
@property (weak, nonatomic) IBOutlet UIButton *btrBtn64;
@property (weak, nonatomic) IBOutlet UIButton *btrBtn128;
@property (weak, nonatomic) IBOutlet UIButton *btrBtnHD;
@property (nonatomic, strong) IBOutlet UIAlertView *alertView;

@end

@implementation EPIPADMainViewController{
    CGFloat _white;

    BOOL _preloading;
    UIView *_preloaderBg;
    UIImageView *_preloaderImageViewLandscape;
    UIImageView *_preloaderImageViewPortrait;
    UIActivityIndicatorView *_activityIndicator;
}


-(void)awakeFromNib{
    
    self.channelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChannelViewController"];
    self.leftmenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
    self.novetlyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NovetlyViewController"];
    self.alarmViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AlarmViewController"];
    
    
    
    [self setLeftPanel:_leftmenuViewController];

    [super awakeFromNib];
}


- (void)setupReachability
{
    //Reachability
    
    EPIPADMainViewController *weakSelf;
    __block EPWebsiteDataHelper *weakDataHelper = [EPWebsiteDataHelper shared];
    __block BOOL reachability = NO;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        CLS_LOG(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                CLS_LOG(@"No Internet Connection");
                reachability = NO;
                [weakDataHelper stopDataUpdates];
                
                weakSelf.alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"You are not connected to the Internet. Make sure your internet connection is active and try again"
                                                               delegate:self
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles: nil];
                
                [weakSelf.alertView show];
                
                break;
            }
                
                
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
                
                if (!reachability) {
                    [weakDataHelper startDataUpdates];
                    [weakSelf.alertView dismissWithClickedButtonIndex:0 animated:YES];
                }
                
                reachability = YES;
                
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                
                if (!reachability) {
                    [weakDataHelper startDataUpdates];
                    [weakSelf.alertView dismissWithClickedButtonIndex:0 animated:YES];
                }
                
                reachability = YES;
                
                break;
            default:
                CLS_LOG(@"Unkown network status");
                break;
                
                [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        }
    }];
}

- (void)setupRAC
{
    __block EPIPADMainViewController *weakSelf = self;
    
    
    RAC(self, playlist) = RACObserve([EPData sharedEPData], currentPlaylist);
    
    RACSignal *plitem_complete_signal = RACObserve(weakSelf, playlist.lastItem.complete) ;
    
    
    [plitem_complete_signal subscribeNext:^(id x) {
        
        if ([x boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf updateUI:weakSelf.playlist.lastItem.track.cover.img!=nil sender:self];
            });
        }
        
    }];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (UIInterfaceOrientationIsLandscape( [[UIApplication sharedApplication] statusBarOrientation] ))
        {
            self.recognizesPanGesture=NO;
            [self showLeftPanelAnimated:NO];
            [self setForceCenterSwap:YES];
            _menuBtn.hidden=YES;
            
        }else{
            self.recognizesPanGesture=YES;
            [self showCenterPanelAnimated:NO];
            [self setForceCenterSwap:NO];
            _menuBtn.hidden=NO;
            
        }
    });
    
//    RAC(_switchChennelBtn,hidden) = [[RACObserve(self, centerPanel) distinctUntilChanged] map:^id(id value) {
//        return @(![value isEqual:_channelViewController]);
//    }];
    
    _test1.hidden =  _test2.hidden =    _test3.hidden=    _test4.hidden=    _test5.hidden=YES;
    
    
    RACSignal *btrHidden_signal   = [RACSignal combineLatest:@[
//                                                               RACObserve(self, btrBtnHD.hidden),
                                                               RACObserve(self, btrBtn128.hidden),
                                                               RACObserve(self, btrBtn64.hidden),
                                                               RACObserve(self, btrBtn32.hidden)
                                                               ] reduce:^id{
                                                                   return @( weakSelf.btrBtnHD.hidden && weakSelf.btrBtn128.hidden
                                                                   && weakSelf.btrBtn64.hidden && weakSelf.btrBtn32.hidden);
                                                               }];
    
    
    RAC(self, channelViewController.buttomSpacingCnstr.constant) = [btrHidden_signal map:^id(id value) {
        return @([value boolValue]?0:44);
    }];
    
    RAC(self, channelViewController.plTopSpacingCnstr.constant) = [btrHidden_signal map:^id(id value) {
        return @([value boolValue]?100:100-20);
    }];
}

- (void)setupUI
{
    [[EPData sharedEPData] setIpadChannelController:_channelViewController];
    
    [self setCenterPanel:_channelViewController];
    
    [self setLeftFixedWidth:314];
    [self setShouldResizeLeftPanel:YES];
    [self setPushesSidePanels:YES];
    [self setBounceOnSidePanelClose:NO];
    [self setBounceOnSidePanelOpen:NO];
    [self setRecognizesPanGesture:YES];
    [self showLeftPanelAnimated:NO];
    
    //[self setStyle:JASidePanelMultipleActive];
    
    self.centerPanelContainer.clipsToBounds = NO;
    
    [_blurView setBlurRadius:200.0];
    [_blurView setDynamic:NO];
    [_blurView setTintColor:[UIColor clearColor]];
    
    [_carouselContainer setUserInteractionEnabled:NO];
    [_carouselContainer setHidden:YES];
    [_carouselContainer setBackgroundColor:[UIColor clearColor]];
    
    [self.view bringSubviewToFront:_mToolbar];
    [self.view bringSubviewToFront:_mTopbar];
    [self.view bringSubviewToFront:_carouselContainer];
}

- (void)setupPreloader
{
    _preloaderBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1500, 1500)];
    _preloaderBg.backgroundColor = [UIColor blackColor];
    _preloaderBg.center = self.view.center;

    BOOL isLandscape = UIInterfaceOrientationIsLandscape( [[UIApplication sharedApplication] statusBarOrientation] );
    _preloaderImageViewLandscape = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LaunchImage-Landscape"]];
    _preloaderImageViewPortrait = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LaunchImage-Portrait"]];
    
    [self.view setUserInteractionEnabled:NO];
    [self.view addSubview:_preloaderBg];
    [self.view addSubview:_preloaderImageViewLandscape];
    [self.view addSubview:_preloaderImageViewPortrait];
    
    if (isLandscape) {
        _preloaderImageViewPortrait.alpha=0;
    }
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds)-5,
                                                                                   CGRectGetMaxY(self.view.bounds)-100,
                                                                                   10,
                                                                                   10)];
    
    
    [_activityIndicator startAnimating];
    [self.view addSubview:_activityIndicator];
    
    _activityIndicator.center = isLandscape
    ?CGPointMake(_preloaderImageViewLandscape.center.x, _preloaderImageViewLandscape.center.y+270)
    :CGPointMake(_preloaderImageViewPortrait.center.x, _preloaderImageViewPortrait.center.y+380);
    
}


-(void) removePreloaderIfNeeded{
    if(_preloaderImageViewPortrait){
        [self removePreloader];
    }
}

-(void) removePreloader{
    
    [_preloaderBg removeFromSuperview];
    _preloaderBg = nil;
    [UIView animateWithDuration:.23 animations:^{
        _preloaderImageViewPortrait.alpha=0;
        _preloaderImageViewLandscape.alpha=0;
        _activityIndicator.alpha=0.5;
    } completion:^(BOOL finished) {
        [_preloaderImageViewPortrait removeFromSuperview];
        [_preloaderImageViewLandscape removeFromSuperview];
        [_activityIndicator removeFromSuperview];
        
        _preloaderImageViewPortrait = nil;
        _preloaderImageViewLandscape = nil;
        _activityIndicator = nil;
        
        [self.view setUserInteractionEnabled:YES];
    }];
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    

    [self setupUI];
    [self setupRAC];
    [self setupReachability];
    [self setupPreloader];

}

- (void)updateStreamBtns {
    //    _mTopbar.backgroundColor = [_tintColor colorWithAlphaComponent:.1];
    //    _mToolbar.backgroundColor = [_tintColor colorWithAlphaComponent:.1];
    
    _btrBtn32.hidden =  [_playlist.stream32 isEqualToString:@""];
    _btrBtn64.hidden =  [_playlist.stream64 isEqualToString:@""];
    _btrBtn128.hidden = [_playlist.stream128 isEqualToString:@""];
    _btrBtnHD.hidden =  [_playlist.streamHD isEqualToString:@""];
    
    
    
    _btrBtn32.selected = [_playlist.preferredStream isEqualToString:@"stream32"];
    _btrBtn64.selected = [_playlist.preferredStream isEqualToString:@"stream64"];
    _btrBtn128.selected = [_playlist.preferredStream isEqualToString:@"stream128"];
    _btrBtnHD.selected = [_playlist.preferredStream isEqualToString:@"streamHD"];
}

- (void)updateUI:(BOOL)blur sender : (id) sender{
    
    if (![_channelViewController.playlist isEqual:_playlist]) {
        _channelViewController.playlist = _playlist;
    }


    if ([self.centerPanel isEqual:_channelViewController]) {
        _headerLabel.text = _playlist.title;
    }

    BOOL fadeAnimation = NO;
    
    
    if (_carouselContainer.hidden) {
        UIImage *screenshot = [[self view] screenshotAfterScreenUpdates:YES];
        
        __block UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        imgView.userInteractionEnabled=NO;
        imgView.image = screenshot;
        imgView.tag = 42;
        
        [self.view addSubview:imgView];
        
        fadeAnimation = YES;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (imgView && imgView.superview) {
                [imgView removeFromSuperview];
            }
        });
    }

    
    if (_playlist.lastItem.track.cover.img) {
        _tImg.image = [UIImage imageWithData:_playlist.lastItem.track.cover.img];
        [_blurView setNeedsDisplay];
    }else{
        _tImg.image = [UIImage imageNamed:@"no_cover"];
        [_blurView setNeedsDisplay];

    }
    
    
    UIColor *avgColor = [_tImg.image averageColor];
    

    [avgColor.grayscale getWhite:&_white alpha:nil];
    
    _tintColor = (_white<=0.45f)?[UIColor whiteColor]:[UIColor blackColor];
    _aTintColor = (_white<=0.45f)?[UIColor blackColor]:[UIColor whiteColor];
    
    
    [@[_headerLabel] enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger idx, BOOL *stop) {
        label.textColor = _tintColor;
    }];
    
    
    [@[_menuBtn,_btrBtn32, _btrBtn64, _btrBtn128, _btrBtnHD] enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
        [btn setImageTintColor:_tintColor];
    }];
    

    [self updateStreamBtns];
    
    int c = [@(_btrBtn32.hidden) intValue] + [@(_btrBtn64.hidden) intValue] + [@(_btrBtn128.hidden) intValue] + [@(_btrBtnHD.hidden) intValue];
    
    if (c>0) {
        _btrBtn32.hidden = _btrBtn64.hidden = _btrBtn128.hidden = _btrBtnHD.hidden = YES;
    }
    
    [(EPIPADStyledViewController *) [self centerPanel]  updateWithPlaylist:_playlist tintColor:_tintColor aTintColor:_aTintColor white:_white];

    [_leftmenuViewController  updateWithPlaylist:_playlist tintColor:_tintColor aTintColor:_aTintColor white:_white];

    if (_white<=.45) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    
    if (fadeAnimation) {
        
        UIImageView *imgView = (UIImageView *) [self.view viewWithTag:42];
        
        [UIView animateWithDuration:.3 animations:^{
            
            imgView.alpha=0;
            
        } completion:^(BOOL finished) {
            [imgView removeFromSuperview];
            [self removePreloaderIfNeeded];
        }];
    }

}


-(BOOL) shouldAutorotate{
    return YES;

}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{

    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        self.recognizesPanGesture=NO;
        [self showLeftPanelAnimated:YES];
        _menuBtn.hidden=YES;
        [self setForceCenterSwap:YES];
        

        
    }else{
        self.recognizesPanGesture=YES;
        [self showCenterPanelAnimated:YES];
        _menuBtn.hidden=NO;
        [self setForceCenterSwap:NO];

    }

    if (_preloaderImageViewLandscape) {
        BOOL isLandscape = UIInterfaceOrientationIsLandscape( toInterfaceOrientation );
        
        
        _activityIndicator.center = isLandscape
                                    ?CGPointMake(_preloaderImageViewLandscape.center.x, _preloaderImageViewLandscape.center.y+270)
                                    :CGPointMake(_preloaderImageViewPortrait.center.x, _preloaderImageViewPortrait.center.y+380);
        
        [UIView animateWithDuration:duration animations:^{
            _preloaderImageViewPortrait.alpha = isLandscape?0:1;
            _preloaderImageViewLandscape.alpha = isLandscape?1:0;
            
        } completion:^(BOOL finished) {
            
        }];

    }
    
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
//       self.view.clipsToBounds = NO;
}

- (IBAction)switchChannel:(id)sender {

    [_carouselContainer setAlpha:0.0];
    [_carouselContainer setHidden:NO];
    _carouselContainer.transform = CGAffineTransformMakeScale(1.6, 1.6);
    

    [UIView animateWithDuration:.25
                          delay:.0
                        options:0 animations:^{
                            
                            
                            [self.leftPanel.view setAlpha:.0];
                            [self.centerPanel.view setAlpha:.0];
                            [self.mToolbar setAlpha:0.0];
                            [self.mTopbar setAlpha:0.0];
                            
                        }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:.25
                                          animations:^{
                                              _carouselContainer.alpha = 1.0;
                                              _carouselContainer.transform = CGAffineTransformMakeScale(1, 1);

                                          }
                                          completion:^(BOOL finished) {
                                              [_carouselContainer setUserInteractionEnabled:YES];
                                          }];
                         
                         
                     }];
    
    
    
}


- (IBAction)toggleStream:(UIButton *)sender {
    
    if (sender.selected) {
        return;
    }
    
    switch (sender.tag) {
        case 32:
            _playlist.preferredStream = @"stream32";
            break;
            
        case 64:
            _playlist.preferredStream = @"stream64";
            break;
            
        case 128:
            _playlist.preferredStream = @"stream128";
            break;
            
        case 256:
            _playlist.preferredStream = @"streamHD";
            break;
            
            
        default:
            break;
    }
    
    NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                 action:@"button_press"
                                                                  label:[NSString stringWithFormat:@"toggle_stream_to_%@", _playlist.preferredStream]
                                                                  value:nil] build];
    [[[GAI sharedInstance] defaultTracker] send:dict];
    
    [self updateStreamBtns];
    
}


- (IBAction)showMenu:(id)sender {
    [self showLeftPanelAnimated:YES];
}


-(void)didSelectCannel:(Playlist *)playlist{
    [[EPData sharedEPData] setCurrentPlaylist:playlist];
    [_carouselContainer setUserInteractionEnabled:NO];
    
    [UIView animateWithDuration:.25
                          delay:.0
                        options:0 animations:^{
                            
                            [_carouselContainer setAlpha:0.0];

                        }
                     completion:^(BOOL finished) {
                         
                         if (finished) {
                             [_carouselContainer setHidden:finished];
                             
                             [UIView animateWithDuration:.25 animations:^{
                                 [self.leftPanel.view setAlpha:1.0];
                                 [self.centerPanel.view setAlpha:1.0];
                                 [self.mToolbar setAlpha:1.0];
                                 [self.mTopbar setAlpha:1.0];

                             }];
                         }
                         
                     }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqual:@"carousel"]) {
        _carouselViewController = (EPIPADCarouselViewController*) segue.destinationViewController;
        _carouselViewController.delegate = self;
    }
}

- (IBAction)tst:(id)sender {
    UISlider *slider = (UISlider *) sender;

    [_blurView setBlurRadius:    slider.value];
    [_blurView setNeedsDisplay];
}




- (void) switchToChannel{
    if (![self.centerPanel isEqual:_channelViewController]) {
        [self setCenterPanel:_channelViewController];
        //    [self _swapCenter:self.centerPanel previousState:0 with:_channelViewController];
        _headerLabel.text = _playlist.title;
        
        [(EPIPADStyledViewController *) [self centerPanel]  updateWithPlaylist:_playlist tintColor:_tintColor aTintColor:_aTintColor white:_white];

    }

}

- (void) switchToNovetly{
    self.headerLabel.text = NSLocalizedString(@"Novetly", @"");
    [self setCenterPanel:_novetlyViewController];

    [(EPIPADStyledViewController *) [self centerPanel]  updateWithPlaylist:_playlist tintColor:_tintColor aTintColor:_aTintColor white:_white];

}

- (void) switchToAlarm{
    self.headerLabel.text = NSLocalizedString(@"Alarm", @"");
    [self setCenterPanel:_alarmViewController];
    
    [(EPIPADStyledViewController *) [self centerPanel]  updateWithPlaylist:_playlist tintColor:_tintColor aTintColor:_aTintColor white:_white];

}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        self.alertView = [[UIAlertView alloc] initWithTitle:@""
                                                    message: NSLocalizedString(@"NETWORK ERROR",nil)
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
        
        [self.alertView show];
    }
}



@end
