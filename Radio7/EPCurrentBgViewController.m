//
//  EPCurrentBgViewController.m
//  Europa Plus
//
//  Created by Piezo on 2/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPCurrentBgViewController.h"
#import "FXBlurView.h"
#import "EPData.h"
#import "EPChannelScrollViewController.h"
#import "UIImage+Tint.h"
#import "UIButton+tintImage.h"
#import "UIView+Screenshot.h"

@interface EPCurrentBgViewController ()

//@property (weak, nonatomic) UIView *bgCoverContainerView;
@property (weak, nonatomic) UIImageView *bgCoverImageView;
//@property (weak, nonatomic) UIImageView *bgNoCoverImageView;
//@property (weak, nonatomic) FXBlurView *bgBlurView;

@property (nonatomic, strong) UIColor *tintColor;

@end



@implementation EPCurrentBgViewController



- (void)viewDidLoad
{
    UIImageView *bgCoverImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    
    [[self view] insertSubview:bgCoverImageView atIndex:0];

    _bgCoverImageView = bgCoverImageView;

    self.playlist = [[self currentController] playlist];
 
    __block EPCurrentBgViewController *weakSelf = self;
    
    [[[RACObserve([EPData sharedEPData], currentChannelController.bgImg) distinctUntilChanged] skip:1] subscribeNext:^(id x) {
        [weakSelf updateUIAndAnimate:YES];
    }];

    [weakSelf updateUIAndAnimate:NO];
    
    [super viewDidLoad];
}


-(EPChannelScrollViewController *) currentController{
    return [[EPData sharedEPData] currentChannelController];
}


-(void)currentControllerUpdated : (NSNotification *) aNotification{
    EPChannelScrollViewController *cntrl = [self currentController];
    self.playlist =  cntrl.playlist;
    
    [self updateUIAndAnimate:YES];
}


- (void)updateUIAndAnimate: (BOOL) animate
{
    
    UIImage *img;
    if (animate) {
        img = [self.view screenshotAfterScreenUpdates:YES];
    }
    
    
//    _bgCoverImageView.image = [[[EPData sharedEPData] currentChannelController] bgImg];
    
    [self updateUITheme];
    
    
    if (animate) {
        UIImageView *imgView= [[UIImageView alloc] initWithImage:img];
        imgView.userInteractionEnabled=NO;
        [self.view addSubview:imgView];
        
        [UIView animateWithDuration:.25
                              delay:0
                            options:0
                         animations:^{
                             imgView.alpha=0;
                         }completion:^(BOOL finished){
                             [imgView removeFromSuperview];
                         }];
        
    }
    
}

-(void) updateUITheme{
//    self.tintColor = [[[EPData sharedEPData] currentChannelController] tintColor];
//    UIColor *aTintColor = ([[[EPData sharedEPData] currentChannelController] white]<=0.65f)?[UIColor blackColor]:[UIColor whiteColor];
    self.tintColor = [UIColor blackColor];
    
    [_themeBtns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop){
        [btn setImageTintColor:_tintColor];
        [btn setTitleColor:_tintColor forState:UIControlStateNormal];
        [btn setTitleColor:_tintColor forState:UIControlStateHighlighted];
        [btn setTitleColor:_tintColor forState:UIControlStateSelected];
        
//        btn.layer.shadowColor = [aTintColor CGColor];
//        btn.layer.shadowOffset= CGSizeMake(0, 0);
//        btn.layer.shadowRadius=4;
//        btn.layer.shadowOpacity=0.2;

    }];
    
    [_themeLabels  enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger idx, BOOL *stop){
        label.textColor = _tintColor;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
