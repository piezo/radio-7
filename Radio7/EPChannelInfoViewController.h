//
//  EPChannelInfoViewController.h
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPChannelControllerDelegate.h"
#import <StreamingKit/STKAudioPlayer.h>


@interface EPChannelInfoViewController : UIViewController<STKAudioPlayerDelegate>{
}


@property (nonatomic, weak) id<EPChannelControllerDelegate> delegate;
@property (nonatomic, assign) CGFloat infoHeightProgress;

@property (nonatomic, strong) Playlist *playlist;

@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverHeightCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverHeight2Cnstr;


-(void) updateWithPlaylist : (Playlist *) playlist
                   bgImage : (UIImage *) bgImage
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white;

- (NSString *) streamUrl;
    
@end
