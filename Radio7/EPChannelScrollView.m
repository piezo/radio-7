//
//  EPChannelScrollView.m
//  Europa Plus
//
//  Created by Piezo on 4/13/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPChannelScrollView.h"

@implementation EPChannelScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(CGSize) contentSize{
    return CGSizeMake(320, 800);
}

@end
