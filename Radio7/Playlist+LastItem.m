//
//  Playlist+LastItem.m
//  Europa Plus
//
//  Created by Piezo on 2/2/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "Playlist+LastItem.h"
#import "PlaylistItem.h"

@implementation Playlist (LastItem)



-(PlaylistItem *) updateLastItem{
    NSSortDescriptor *sordDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time_end" ascending:NO];
    NSArray *sorted = [self.items sortedArrayUsingDescriptors:@[  sordDescriptor  ]];
    
    if (sorted.count==0) {
        return nil;
    }

    if (![self.lastItem isEqual:sorted[0]]) {
        self.lastItem=sorted[0];
    }
    
    return sorted[0];
}

@end
