//
//  EPWebsiteDataHelper.m
//  Europa Plus
//
//  Created by Piezoon 1/31/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPWebsiteDataHelper.h"
#import "NSString+EncodeURIComponent.h"
#import "JMImageCache.h"
#import "NSString+MD5.h"
#import "EPData.h"
#import "EPChannelViewController.h"
#import "EPChannelScrollViewController.h"
#import "EPPlaylistResponseSerializer.h"
#import "EPNoveltyResponseSerializer.h"
#import "AKTransliteration.h"
#import <AFKissXMLRequestOperation@aceontech/AFKissXMLResponseSerializer.h>
#import <DDXML.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <AFNetworking/AFURLRequestSerialization.h>
#import <AFNetworking/AFURLResponseSerialization.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>

@interface EPWebsiteDataHelper (){
    BOOL requestPlayerDataInProgress;
}


@property (nonatomic, strong) NSTimer *scheduledTimer;

//-(void) lookupImageOnDiscogsForTrack: (Track *) aTrack completeBlock :  ( void ( ^ )() )completeBlock;


@end

@implementation EPWebsiteDataHelper

static EPWebsiteDataHelper *sharedEPWebsiteDataHelperInstance = nil;

+(EPWebsiteDataHelper *) shared
{
    @synchronized(self) {
        if (sharedEPWebsiteDataHelperInstance == nil) {
            sharedEPWebsiteDataHelperInstance = [[self alloc] init];
        }
        return sharedEPWebsiteDataHelperInstance;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [[RACObserve(self, updateType) skip:1] subscribeNext:^(id x) {
            EPWebsiteDataUpdType type = [x integerValue];
            
            if (type == EPWebsiteDataUpdTypeNone) {
                [self stopDataUpdates];
            }else{
                
                [self restartPlaylistRequest];
            }
            
            
        }];
        
    }
    return self;
}


-(void)setupPlaylistsWithCompleteBlock : ( void (^)() ) completeBlock
{
    Playlist *ep = [Playlist MR_createEntity];
    ep.name = @"r7";
    ep.title = @"Радио 7";
    ep.logo = @"r7_logo";
    ep.streamHD = @"";
    ep.stream128 = @"http://radio764aac.streamr.ru/";
    ep.stream64 = @"http://radio732aac.streamr.ru/";
    ep.stream32 = @"";
    ep.priotiry = @(100);
    ep.preferredStream = @"stream128";
    
    
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error){
        completeBlock();
    }];
    
    
}

-(void) startDataUpdates{
    
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        return;
    }
    
    [self requestPlayerData];
    [self requestNovelty];
    [self requestBanners];
}



- (void)trackFromDict:(NSDictionary *)dict playlist:(Playlist *)playlist {
    NSString *hash = [[NSString stringWithFormat:@"%@%@", dict[@"artist"][@"name"], dict[@"song"][@"name"]] MD5Digest];
    
    Track *track = [Track MR_findFirstByAttribute:@"hash_id" withValue: hash ];
    
    
    if (track == nil) {
        
        track = [Track MR_createEntity];
        track.hash_id = hash;
        
        if( dict[@"artist"] )   track.artist = dict[@"artist"][@"name"];
        if( dict[@"artist_id"] ) track.artist_id = @( [dict[@"artist"][@"id"] intValue] );
//        if( dict[@"img1"] )     track.img1 = dict[@"img1"];
//        if( dict[@"img2"] )     track.img2 = dict[@"img2"];
//        if( dict[@"rating"] )   track.rating = @([dict[@"rating"] floatValue]);
        if( dict[@"song"] )     track.song = dict[@"song"][@"name"];
        if( dict[@"song_id"] )  track.song_id = @( [dict[@"song"][@"id"] intValue] );
        if( dict[@"duration"] ) track.duration = @( [dict[@"duration"] intValue] );
        
        
    }
    
    
    PlaylistItem *playlistItem = [PlaylistItem MR_findFirstByAttribute:@"start_ts" withValue:@([dict[@"start_ts"] intValue])];
    
    if (playlistItem == nil) {
        playlistItem = [PlaylistItem MR_createEntity];
        playlistItem.tid = @( [dict[@"id"] intValue] );
        playlistItem.time_start = [NSDate dateWithTimeIntervalSince1970:[dict[@"start_ts"] intValue]];
        playlistItem.time_end = [NSDate dateWithTimeIntervalSince1970:[dict[@"start_ts"] intValue] + [dict[@"duration"] intValue]  ];
        playlistItem.start_ts = @([dict[@"start_ts"] intValue]);
        
        playlistItem.track = track;
        
        if ([playlistItem.time_end timeIntervalSinceNow]<1000) {
            [playlist addItemsObject:playlistItem];
        }else{
            [playlistItem MR_deleteEntity];
        }
    }
}

- (void)processPlaylist:(NSString *)plName playlistArr:(NSArray *)playlistArr {
  
    __block Playlist *playlist = [Playlist MR_findFirstByAttribute:@"name" withValue:plName];
    
    if (playlist == nil) {
        playlist = [Playlist MR_createEntity];
        playlist.name = @"r7";
    }
    
    [playlistArr enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop){
        
        [self trackFromDict:dict playlist:playlist];
        
    }];
    
    [playlist updateLastItem];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:epPlaylistUpdateNotification
                                                        object:self
                                                      userInfo:@{@"playlist":playlist}];
    
    if (![playlist.lastItem.track.itn_complete boolValue]) {

        [self searchTrackInItunes:playlist.lastItem.track complete:^(BOOL success, Track *aTrack) {
            
            Cover *cover = aTrack.cover;
            
            
            if (success) {
                if (cover==nil && aTrack.itn_artworkUrl100!=nil) {
                    
                    
                    [self downloadImageForTrack:aTrack complete:^(BOOL success, UIImage *image, Track *aTrack) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:epItunesTrackUpdateNotification
                                                                            object:self
                                                                          userInfo:@{@"track":aTrack, @"playlist":playlist}];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:epItunesTrackCoverUpdateNotification
                                                                            object:self
                                                                          userInfo:@{@"track":aTrack, @"playlist":playlist}];
                        
                        playlist.lastItem.complete = @(YES);
                    }];
                    
                }else{
                    
                    Playlist *playlist = [Playlist MR_findFirstByAttribute:@"name" withValue:plName];
                    

                    if (playlist && aTrack) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:epItunesTrackUpdateNotification
                                                                            object:self
                                                                          userInfo:@{@"track":aTrack, @"playlist":playlist}];
                        
                        playlist.lastItem.complete = @(YES);
                    }
                    
                }
            }else{
                

                cover.img = [NSData new];
                
                Playlist *playlist = [Playlist MR_findFirstByAttribute:@"name" withValue:plName];
 
                if (playlist && aTrack) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:epItunesTrackUpdateNotification
                                                                        object:self
                                                                      userInfo:@{@"track":aTrack, @"playlist":playlist}];
                    
                    playlist.lastItem.complete = @(YES);
                }
            }
            
            
        }];

    }else{
        
    }
    
}

- (void)stopDataUpdates {
    if (self.scheduledTimer) {
        [self.scheduledTimer invalidate];
        self.scheduledTimer=nil;
    }
    
}

- (void)restartPlaylistRequest {
    //Restart
    
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        return;
    }
    
    NSDate *nearestEnd;
    BOOL restart =  YES;
    
    switch (_updateType) {
        case EPWebsiteDataUpdTypeAll:{
            NSArray *playlists = [Playlist MR_findAll];
            
            
            
            if (playlists) {
                
                __block BOOL n = YES;
                
                [playlists enumerateObjectsUsingBlock:^(Playlist *pl, NSUInteger idx, BOOL *stop) {
                    
                    if (pl.lastItem==nil) {
                        n = NO;
                    }
                }];
                
                if (n) {
                    nearestEnd = [playlists valueForKeyPath:@"lastItem.@min.time_end"];
                }else{
                    nearestEnd = [NSDate new];
                }
                
                
            }else{
                restart = NO;
            }
            
            
            break;
        }

        case EPWebsiteDataUpdTypeCurrent:{
            Playlist *currentPlaylist = iPad
            ?[[EPData sharedEPData] currentPlaylist]
            :[[[EPData sharedEPData] currentChannelController] playlist];
            
            nearestEnd = [[currentPlaylist lastItem] time_end];
            
            break;
            
        }
        case EPWebsiteDataUpdTypeNone:
            restart = NO;
            break;
            
        default:
            restart = NO;
            break;
    }
    
    if (restart) {
        NSTimeInterval intervalToNextRequest = [nearestEnd timeIntervalSinceDate:[NSDate new]];
        
//        CLS_LOG(@"interval : %f, %@", intervalToNextRequest, nearestEnd);
        
        intervalToNextRequest = intervalToNextRequest<0?15:intervalToNextRequest>60?60:intervalToNextRequest;
        
        CLS_LOG(@"interval! : %f", intervalToNextRequest);
        
        self.scheduledTimer = [NSTimer scheduledTimerWithTimeInterval:intervalToNextRequest
                                                               target:self
                                                             selector:@selector(requestPlayerData)
                                                             userInfo:nil
                                                              repeats:NO];
    }
}

-(void) requestPlayerData{
    
    if (requestPlayerDataInProgress) {
        CLS_LOG(@"[!] requestPlayerDataInProgress");
        return;
    }
    
    requestPlayerDataInProgress = YES;
    
    CLS_LOG(@"requestPlayerData");
    
    [self stopDataUpdates];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    
    NSMutableSet *set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [set  addObject:@"application/x-javascript"];
    [set  addObject:@"application/octet-stream"];
    [set  addObject:@"text/plain"];
    manager.responseSerializer.acceptableContentTypes = set;
    
    
    
    [manager GET:@"https://radio7.ru/on_air/1.json" parameters:@{@"t":@([[NSDate new] timeIntervalSince1970]) } success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        requestPlayerDataInProgress=NO;
        
        if (responseObject) {
            
            
            NSArray *playlistArr = responseObject[@"playlist"];
            NSDictionary *playlists = responseObject[@"playlists"];
            
            
            if (playlistArr) {
                
                NSString *plName = @"r7";
                [self processPlaylist:plName playlistArr:playlistArr];

            }
            
            if (playlists) {
                
                [playlists enumerateKeysAndObjectsWithOptions:0 usingBlock:^(NSString *plName, NSArray *plArr, BOOL *stop){
                    
                    [self processPlaylist:plName playlistArr:plArr];
                    
                }];
                
            }
        }
        
        
        [self restartPlaylistRequest];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        requestPlayerDataInProgress=NO;
        
        CLS_LOG(@"Error: %@", error);
        
        float interval  = 5;
        self.scheduledTimer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                               target:self
                                                             selector:@selector(requestPlayerData)
                                                             userInfo:nil
                                                              repeats:NO];

    }];
    
    
}

static NSString * const kAFCharactersToBeEscapedInQueryString = @":/?&=;!@#$()',*";


-(void)searchTrackInItunes : (Track *) aTrack
                  complete : (itunesTrackSearchComplete) completeBlock{
    
    [self searchTrackInItunes:aTrack transform:NO complete:completeBlock];
    
}

-(void)searchTrackInItunes : (Track *) aTrack
                 transform : (BOOL) aTransform
                  complete : (itunesTrackSearchComplete) completeBlock;
{
        
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://itunes.apple.com"]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    __block Track *theTrack = aTrack;
    
    NSString *termArtist = [NSString stringEncodeURIComponent2:theTrack.artist];
    NSString *termSong = [NSString stringEncodeURIComponent2:theTrack.song];
    
    if (aTransform) {
        termSong = [[[AKTransliteration alloc] initForDirection:TD_EnRu] transliterate:termSong];
    }
    
    [manager GET:@"/search"
      parameters:@{
                   @"term":[NSString stringWithFormat:@"%@+%@", termArtist, termSong],
                   @"country":@"ru",
                   @"media":@"music",
                   @"entity":@"musicTrack",
                   @"lang":@"ru_ru"
                   }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSArray *results = (NSArray *) responseObject[@"results"];
             
             if (results.count>0) {
                 theTrack.itn_collectionId = results[0][@"collectionId"];
                 theTrack.itn_artworkUrl100 =results[0][@"artworkUrl100"];
                 theTrack.itn_collectionViewUrl = results[0][@"collectionViewUrl"];
                 theTrack.itn_previewUrl = results[0][@"previewUrl"];
                 theTrack.itn_trackId = results[0][@"trackId"];
                 theTrack.itn_trackPrice = results[0][@"trackPrice"];
                 theTrack.itn_currency = results[0][@"RUB"];
                 theTrack.itn_complete_success = @(YES);
                 theTrack.itn_complete = @(YES);
                 theTrack.artist = results[0][@"artistName"];
                 theTrack.song = results[0][@"trackName"];
                 
                 completeBlock(YES, theTrack);
             }else{
                 
                 //Try Cirrilic
                 if(!aTransform){
                     [self searchTrackInItunes:aTrack transform:YES complete:completeBlock];
                 }else{
                     theTrack.itn_complete_success = @(NO);
                     theTrack.itn_complete = @(YES);

                     completeBlock(NO, theTrack);
                 }
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             CLS_LOG(@"ERROR %@ %@",operation, error);
             
             completeBlock(NO, theTrack);
         }];
    
    
}


-(void)downloadImageForTrack : (Track *) aTrack complete : (imageDownloadComplete) completeBlock{

    
    __block Cover *cover = [Cover MR_findFirstByAttribute:@"searchUrl" withValue:aTrack.itn_collectionViewUrl];
    
    
    if (cover==nil) {

        cover = [Cover MR_createEntity];
        cover.searchUrl = aTrack.itn_collectionViewUrl;
        
        NSString *thumbUrlString = aTrack.itn_artworkUrl100;
        thumbUrlString = [thumbUrlString stringByReplacingOccurrencesOfString:@"100x100" withString:@"600x600"];
        
        
        [[JMImageCache sharedCache] imageForURL:[NSURL URLWithString:thumbUrlString]
                                completionBlock:^(UIImage *image)
         {
             
             cover.img = UIImageJPEGRepresentation(image, 100);
             
             aTrack.cover = cover;

             completeBlock(YES, nil, aTrack);
             
         }];
        
    }else{
        aTrack.cover = cover;
        completeBlock(YES, nil, aTrack);
    }

    
}

-(void) requestPlaylist{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [EPPlaylistResponseSerializer serializer];
    
    NSMutableSet *set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [set addObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = set;
    
    //@"date":@"2014-02-10"
    [manager GET:@"http://www.europaplus.ru/index.php" parameters:@{@"go":@"Playlist",@"__ajax__":@"1",@"list_only":@(1)   }
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             
             [[NSNotificationCenter defaultCenter] postNotificationName:epPlaylist2UpdateNotification object:self userInfo:@{}];
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             CLS_LOG(@"Error: %@", error);
         }];
}

-(void) requestNovelty{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [EPNoveltyResponseSerializer serializer];
    
    NSMutableSet *set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [set  addObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = set;
    
    //@"date":@"2014-02-10"
    [manager GET:@"http://www.europaplus.ru/index.php" parameters:@{@"go":@"Novelty",@"__ajax__":@"1",@"list_only":@(1)   }
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             
             [[NSNotificationCenter defaultCenter] postNotificationName:epNoveltyUpdateNotification object:self userInfo:@{}];
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             CLS_LOG(@"Error: %@", error);
         }];
}

-(void) requestBanners{
    
    NSMutableArray *bannersArr = [NSMutableArray arrayWithCapacity:42];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFKissXMLResponseSerializer serializer];

    [manager GET:@"http://europaplus.ru/xml/banners.xml" parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             
             DDXMLDocument *xml = (DDXMLDocument *) responseObject;
             
             NSString *baseUrl = [(DDXMLNode *)[xml.rootElement attributeForName:@"baseUrl"] stringValue];
             NSArray *bannerElements = [xml.rootElement elementsForName:@"banner"];

             [bannerElements enumerateObjectsUsingBlock:^(DDXMLElement *obj, NSUInteger idx, BOOL *stop) {
                NSString *banPath = [[obj attributeForName:@"banPath"] stringValue];
                NSString *banPic = [[obj attributeForName:@"banPic"] stringValue];
                NSString *banPicSmall = [[obj attributeForName:@"banPicSmall"] stringValue];
                NSString *banLink = [[obj attributeForName:@"banLink"] stringValue];
                 
                 
                 [bannersArr addObject:@{
                                         @"path":[NSString stringWithFormat:@"%@%@",baseUrl,banPath],
                                         @"pic":[NSString stringWithFormat:@"%@%@",baseUrl,banPic],
                                         @"picSmall":[NSString stringWithFormat:@"%@%@",baseUrl,banPicSmall],
                                         @"link":[NSString stringWithFormat:@"%@%@",baseUrl,banLink]
                                         
                                         }];
                 
             }];
             
             [[EPData sharedEPData] setBannersArray:bannersArr];
             
             
             
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             CLS_LOG(@"Error: %@", error);
         }];
}

@end

