//
//  EPPlaylistViewController.h
//  Europa Plus
//
//  Created by Piezo on 2/11/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPCurrentBgViewController.h"
#import <STKAudioPlayer.h>
#import "EPPlaylistCell.h"

@interface EPPlaylistViewController : EPCurrentBgViewController<UITableViewDelegate, UITableViewDataSource, STKAudioPlayerDelegate, EPPlaylistCellDelegate>

@end
