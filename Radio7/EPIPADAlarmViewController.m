//
//  EPIPADAlarmViewController.m
//  Europa Plus
//
//  Created by Piezo on 5/27/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADAlarmViewController.h"
#import <iCarousel.h>
#import "EPData.h"
#import "UIButton+tintImage.h"
#import "UIImage+Tint.h"
#import "EPAlarmTableViewCell.h"

@interface EPIPADAlarmViewController ()

@property (weak, nonatomic) IBOutlet iCarousel *hourCarousel;
@property (weak, nonatomic) IBOutlet iCarousel *minuteCarousel;
@property (weak, nonatomic) IBOutlet UIView *centerContiner;

@property (strong, nonatomic)  UIColor *tintColor;
@property (nonatomic, strong) NSArray *playlists;
@property (weak, nonatomic) IBOutlet UIButton *stationBtn;
@property (weak, nonatomic) IBOutlet UIButton *stationToggleBtn;
@property (weak, nonatomic) IBOutlet UIImageView *separatorsImageView;
@property (weak, nonatomic) IBOutlet UITableView *stationTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftCnstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCnstr;

@end

@implementation EPIPADAlarmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tintColor = [UIColor blackColor];
    // Do any additional setup after loading the view.
    
    [_hourCarousel setType:iCarouselTypeLinear];
    [_hourCarousel setVertical:YES];
    [_hourCarousel setBackgroundColor:[UIColor clearColor]];
    
    [_minuteCarousel setType:iCarouselTypeLinear];
    [_minuteCarousel setVertical:YES];
    [_minuteCarousel setBackgroundColor:[UIColor clearColor]];
    
    NSDate *date = [NSDate new];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    _stationTableView.layer.masksToBounds = NO;
    _stationTableView.hidden=YES;
    
    
    if ([[EPData sharedEPData] alarmHours]>-1) {
        [_hourCarousel setCurrentItemIndex:[[EPData sharedEPData] alarmHours]];
    }else{
        [_hourCarousel setCurrentItemIndex:hour];
    }
    
    
    if ([[EPData sharedEPData] alarmMinutes]>-1) {
        [_minuteCarousel setCurrentItemIndex:[[EPData sharedEPData] alarmMinutes]];
    }else{
        [_minuteCarousel setCurrentItemIndex:minute];
    }
    
    
    _playlists = [Playlist MR_findAllSortedBy:@"priotiry" ascending:NO];
    
    [self setupSelectedRow];

    
    if ([[EPData sharedEPData] alarmDays]) {
        
        [[[EPData sharedEPData] alarmDays] enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
            [(UIButton *) [_centerContiner viewWithTag:[obj integerValue]] setSelected:YES];
        }];
        
    }
    
    
        if (UIInterfaceOrientationIsLandscape( [[UIApplication sharedApplication] statusBarOrientation] ))
        {

            _leftCnstr.constant=(1024-314-308)/2;
        }else{
            _leftCnstr.constant=225;
        }
    
    

}


-(void)updateViewConstraints{
    [super updateViewConstraints];
    self.topCnstr.constant = 100 - self.topLayoutGuide.length;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
        
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        _leftCnstr.constant=(1024-314-308)/2;
    }else{
        _leftCnstr.constant=225;
    }

}

-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white

{
    
    [super updateWithPlaylist:playlist tintColor:tintColor aTintColor:aTintColor white:white];

    self.tintColor = tintColor;
    
    [_stationToggleBtn setImageTintColor:tintColor];
    [_stationToggleBtn setTitleColor:tintColor forState:UIControlStateNormal];
    [_stationToggleBtn setTitleColor:tintColor forState:UIControlStateHighlighted];
    
    [_stationBtn setImageTintColor:tintColor];
    [_stationBtn setTitleColor:tintColor forState:UIControlStateNormal];
    [_stationBtn setTitleColor:tintColor forState:UIControlStateHighlighted];
    
    
    _separatorsImageView.image = [[UIImage imageNamed:@"alarm_delimiters"] imageTintedWithColor:tintColor];
    

    [_hourCarousel reloadData];
    [_minuteCarousel reloadData];
    [_stationTableView reloadData];
    [self setupSelectedRow];
}

- (void)setupSelectedRow
{
    NSInteger idx = 0;
    
    if ([[EPData sharedEPData] alarmPlaylist]) {
        
        idx = [_playlists indexOfObject:[[EPData sharedEPData] alarmPlaylist]];
        
    }
    
    [_stationTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]
                                   animated:NO
                             scrollPosition:UITableViewScrollPositionNone];
}

- (IBAction)toggleDay:(id)sender {
    [(UIButton *) sender setSelected:![(UIButton*) sender isSelected]];
    
    [self setupAlarms];
    
}


#pragma mark
#pragma mark iCarouselDataSource


- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    
    
    NSInteger number = 0;
    
    switch (carousel.tag) {
        case 101:
            number = 24;
            break;
            
        case 102:
            number = 60;
            break;
            
            
        default:
            break;
    }
    
    
    return number;
    
}

- (void)setupAlarms {
    NSInteger hours = [_hourCarousel currentItemIndex];
    NSInteger minutes = [_minuteCarousel currentItemIndex];
    
    NSMutableArray *days = [NSMutableArray arrayWithCapacity:42];
    
    for (int i = 201; i<208; i++ ) {
        if ( [(UIButton *) [_centerContiner viewWithTag:i] isSelected] ) {
            [days addObject:@(i)];
        }
    }
    
    NSIndexPath *path = [_stationTableView indexPathForSelectedRow];
    Playlist *pl =_playlists[path.row];
    
    [[EPData sharedEPData] setupTimerAlarmsForHour:hours
                                      minutes:minutes
                                         days:days
                                   channelIdx:pl.name
                                     playlist:pl];
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    
    [self setupAlarms];
    
    
}


- (void)clockView:(UIView **)view_p index:(NSUInteger)index {
    UILabel *label;
    
    if (*view_p == nil) {
        *view_p = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 130, 130)];
        label = [[UILabel alloc] initWithFrame:(*view_p).bounds];
        label.tag = 100;
        
        NSMutableArray * mthemeLabels = [NSMutableArray arrayWithArray: self.themeLabels];
        [mthemeLabels addObject:label];
        self.themeLabels=mthemeLabels;
        
        [*view_p addSubview:label];
    }
    
    label = (UILabel *) [*view_p viewWithTag:100];
    
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:96];
    label.text = [NSString stringWithFormat:@"%02lu", (unsigned long)index];
    label.textColor=_tintColor;
    label.textAlignment = NSTextAlignmentCenter;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    switch (carousel.tag) {
        case 101:
            [self clockView:&view index:index];
            break;
        case 102:
            [self clockView:&view index:index];
            break;
        
        default:
            break;
    }
    
    return view;
}

#pragma mark
#pragma mark iCarouselDelegate


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    
    switch (carousel.tag) {
        case 103:
            switch (option) {
                case iCarouselOptionWrap:
                    return 0.0;
                    break;
                    
                    
                default:
                    break;
            }
            break;
            
        default:{
            switch (option) {
                case iCarouselOptionWrap:
                    return 1.0;
                    break;
                    
                case iCarouselOptionFadeMax:
                    return 0.0f;
                    break;
                case iCarouselOptionFadeMin:
                    return 0.0f;
                    break;
                case iCarouselOptionVisibleItems:
                    return 3.0f;
                    break;
                    
                    
                    
                default:
                    break;
            }
            break;
        }
            
    }
    
    return value;
}


#pragma mark
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _playlists.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CCell";
    EPAlarmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.mTextLabel.text = [(Playlist *) _playlists[indexPath.row] title];
    cell.mTextLabel.textColor = _tintColor;
    cell.backgroundColor=[UIColor clearColor];
    

    
    UIView *bgView = [[UIView alloc] initWithFrame: CGRectInset(cell.bounds, 10, 10) ];
    bgView.layer.cornerRadius = 10;
    bgView.layer.borderColor = _tintColor.CGColor;
    bgView.layer.borderWidth = 1;
    bgView.layer.backgroundColor = [[UIColor clearColor] CGColor];
    
    cell.selectedBackgroundView = bgView;
    cell.layer.masksToBounds = NO;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self setupAlarms];
}




@end
