//
//  EPIPADCoverViewController.h
//  Europa Plus
//
//  Created by Piezoon 5/23/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPIPADCoverViewController : UIViewController{
//    PlaylistItem *plItem;
//    Playlist *playlist;
}
@property (nonatomic, strong) Playlist *playlist;
@property (nonatomic, strong) PlaylistItem *plItem;

@end
