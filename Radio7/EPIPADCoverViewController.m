//
//  EPIPADCoverViewController.m
//  Europa Plus
//
//  Created by Piezo on 5/23/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADCoverViewController.h"

@interface EPIPADCoverViewController ()

@end

@implementation EPIPADCoverViewController{
    
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UIImageView *logoImageView;
    __weak IBOutlet UIImageView *logo2ImageView;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    __block EPIPADCoverViewController *weakSelf = self;
    __block Playlist *weakPlaylist = self.playlist;
    
    RACSignal *img_signal = [_playlist rac_valuesForKeyPath:@"lastItem.track.cover.img" observer:self];
    RACSignal *img_itn_complete = [_playlist rac_valuesForKeyPath:@"lastItem.track.itn_complete" observer:self];
    
    
    [[RACSignal combineLatest:@[img_signal, img_itn_complete]] subscribeNext:^(RACTuple *x) {
        
        
        if ([x.second boolValue]) {
            if (x.first) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    imageView.image = [UIImage imageWithData:x.first];
                    logoImageView.hidden = YES;
                    logo2ImageView.hidden = NO;
                    
                });
                
                
            }else if(weakPlaylist.lastItem.track.cover.url == nil){
                dispatch_async(dispatch_get_main_queue(), ^{
                    imageView.image = [UIImage imageNamed:@"no_cover"];
                    logoImageView.hidden = NO;
                    logo2ImageView.hidden = YES;
                });
                
                
            }
            
            logo2ImageView.image = [UIImage imageNamed:weakPlaylist.logo];;
            logoImageView.image = [UIImage imageNamed:weakPlaylist.logo];
            
        }
    }];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
