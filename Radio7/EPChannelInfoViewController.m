//
//  EPChannelInfoViewController.m
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPChannelInfoViewController.h"
#import <CBAutoScrollLabel.h>
#import "UIColor+Utils.h"
#import "EPPlayIButton.h"
#import "EPVolumeView.h"
#import "UIButton+tintImage.h"
#import "EPData.h"

@interface EPChannelInfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *controlsView;
@property (weak, nonatomic) IBOutlet UIView *controlsViewForSecondStage;
@property (weak, nonatomic) IBOutlet UIView *controlsView2;
@property (weak, nonatomic) IBOutlet UIView *playAndVolumeView;
@property (weak, nonatomic) IBOutlet EPVolumeView *volumeView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverXCenterCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsViewLeftCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsViewRightCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsViewBottomCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsViewTopCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsViewForSecondStageLeftCnstr;

//@property (weak, nonatomic) IBOutlet UIImageView *logo1ImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *logo2ImageView;

@property (strong, nonatomic) IBOutletCollection(EPPlayIButton) NSArray *playerBtns;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *itunesBtns;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *themeBtns;

@property (strong, nonatomic) IBOutletCollection(CBAutoScrollLabel) NSArray *scrollLabels;
@property (strong, nonatomic) IBOutletCollection(CBAutoScrollLabel) NSArray *artistScrollLabels;
@property (strong, nonatomic) IBOutletCollection(CBAutoScrollLabel) NSArray *recordingScrollLabels;
@property (strong, nonatomic) IBOutletCollection(CBAutoScrollLabel) NSArray *stage1scrollLabels;
@property (strong, nonatomic) IBOutletCollection(CBAutoScrollLabel) NSArray *stage2scrollLabels;


@end

@implementation EPChannelInfoViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [_controlsView setBackgroundColor:[UIColor clearColor]];
    [_controlsView2 setBackgroundColor:[UIColor clearColor]];
    [_playAndVolumeView setBackgroundColor:[UIColor clearColor]];
    
    _volumeView.showsRouteButton = YES;
    
    //Setup scroll labels
    
    [_scrollLabels enumerateObjectsUsingBlock:^(CBAutoScrollLabel *label,
                                                NSUInteger idx,
                                                BOOL *stop)
    {
        label.font = [UIFont fontWithName:@"HelveticaNeue-" size:18];
        label.labelSpacing = 35; // distance between start and end labels
        label.pauseInterval = 0; // seconds of pause before scrolling starts again
        label.scrollSpeed = 30; // pixels per second
        label.fadeLength = 12.f; // length of the left and right edge fade, 0 to disable
        label.text = @" ";
        
    }];
    
    [_stage1scrollLabels enumerateObjectsUsingBlock:^(CBAutoScrollLabel *label, NSUInteger idx, BOOL *stop){
        label.textAlignment = NSTextAlignmentCenter;
    }];
    
    [_stage2scrollLabels enumerateObjectsUsingBlock:^(CBAutoScrollLabel *label, NSUInteger idx, BOOL *stop){
        label.textAlignment = NSTextAlignmentLeft;
         
    }];
    
    [_itunesBtns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
       [btn setTitle:@"" forState:UIControlStateNormal];
    }];
    
    
    
    _controlsViewForSecondStageLeftCnstr.constant = 0;
    
    __block EPChannelInfoViewController *weakSelf = self;
    

    RACSignal *heightProgressSignal =RACObserve(self, infoHeightProgress);

    RACSignal *stagesProgress = [heightProgressSignal map:^id(id value) {

        BOOL isFirstStage = [value floatValue]>.4;
        CGFloat firstStageProgress = 1.0-([value floatValue] - 0.4)/.6;
        CGFloat secondStageProgress = 1.0-[value floatValue]/.4;
        
        return RACTuplePack(@(isFirstStage), @(firstStageProgress),@(secondStageProgress));
    }];
    
    RAC(self, coverXCenterCnstr.constant)= [heightProgressSignal map:^id(id value) {
        return @(((weakSelf.view.bounds.size.width-20-95)/2)*(1-[value floatValue]));
    }];
    
    
    [stagesProgress subscribeNext:^(RACTuple *value) {
        BOOL isFirstStage = [[value first] boolValue];
        
        weakSelf.controlsView.alpha = isFirstStage?1.0 - [value.second floatValue]:[value.third floatValue];
        weakSelf.controlsView.hidden = !isFirstStage;
        
        weakSelf.controlsViewForSecondStage.hidden = isFirstStage;
        weakSelf.controlsViewForSecondStage.alpha = [value.third floatValue];

    }];

    
    
    __block STKAudioPlayer *weakPlayer = [[EPData sharedEPData] player];
    
    RACSignal *myPlayerState_Signal = [RACObserve([[EPData sharedEPData] player], state) filter:^BOOL(id x) {
        
        STKAudioPlayerState state = [x intValue];
        NSString *urlStr = [[NSString alloc] init];
        
        if (state == STKAudioPlayerStateBuffering && weakPlayer.pendingQueueCount>0) {
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.pendingQueue[0]];
        }else{
            urlStr = [NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId];
        }
        
        
        return [urlStr isEqualToString:[weakSelf streamUrl]];
        
    }];
   
    RACSignal *playlistChanged_Signal  = [RACObserve(self, playlist) distinctUntilChanged] ;
    
//    [playlistChanged_Signal subscribeNext:^(id x) {
//        weakSelf.logo1ImageView.image = [UIImage imageNamed:_playlist.logo];
//        weakSelf.logo2ImageView.image = [UIImage imageNamed:_playlist.logo];
//    }];
//    
    RACSignal *preferredStreamChanged_Signal  = [RACObserve(self, playlist.preferredStream) distinctUntilChanged] ;

    [[RACSignal combineLatest:@[
                                playlistChanged_Signal,
                                preferredStreamChanged_Signal,
                                RACObserve([EPData sharedEPData], currentChannelController)
                                ]] subscribeNext:^(RACTuple *x)
    {
        
        
        if (x.first) {
            
            BOOL stateIsPlaPlaying = [weakPlayer state] == STKAudioPlayerStatePlaying;
            BOOL playerOnChannel = [weakPlayer.delegate isKindOfClass:[EPChannelInfoViewController class]];
            BOOL currentlyPlayingIsNotMy = ![[NSString stringWithFormat:@"%@", weakPlayer.currentlyPlayingQueueItemId] isEqualToString:[weakSelf streamUrl]];
            BOOL isMine = [self.delegate isEqual:[[EPData sharedEPData] currentChannelController]];
            
            if ( stateIsPlaPlaying
                && playerOnChannel
                && currentlyPlayingIsNotMy
                && isMine)
            {
                NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];
                [weakPlayer setDelegate:weakSelf];
                [weakPlayer playURL:url];
                CLS_LOG(@"start playing -  %@", weakSelf.playlist.name);
                CLS_LOG(@"stateIsPlaPlaying : %@", @(stateIsPlaPlaying));
                CLS_LOG(@"playerOnChannel : %@", @(playerOnChannel));
                
                
            }
            
        }
        
    }];
    
    
    [_playerBtns enumerateObjectsUsingBlock:^(EPPlayIButton *playerButton, NSUInteger idx, BOOL *stop) {
        
        RAC(playerButton, playerState) = [RACSignal combineLatest:@[
                                                                    [myPlayerState_Signal distinctUntilChanged],
                                                                    [RACObserve(weakPlayer, delegate) distinctUntilChanged]
                                                                    ]
                                                           reduce:^id{
                                                               STKAudioPlayerState state;
                                                               
                                                               if (![weakPlayer.delegate isEqual:weakSelf]) {
                                                                   state = STKAudioPlayerStateStopped;
                                                               }else{
                                                                   state = weakPlayer.state;
                                                               }
                                                               
                                                               return @(state);
                                                           }];
        
        
        [[playerButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            
            
            NSDictionary *dict;
            
            if (   playerButton.playerState == STKAudioPlayerStatePlaying
                || playerButton.playerState == STKAudioPlayerStateBuffering)
            {
                
                [weakPlayer stop ];
                [weakPlayer setDelegate:nil];
                
                dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"pause"
                                                                value:nil] build];
                
                
            }else{
                NSURL *url = [NSURL URLWithString:[weakSelf streamUrl]];
                [weakPlayer setDelegate:weakSelf];
                [weakPlayer playURL:url ];
                
                
                
                dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"play"
                                                                value:nil] build];
                
                
            }
            
            [[[GAI sharedInstance] defaultTracker] send:dict];
            
        }];
        
    }];
    
}

- (NSString *) streamUrl{
    return [_playlist valueForKey:[_playlist preferredStream]];
}

-(void) updateWithPlaylist : (Playlist *) playlist
                   bgImage : (UIImage *) bgImage
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white
{
 
    BOOL isBlack = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:tintColor]<.02;
    
    _logoImageView.image=[UIImage imageNamed:isBlack?@"Logo_R7":@"Logo_R7__white"];
    
    self.playlist = playlist;
    
    PlaylistItem *_plItem = _playlist.lastItem;
    
    [_headerLabel setText:_playlist.title];

    
//    _bgImageView.image = bgImage;
    
    if (_plItem.track.cover.img) {
        _coverImageView.image = [UIImage imageWithData:_plItem.track.cover.img];
        
//        _logo1ImageView.hidden=YES;
//        _logo2ImageView.hidden=NO;
    }else{
        _coverImageView.image = [UIImage imageNamed:@"no_cover"];

//        _logo1ImageView.hidden=NO;
//        _logo2ImageView.hidden=YES;
    }
    
    [_artistScrollLabels enumerateObjectsUsingBlock:^(CBAutoScrollLabel *label, NSUInteger idx, BOOL *stop) {
       
        [label setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
        [label setText:_plItem.track.artist];
        
    }];
    
    [_recordingScrollLabels enumerateObjectsUsingBlock:^(CBAutoScrollLabel *label, NSUInteger idx, BOOL *stop) {
        [label setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]];
        [label setText:_plItem.track.song];
        
    }];
    
    
    [_itunesBtns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
        UIImage *itunesImage = [EPStyleKit imageOfITunesSmallBadgeWithTintIsBlack:isBlack];
        [btn setImage:itunesImage forState:UIControlStateNormal];
        [btn setNeedsDisplay];
    }];
    
    //Tint
    
    [_scrollLabels enumerateObjectsUsingBlock:^(CBAutoScrollLabel *label, NSUInteger idx, BOOL *stop) {
        [label setTextColor:tintColor];
    }];
    
    [_playerBtns enumerateObjectsUsingBlock:^(EPPlayIButton *btn, NSUInteger idx, BOOL *stop) {
        [btn setBackgroundColor:[UIColor clearColor]];
        [btn setTintIsBlack:isBlack];
    }];
    
    [_volumeView setTintColor:tintColor];
    
    [_themeBtns enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
        [btn setImageTintColor:tintColor];
    }];
    
    [_headerLabel setTextColor:tintColor];
    
}

- (IBAction)menuBtnDidPress:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(menuBtnDidPress)]) {
        [_delegate menuBtnDidPress];
    }
}

- (IBAction)channelsBtnDidPress:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(channelsBtnDidPress)]) {
        [_delegate channelsBtnDidPress];
    }

}


- (IBAction)showInItunes:(id)sender {
    
    Track *track = _playlist.lastItem.track;
    NSURL *url = [NSURL URLWithString:[track.itn_collectionViewUrl stringByAppendingString:@"&at=10ldCb&ct=r7_ios_app"]];
    
    [[UIApplication sharedApplication] openURL:url];
    
//    NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"iTunes"
//                                                                 action:@"Show_In_iTunes"
//                                                                  label:[NSString stringWithFormat:@"%@ %@ %@", track.artist, track.song, [track.itn_collectionViewUrl stringByAppendingString:@"&at=10lsPX&ct=ep_ios_app"]]
//                                                                  value:nil] build];
//    
//    
//    [[[GAI sharedInstance] defaultTracker] send:dict];
    
    
    
//    dict = [[GAIDictionaryBuilder createTransactionWithId:[NSString stringWithFormat:@"%@", track.itn_trackId]
//                                              affiliation:@"iTunes"
//                                                  revenue:track.itn_trackPrice
//                                                      tax:@0.0F
//                                                 shipping:@1
//                                             currencyCode:track.itn_currency] build];
//    
//    [[[GAI sharedInstance] defaultTracker] send:dict];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/// Raised when an item has started playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId{}
/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId{}
/// Raised when the state of the player has changed
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState{}
/// Raised when an item has finished playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration{
}
/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode{}


@end
