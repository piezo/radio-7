//
//  EPIPADNovetlyViewController.m
//  Europa Plus
//
//  Created by Piezo on 5/26/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPIPADNovetlyViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+Tint.h"
#import "UIButton+tintImage.h"
#import "UIColor+Utils.h"

@interface EPIPADNovetlyViewController (){
        EPIPADPlaylistViewController *_playlistController;
}


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCnstr;

@end

@implementation EPIPADNovetlyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)updateViewConstraints{
    [super updateViewConstraints];
    self.topCnstr.constant = 80.0 - self.topLayoutGuide.length;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white
{
    
    [_playlistController setTintColor:tintColor];
    [_playlistController setATintColor:aTintColor];
    [_playlistController setPlaylist:playlist];
    
    [[_playlistController collectionView] reloadData];
    
    [[_playlistController collectionView] scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    
    

    [super updateWithPlaylist:playlist tintColor:tintColor aTintColor:aTintColor white:white];
    
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"playlist"]) {
        
        _playlistController = (EPIPADPlaylistViewController *) segue.destinationViewController ;
        _playlistController.delegate = self;
        
    }
    
}


#pragma mark -  EPIPADPlaylistViewControllerDelegate

-(NSArray *)playlistCntr: (EPIPADPlaylistViewController*) playlistCntr sortedPLItemsWithRemFirst : (BOOL) remFirst{
//    NSSortDescriptor *time_start_Descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time_start" ascending:NO];
//    NSArray *sorted = [[_playlist items] sortedArrayUsingDescriptors:[NSArray arrayWithObject:time_start_Descriptor]];
//    
//    NSMutableArray *msorted = [NSMutableArray arrayWithArray:sorted];
//    
//    if (msorted.count>0) {
//        [msorted removeObjectAtIndex:0];
//    }
    
    
    NSArray *sorted = [SiteNoveltyItem MR_findAllSortedBy:@"sort" ascending:YES];
    
    return sorted;
}

-(void) playlistCntr : (EPIPADPlaylistViewController*) playlistCntr
          fillupCell : (EPPlaylistCollectionCell *) cell
             atIndex : (NSIndexPath *) indexPath
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        SiteNoveltyItem *plItem = [[playlistCntr sortedPLItems] objectAtIndex:indexPath.row];
        
        cell.backgroundColor = [UIColor clearColor];
        
        
        cell.cover.image = [[UIImage alloc] init];
        cell.artistLabel.text = plItem.artist;
        cell.recordingLabel.text = plItem.recording;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        
        cell.timeLabel.text = plItem.time;
        
        [cell.cover setImageWithURL:[NSURL URLWithString:plItem.imgURL] placeholderImage:[[UIImage alloc] init]];
        
        cell.streamUrl = plItem.mp3;
        
        //    CGFloat _radius = 4;
        //    CGFloat _opacity = .2;
        //        label.layer.shadowColor = [_aTintColor CGColor];
        //        label.layer.shadowOffset= CGSizeMake(0, 0);
        //        label.layer.shadowRadius=_radius;
        //        label.layer.shadowOpacity=_opacity;
        
        
        [@[ cell.timeLabel, cell.artistLabel, cell.recordingLabel] enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger idx, BOOL *stop) {
            label.textColor = playlistCntr.tintColor;
        }];
        if (playlistCntr.tintColor) {
            CGFloat cDist = [UIColor findDistanceBetweenTwoColor:[UIColor blackColor] secondColor:playlistCntr.tintColor];
            [cell.playerButton setTintIsBlack:cDist<.02];
        }
    });
    
    

}

@end
