//
//  EPChannelScrollViewController.m
//  Europa Plus
//
//  Created by Piezo on 6/8/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "EPChannelScrollViewController.h"
#import "EPChannelInfoViewController.h"
#import "EPChannelTableViewController.h"
#import "EPData.h"
#import "FXBlurView.h"
#import "UIView+Screenshot.h"
#import "UIColor+Grayscale.h"
#import <UIImage+AverageColor.h>
#import "UIButton+tintImage.h"

static const NSInteger kBtrButtonWidth = 40.0f;

@interface EPChannelScrollViewController (){

    EPChannelTableViewController *_playlistCntr;
    
    __weak IBOutlet NSLayoutConstraint *_infoHeightCnstr;

    
    BOOL _UIHidden;
    BOOL _needsUpdate;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoTopCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playlistTopCnstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playlistHeightCnstr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarBottomCnstr;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (nonatomic, strong) IBOutlet UIImageView *bgCoverImageView;
@property (nonatomic, strong) FXBlurView *bgBlurView;
@property (strong, nonatomic) UIView *previewImageView;

@property (weak, nonatomic) IBOutlet UIButton *btrBtn32;
@property (weak, nonatomic) IBOutlet UIButton *btrBtn64;
@property (weak, nonatomic) IBOutlet UIButton *btrBtn128;
@property (weak, nonatomic) IBOutlet UIButton *btrBtnHD;

@property (nonatomic, assign) CGFloat toolbarHeight;
@property (nonatomic, assign) CGFloat infoMaxHeight;
@property (nonatomic, assign) CGFloat infoMinHeight;
@property (nonatomic, assign, getter=isCanUpdate) BOOL canUpdate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btrBtn32Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btrBtn64Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btrBtn128Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btrBtnHDWidth;


@end

@implementation EPChannelScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __block EPChannelScrollViewController *weakSelf = self;

    _canUpdate = YES;
    _toolbarHeight = 30.0;
    
    _infoMinHeight = 180;

    RACSignal *infoMaxHeightSignal = [RACObserve(self, toolbarHeight) map:^id(id value) {
        return @([weakSelf.view bounds].size.height-[value floatValue]);
    }];

    RAC(self, infoMaxHeight) = infoMaxHeightSignal;

    CGFloat ios6TopFix =  !IS_RETINA?2.0:IS_OS_7_OR_LATER?1:1.5;
    
    _infoCntr.coverHeightCnstr.priority = 750;
    _infoCntr.coverHeight2Cnstr.priority = 950;
    
    RAC(self, playlistTopCnstr.constant) = infoMaxHeightSignal;
    
    
    
    CGFloat sideSize = self.view.height;
    
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _bgCoverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, sideSize, sideSize)];
    _bgCoverImageView.center = self.view.center;
    
    [FXBlurView setBlurEnabled:YES];
    [FXBlurView setUpdatesEnabled];
    
    
    _bgBlurView = [[FXBlurView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [_bgBlurView setBlurRadius:90.0];
    [_bgBlurView setDynamic:NO];
    [_bgBlurView setTintColor:[UIColor clearColor]];
    
    [_bgView insertSubview:_bgBlurView atIndex:0];
    [_bgView insertSubview:_bgCoverImageView atIndex:0];
    [self.view insertSubview:_bgView atIndex:0];
    
    
    RACSignal *plitem_complete_signal = RACObserve(self, playlist.lastItem.complete) ;
    
    [plitem_complete_signal subscribeNext:^(id x) {
        
        if ([x boolValue]) {
            
            __block BOOL animate = weakSelf.playlist.lastItem.track.cover.img!=nil;
            
            if ([weakSelf isEqual:[[EPData sharedEPData] currentChannelController]]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf updateUI:animate sender:weakSelf];
                });
            }else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf updateUI:animate sender:weakSelf];
                    });
                });
            }
        }
        
    }];
    
    RACSignal *scrollViewContentOffsetY_Signal = [RACObserve(self, scrollView.contentOffset) map:^id(NSValue *value) {
        return @(  [value CGPointValue].y );
    }];
    

    RACSignal *infoHeightSignal = [RACSignal combineLatest:@[scrollViewContentOffsetY_Signal,
                                                             infoMaxHeightSignal]
                                                    reduce:^id{
                                                        NSNumber *height = @( fmin(_infoMaxHeight, fmaxf(_infoMinHeight, _infoMaxHeight-weakSelf.scrollView.contentOffset.y))  );
                                                        
                                                        return height;
                                                    }];
    
    
    
    RAC(_infoTopCnstr, constant) = [scrollViewContentOffsetY_Signal map:^id(id value) {
        return @( [value floatValue]*ios6TopFix );
    }];
    
    RAC(_infoHeightCnstr, constant) = infoHeightSignal;
    
    RACSignal *infoHeightProgressSignal = [infoHeightSignal map:^id(id value) {
        return @(  ([value floatValue] - _infoMinHeight) / (_infoMaxHeight-_infoMinHeight));
    }];
    
    RAC(self, infoCntr.infoHeightProgress) = infoHeightProgressSignal;
    
    RAC(self, playlistHeightCnstr.constant) = [RACSignal combineLatest:@[
                                                                         RACObserve(_playlistCntr, sortedPLItems),
                                                                         RACObserve(self, toolbarHeight)
                                                                         ]
                                                                reduce:^id{
                                                                   return @( fmaxf([_playlistCntr.sortedPLItems count]*79, weakSelf.view.height-weakSelf.toolbarHeight-_infoMinHeight));
                                                                }];
    
    
    
    [RACObserve(self, bgBlurView.blurredImage) subscribeNext:^(UIImage *x) {
        weakSelf.bgImg = x;
    }];
    
    /*

    _infoCntr.view.layer.contentsGravity = kCAGravityBottom;
    
    RAC(self, infoCntr.view.layer.contents) = RACObserve(self, bgBlurView.layer.contents);
    RAC(self, infoCntr.view.layer.contentsScale) = [RACObserve(self, bgBlurView.layer.contents) map:^id(id value) {
        CGImageRef ref =  (__bridge CGImageRef)value;
        return @(CGImageGetWidth(ref)/320.0);
    }];
     */
    
    RAC(self, infoCntr.view.backgroundColor) = [[RACObserve(self, bgBlurView.layer.contents)
                                    filter:^BOOL(id value) {
                                        return  value!=nil;
                                    }]
                                   
                                   map:^id(id value) {
       
                                       CGImageRef ref =  (__bridge CGImageRef)value;
                                       CGFloat scale = CGImageGetWidth(ref)/320.0;
                                       UIImage *img = [UIImage imageWithCGImage:ref
                                                                          scale:scale
                                                                    orientation:UIImageOrientationUp];
                                    
                                       return [UIColor colorWithPatternImage:img];
                                }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self updateViewConstraints];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateViewConstraints];
}

- (void)updateUI:(BOOL)blur sender : (id) sender{
    
    
    
    if (!_canUpdate) {
        _needsUpdate = YES;
        CLS_LOG(@"can not updateUI %@", self.playlist.name);
        return;
    }
    
    CLS_LOG(@"updateUI %@", self.playlist.name);
    
    _needsUpdate = NO;
    
    BOOL animation = NO;
    
    UIImage *screenshotForAnimation;
    __block UIImageView *screenshotForAnimationImageView;
    __block EPChannelScrollViewController *weakSelf = self;
    __block id weakSender = sender;
    
    if ([[[(EPCarouselViewController*) _delegate carousel] currentItemView] isEqual:self.view]) {
        animation = YES;
        screenshotForAnimation = [self.view screenshotAfterScreenUpdates:YES];
        screenshotForAnimationImageView = [[UIImageView alloc] initWithImage:screenshotForAnimation];
        screenshotForAnimationImageView.tag = 101;
        screenshotForAnimationImageView.userInteractionEnabled=NO;
        [[self view] addSubview:screenshotForAnimationImageView];
        _canUpdate = NO;
    }
    
    PlaylistItem *_plItem = _playlist.lastItem;
    
    if (_plItem.track.cover.img) {
        _bgCoverImageView.image = [UIImage imageWithData:_plItem.track.cover.img];
        [_bgBlurView setNeedsDisplay];
        
    }else{
        _bgCoverImageView.image = [UIImage imageNamed:@"no_cover"];
        [_bgBlurView setNeedsDisplay];
    }
    
    UIImage *l_bgImg = [self.bgView screenshotAfterScreenUpdates:YES];
    UIColor *avgColor = [l_bgImg averageColor];
    
    [avgColor.grayscale getWhite:&_white alpha:nil];
    
    self.tintColor = (_white<=0.45f)?[UIColor whiteColor]:[UIColor blackColor];
    self.aTintColor = (_white<=0.45f)?[UIColor blackColor]:[UIColor whiteColor];
    
    
    [@[_btrBtn32, _btrBtn64, _btrBtn128, _btrBtnHD] enumerateObjectsUsingBlock:^(UIButton *btn, NSUInteger idx, BOOL *stop) {
        [btn setImageTintColor:_tintColor];
        
    }];
    
    [self updateStreamBtns];
//!!!
//    self.bgImg = l_bgImg;
    
    [_infoCntr updateWithPlaylist:_playlist
                          bgImage:l_bgImg
                        tintColor:_tintColor
                       aTintColor:_aTintColor
                            white:.4];
    
    [_playlistCntr updateWithPlaylist:_playlist
                            tintColor:_tintColor
                           aTintColor:_aTintColor white:_white];
    
    if (animation) {
        [UIView animateWithDuration:.25 animations:^{
            screenshotForAnimationImageView.alpha=0;
        } completion:^(BOOL finished) {
            [screenshotForAnimationImageView removeFromSuperview];
            screenshotForAnimationImageView = nil;
            weakSelf.canUpdate=YES;
            
            if (_needsUpdate) {

                [weakSelf updateUI:blur sender:weakSender];
            }
        }];
    }

}

- (void)updateStreamBtns {
    //    _mTopbar.backgroundColor = [_tintColor colorWithAlphaComponent:.1];
    //    _mToolbar.backgroundColor = [_tintColor colorWithAlphaComponent:.1];
    
    _btrBtn32.hidden =  [_playlist.stream32 isEqualToString:@""];
    _btrBtn64.hidden =  [_playlist.stream64 isEqualToString:@""];
    _btrBtn128.hidden = [_playlist.stream128 isEqualToString:@""];
    _btrBtnHD.hidden =  [_playlist.streamHD isEqualToString:@""];
    
    _btrBtn32Width.constant = _btrBtn32.hidden ? 0.0f : kBtrButtonWidth;
    _btrBtn64Width.constant = _btrBtn64.hidden ? 0.0f : kBtrButtonWidth;
    _btrBtn128Width.constant = _btrBtn128.hidden ? 0.0f : kBtrButtonWidth;
    _btrBtnHDWidth.constant = _btrBtnHD.hidden ? 0.0f : kBtrButtonWidth;
    
    _btrBtn32.selected = [_playlist.preferredStream isEqualToString:@"stream32"];
    _btrBtn64.selected = [_playlist.preferredStream isEqualToString:@"stream64"];
    _btrBtn128.selected = [_playlist.preferredStream isEqualToString:@"stream128"];
    _btrBtnHD.selected = [_playlist.preferredStream isEqualToString:@"streamHD"];
    
    
    int c = [@(!_btrBtn32.hidden) intValue] + [@(!_btrBtn64.hidden) intValue] + [@(!_btrBtn128.hidden) intValue] + [@(!_btrBtnHD.hidden) intValue];
    
    if (c < 2) {
        _btrBtn32.hidden = _btrBtn64.hidden = _btrBtn128.hidden = _btrBtnHD.hidden = YES;
        _toolbarBottomCnstr.constant = -30;
        
        _infoCntr.coverHeightCnstr.priority = 950;
        _infoCntr.coverHeight2Cnstr.priority = 750;

        self.toolbarHeight = 0;
        
    }else{
        _toolbarBottomCnstr.constant = 0;
        
        _infoCntr.coverHeightCnstr.priority = 750;
        _infoCntr.coverHeight2Cnstr.priority = 950;
        
        self.toolbarHeight = 30;
    }
}

-(void)updateViewConstraints{
    [super updateViewConstraints];
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        _infoCntr.topCnstr.constant = self.topLayoutGuide.length>0?0:20;
    }else{
        _infoCntr.topCnstr.constant = 20;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (Playlist *) playlistForCntr : (UIViewController *) controller{
    return self.playlist;
}

-(UIImage *) coverScreenshot{
    return [_infoCntr.coverView screenshotAfterScreenUpdates:YES];
}




#pragma mark - ScrollView

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    CGFloat targetY = _infoMaxHeight-_infoMinHeight;
    if ( (*targetContentOffset).y<targetY/2) {
        (*targetContentOffset).y = 0;
    }else if((*targetContentOffset).y<targetY+100){
        (*targetContentOffset).y = targetY;
    }
    
}

#pragma mark Actions

- (void) menuBtnDidPress{
    if (_delegate && [_delegate respondsToSelector:@selector(menuBtnDidPress)]) {
        [_delegate menuBtnDidPress];
    }
}

- (void) channelsBtnDidPress{
    if (_delegate && [_delegate respondsToSelector:@selector(channelsBtnDidPress)]) {
        [_delegate channelsBtnDidPress];
    }
}

- (IBAction)toggleStream:(UIButton *)sender {
    
    if (sender.selected) {
        return;
    }
    
    switch (sender.tag) {
        case 32:
            _playlist.preferredStream = @"stream32";
            break;
            
        case 64:
            _playlist.preferredStream = @"stream64";
            break;
            
        case 128:
            _playlist.preferredStream = @"stream128";
            break;
            
        case 256:
            _playlist.preferredStream = @"streamHD";
            break;
            
            
        default:
            break;
    }
    
    NSDictionary *dict = [[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                 action:@"button_press"
                                                                  label:[NSString stringWithFormat:@"toggle_stream_to_%@", _playlist.preferredStream]
                                                                  value:nil] build];
    [[[GAI sharedInstance] defaultTracker] send:dict];
    
    [self updateStreamBtns];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"info"]) {
        _infoCntr = (EPChannelInfoViewController*) segue.destinationViewController;
        _infoCntr.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"playlist"]) {
        _playlistCntr = (EPChannelTableViewController*) segue.destinationViewController;
        _playlistCntr.delegate = self;
    }
}


- (void) hideUI{
    
    if (_UIHidden) {
        return;
    }
    
    
    
    _UIHidden = YES;
    
    
    if (self.previewImageView ==nil) {
        UIView * __previewImageView = [[UIView alloc] initWithFrame:self.view.bounds];
        
        [self.view addSubview:__previewImageView];
        
        self.previewImageView = __previewImageView;
        
        
        if ([[[EPData sharedEPData] currentChannelController] isEqual:self]) {
            
            if (_scrollView.contentOffset.y!=0) {
                
                [_scrollView setContentOffset:CGPointZero animated:YES];
                
            }
            
        }else{
            _scrollView.contentOffset=CGPointMake(0, 0);
        }

        
    }
    
    
    
    
}

- (void) showUI{
    
    if (!_UIHidden) {
        return;
    }
    
    _UIHidden = NO;
    
    _scrollView.contentOffset=CGPointMake(0, 0);
    
    _canUpdate = YES;
    
    [self.previewImageView removeFromSuperview];
    self.previewImageView = nil;
    
    
    if (_needsUpdate) {
        _needsUpdate = NO;
        [self updateUI:self.playlist.lastItem.track.cover.img!=nil sender:self];
    }
    
    self.view.userInteractionEnabled=YES;
}


@end
