//
//  EPIPADStyledViewController.h
//  Europa Plus
//
//  Created by Piezo on 5/1/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPIPADStyledViewController : UIViewController


@property (strong, nonatomic) IBOutletCollection(id) NSArray *themeBtns;
@property (strong, nonatomic) IBOutletCollection(id) NSArray *themeLabels;
@property (strong, nonatomic) IBOutletCollection(id) NSArray *themeImages;


-(void) updateWithPlaylist : (Playlist *) playlist
                 tintColor : (UIColor *) tintColor
                aTintColor : (UIColor *)aTintColor
                     white : (CGFloat) white;

@end
