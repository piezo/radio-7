Pod::Spec.new do |s|
  s.name         =  'JASidePanels'
  s.version      =  '1.3.2'
  s.license      =  'MIT'
  s.summary      =  'UIViewController container designed for presenting a center panel with revealable side panels - one to the left and one to the right.'
  s.author       =  { 'Jesse Andersen' => 'gotosleep@gmail.com' }
  s.source       =  { :git => 'https://github.com/gotosleep/JASidePanels.git', :tag => '1.3.2' }
  s.homepage     =  'http://github.com/nicklockwood/FXBlurView'
  s.platform     =  :ios
  s.source_files =  'JASidePanels/Source/*'
  s.frameworks   =  'QuartzCore'
  s.requires_arc =  true
  s.ios.deployment_target = '4.3'
end